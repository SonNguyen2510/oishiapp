# mcapp-mobile

#### iOS

1.  Xcode
2.  Cocoapods

#### Android

1. Android SDK 6 (API 23)
2. Android SDK Tools (26.1.1)
3. Android Platform-Tools (26.0.2)
4. Android SDK Build-Tools

## Commands

#### Run the proj

1. yarn start --reset-cache

###### iOS

2. react-native run-ios

add --simulator='iPhone 6s' and change device you want.

###### Android

2. react-native run-android

#### Build

###### iOS

1. yarn build:ios
2. Xcode > Product > Archive > ...wait > Distribute app
3. Choose App store for production, Enterprise for UAT/SIT

###### Android

1. yarn build:android
2. cd android
3. ENVFILE=.env.dev ./gradlew asssembleRelease

p/s: .env.uat (UAT) / .env.prod (PRODUCTION)

###### Troubleshoot

8 Steps to Get Rid Of Error “nw_connection_get_connected_socket Connection has no connected handler TCP Conn Failed
1-Xcode menu -> Product -> Edit Scheme…
2-Environment Variables -> Add -> Name: "OS_ACTIVITY_MODE", Value:"disable"
3-Xcode menu -> product -> clean
4-Xcode menu product -> ‘clean build folder’ (hold down option key)
5-Install react-devtools: npm install -g react-devtools
6-Run react-devtools: react-devtools
7-In your project, edit node_modules/react-native/Libraries/Core/Devtools/setupDevtools.js by replacing ‘localhost’ with your development machine’s IP address.
8-Build Project — (it will take long time for first time)

https://medium.com/@hr.hseyin_80381/8-steps-to-get-rid-of-error-nw-connection-get-connected-socket-connection-has-no-connected-handler-1bf622ca2332

## Clean up

watchman watch-del-all
rm -rf node_modules && yarn install
rm -rf /tmp/metro-bundler-cache-_ or yarn start -- --reset-cache
rm -rf /tmp/haste-map-react-native-packager-_

## Sonar execution
