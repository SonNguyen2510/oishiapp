/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mc_event;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.mc_event";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String API_URL = "http://nextgen-uat.ciaosolution.com/nextgen-backend/";
  public static final String GOOGLE_MAPS_API_ANDROID = "AIzaSyChBcam6lKf-O30UokrdChPoqL-WqCGKtU";
  public static final String GOOGLE_MAPS_API_IOS = "AIzaSyD179AXesB_RIBXWd8h8T-J5rgOguNZcNM";
}
