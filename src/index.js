import React from 'react'
import { connect, Provider } from 'react-redux'
import { View, StatusBar, BackHandler, Platform } from 'react-native'
import store from 'configs/store'
import Style from 'variables/style'
import color, { rgba } from 'variables/color'
import AppNavigator from 'configs/navigator'

class NavigationWrapper extends React.Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {})
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress')
  }

  render() {
    return (
      <View style={Style.Flex1}>
        <AppNavigator />
        {/* {Platform.OS === 'android' && (
          <StatusBar barStyle="default" backgroundColor={rgba(color.Black, 0.3)} />
        )} */}
        <StatusBar backgroundColor={rgba(color.Black, 1)} barStyle="light-content" />
      </View>
    )
  }
}

// const mapStateToProps = ({ navigation }) => ({
//   navigation,
// })

// const mapDispatchToProps = dispatch => ({
//   dispatch,
// })

// const AppWithNavigation = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(NavigationWrapper)

/**This setting is use for enable chrome dev tool tracking xhr request made in app */
if (__DEV__) {
  // GLOBAL.FormData = GLOBAL.originalFormData || GLOBAL.FormData
  // IMPORTANT: this will cause FormData requests to fail.
  GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest
}

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationWrapper />
      </Provider>
    )
  }
}
