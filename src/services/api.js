import axios from 'axios'
import _ from 'lodash'
import { Platform } from 'react-native'
import DeviceInfo from 'react-native-device-info'
import Config from 'react-native-config'

import EndPoints from 'configs/endPoints'
import Languages from 'resources/languages'

let source

const api = axios.create({
  baseURL: Config.API_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    // Authorization: 'Bearer 29891a316948bcdf7748b46c2ccb3144f1d62026',
  },
  timeout: 120000,
})

const APP_CONTEXT = {
  deviceId: DeviceInfo.getUniqueId(),
  deviceName: DeviceInfo.getDeviceName(),
  deviceModel: DeviceInfo.getModel(),
  deviceBrand: DeviceInfo.getBrand(),
  deviceOS: Platform.OS,
  appVersion: DeviceInfo.getVersion(),
}

function getToken() {
  return api.defaults.headers.common['Authorization'].split(' ')[1]
}

function setToken(value) {
  api.defaults.headers.common['Authorization'] = `Bearer ${value}`
}

function unsetToken() {
  delete api.defaults.headers.post['Authorization']
}

function initToken(old_token = '') {
  return new Promise((resolve, reject) => {
    api
      .request({
        method: 'POST',
        url: EndPoints.getToken,
        timeout: 60000,
        headers: {
          Authorization: 'Basic c2hvcHBpbmdfb2F1dGhfY2xpZW50OnNob3BwaW5nX29hdXRoX3NlY3JldA==',
        },
        data: { old_token },
      })
      .then(checkStatus)
      .then(res => {
        const token = _.get(res, 'data.access_token')
        setToken(token)
        resolve(res.data)
      })
      .catch(error => {
        console.log('error: ', error)
        reject(error)
      })
  })
}

function checkStatus(response) {
  return new Promise((resolve, reject) => {
    const result = response.data
    const status = _.get(response, 'status')
    if (status === 200) {
      resolve({
        data: result.data,
      })
    } else {
      reject('error')
    }
  })
}

function cancel(message = 'Request canceled') {
  if (source) source.cancel(message)
}

function customFetch(url, method, body = {}, customOptions = {}) {
  console.log('URL',url)
  return new Promise((resolve, reject) => {
    const options = {
      method,
      url,
      data: {
        ...body,
        appContext: {
          ...APP_CONTEXT,
          language: Languages.getLanguage().toUpperCase(),
        },
      },
      ...customOptions,
    }

    api
      .request(options)
      .then(checkStatus)
      .then(result => {
        resolve(result.data)
      })
      .catch(error => {
        const getError = _.get(error.response, 'data.error')
        reject(getError || 'Something went wrong')
      })
  })
}

function postFormData(url, body = {}) {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'POST',
      url,
      data: body,
      headers: {
        Accept: 'multipart/form-data',
        'Content-Type': 'multipart/form-data; boundary=someArbitraryUniqueString',
      },
    }

    api
      .request(options)
      .then(checkStatus)
      .then(result => {
        resolve(result.data)
      })
      .catch(error => {
        const getError = _.get(error.response, 'data.error')
        reject(getError || 'Something went wrong')
      })
  })
}

function post(url, body, options) {
  return customFetch(url, 'POST', body, options)
}

function get(url, body, customOptions = {}) {
  console.log('GET URL',url)
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url,
      params: {
        ...body,
      },
      ...customOptions,
    }

    api
      .request(options)
      .then(checkStatus)
      .then(result => {
        resolve(result.data)
      })
      .catch(error => {
        const getError = _.get(error.response, 'data.error')
        reject(getError || 'Something went wrong')
      })
  })
}

export default {
  getToken,
  setToken,
  unsetToken,
  post,
  get,
  initToken,
  cancel,
  postFormData,
}
