import { Platform } from 'react-native'
import Config from 'react-native-config'

const CL = 'CLIENT'
const SUP = 'SUPERVISOR'
const PM = 'PROJECTMANAGER'
const OM = 'OPERATIONMANAGER'
const ADMIN = 'Administrator'

const STATUS_CLOSED = 'CLOSED'
const STATUS_OPENING = 'OPENING'
const STATUS_NEW = 'NEW'

const PM_APPROVED = 'PM_APPROVED'
const OM_APPROVED = 'OM_APPROVED'
const PENDING = 'PENDING'

const NOTIFY_NEW = 'NEW'
const NOTIFY_READ = 'READ'

const GOOGLE_MAPS_KEY = Platform.OS === 'ios' ? Config.GOOGLE_MAPS_API_IOS : Config.GOOGLE_MAPS_API_ANDROID

export default {
  CL,
  SUP,
  PM,
  OM,
  ADMIN,
  GOOGLE_MAPS_KEY,
  STATUS_CLOSED,
  STATUS_OPENING,
  STATUS_NEW,
  PM_APPROVED,
  OM_APPROVED,
  PENDING,
  NOTIFY_NEW,
  NOTIFY_READ,
}
