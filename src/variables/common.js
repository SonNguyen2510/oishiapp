export default {
  fontRegular: 'Asap-Regular',
  fontMedium: 'Asap-Medium',
  fontBold: 'Asap-Bold',
}
