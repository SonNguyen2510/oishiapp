import { Platform, StyleSheet, StatusBar, Dimensions } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper'

import color, { rgba } from './color'

const SCREEN_WIDTH = Dimensions.get('screen').width
const SLOGAN_WIDTH = Dimensions.get('window').width > 350 ? '90%' : '100%'

export default StyleSheet.create({
  headline1: {
    fontSize: 34,
    lineHeight: 36,
    // color: color.Black1,
  },
  headline2: {
    fontSize: 32,
    lineHeight: 36,
    // color: color.Black1,
  },
  headline3: {
    fontSize: 24,
    lineHeight: 32,
    // color: color.Black1,
  },
  headline4: {
    fontSize: 20,
    lineHeight: 26,
    // color: color.Black1,
  },
  headline5: {
    fontSize: 18,
    lineHeight: 24,
    // color: color.Black1,
  },
  headline6: {
    fontSize: 14,
  },
  bodyText: {
    fontSize: 14,
    lineHeight: 22,
    // color: color.Black1,
  },
  bodyTextDark: {
    fontSize: 14,
    lineHeight: 22,
    color: color.Black2,
  },
  bodyTextLight: {
    fontSize: 14,
    lineHeight: 22,
    color: color.White1,
  },
  smallBodyText: {
    fontSize: 14,
    lineHeight: 18,
    // color: color.Black1,
  },
  smallBodyTextLight: {
    fontSize: 14,
    lineHeight: 18,
    color: color.White1,
  },
  smallBodyTextGrey: {
    fontSize: 14,
    lineHeight: 18,
    color: color.Grey3,
  },
  linkText: {
    fontSize: 14,
    lineHeight: 22,
    color: color.LightBlue,
  },
  linkTextOnWhite: {
    fontSize: 14,
    lineHeight: 22,
    color: color.MidBlue,
  },
  noteText: {
    fontSize: 12,
    lineHeight: 12,
    color: color.Grey3,
  },
  tinyBodyText: {
    fontSize: 12,
    lineHeight: 15,
    color: rgba(color.Black1, 0.6),
  },
  tinyBodyTextLight: {
    fontSize: 12,
    lineHeight: 15,
    color: color.White1,
  },
  textBlack: {
    color: color.Black1,
  },
  textWhite: {
    color: color.White1,
  },
  textGreen: {
    color: color.Green,
  },
  textDarkGreen: {
    color: color.DarkGreen,
  },
  textDarkGreen2: {
    color: color.DarkGreen2,
  },
  textRed: {
    color: color.Red,
  },
  textGrey: {
    color: color.Grey4,
  },
  textGrey3: {
    color: color.Grey3,
  },
  textMidBlue: {
    color: color.MidBlue,
  },
  textLightBlue: {
    color: color.LightBlue,
  },
  textOrangeYellow: {
    color: color.OrangeYellow,
  },
  textWhiteOpaque: {
    color: rgba(color.White1, 0.6),
  },
  textBold: {
    fontWeight: 'bold',
  },
  textCenter: {
    textAlign: 'center',
  },
  textRight: {
    textAlign: 'right',
  },
  textLeft: {
    textAlign: 'left',
  },
  textWrap: {
    flex: 1,
    flexWrap: 'wrap',
  },
  line: {
    width: 22,
    borderTopWidth: 1,
    borderTopColor: rgba(color.Grey3, 0.28),
  },
  hr: {
    marginTop: 16,
    paddingTop: 13,
    borderTopWidth: 1,
    height: 1,
    width: '100%',
    borderTopColor: rgba(color.Black1, 0.2),
    alignSelf: 'flex-start',
  },
  borderTop: {
    // width: '100%',
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: rgba(color.Grey3, 0.28),
  },
  borderBottom: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: rgba(color.Grey3, 0.28),
    paddingBottom: 16,
  },
  borderBottomDashed: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: rgba(color.Grey3, 0.28),
    paddingBottom: 16,
    borderStyle: 'dotted',
    borderRadius: 1,
  },
  dashedLine: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: rgba(color.Grey3, 0.2),
    // borderStyle: 'dashed',
    marginTop: 12,
    marginHorizontal: 8,
  },

  marginBot0: { marginBottom: 0 },
  marginBot4: { marginBottom: 4 },
  marginBot8: { marginBottom: 8 },
  marginBot12: { marginBottom: 12 },
  marginBot16: { marginBottom: 16 },
  marginBot24: { marginBottom: 24 },
  marginBot32: { marginBottom: 32 },
  marginBot40: { marginBottom: 40 },
  marginBot48: { marginBottom: 48 },
  marginBot56: { marginBottom: 56 },
  marginTop4: { marginTop: 4 },
  marginTop8: { marginTop: 8 },
  marginTop12: { marginTop: 12 },
  marginTop16: { marginTop: 16 },
  marginTop24: { marginTop: 24 },
  marginTop32: { marginTop: 32 },
  marginTop40: { marginTop: 40 },
  marginTop48: { marginTop: 48 },
  marginTop56: { marginTop: 56 },
  marginTop80Negative: { marginTop: -80 },
  marginRight8: { marginRight: 8 },
  marginRight16: { marginRight: 16 },
  marginLeft8: { marginLeft: 8 },
  marginLeft16: { marginLeft: 16 },

  paddingBottom4: { paddingBottom: 4 },
  paddingBottom8: { paddingBottom: 8 },
  paddingBottom12: { paddingBottom: 12 },
  paddingBottom16: { paddingBottom: 16 },
  paddingBottom24: { paddingBottom: 24 },
  paddingBottom32: { paddingBottom: 32 },
  paddingBottom40: { paddingBottom: 40 },
  paddingBottom48: { paddingBottom: 48 },
  paddingBottom56: { paddingBottom: 56 },
  paddingTop4: { paddingTop: 4 },
  paddingTop8: { paddingTop: 8 },
  paddingTop12: { paddingTop: 12 },
  paddingTop16: { paddingTop: 16 },
  paddingTop24: { paddingTop: 24 },
  paddingTop32: { paddingTop: 32 },
  paddingTop40: { paddingTop: 40 },
  paddingTop48: { paddingTop: 48 },
  paddingTop56: { paddingTop: 56 },

  marginHorizontal8: { marginHorizontal: 8 },
  marginHorizontal16: { marginHorizontal: 16 },
  marginVertical8: { marginVertical: 8 },
  marginVertical16: { marginVertical: 16 },
  marginVertical24: { marginVertical: 24 },
  padding8: { padding: 8 },
  padding16: { padding: 16 },
  paddingHorizontal4: { paddingHorizontal: 4 },
  paddingHorizontal8: { paddingHorizontal: 8 },
  paddingHorizontal16: { paddingHorizontal: 16 },
  paddingHorizontal24: { paddingHorizontal: 24 },
  paddingHorizontal32: { paddingHorizontal: 32 },
  paddingHorizontal40: { paddingHorizontal: 40 },
  paddingHorizontal48: { paddingHorizontal: 48 },
  paddingVertical0: { paddingVertical: 0 },
  paddingVertical4: { paddingVertical: 4 },
  paddingVertical8: { paddingVertical: 8 },
  paddingVertical16: { paddingVertical: 16 },
  paddingVertical24: { paddingVertical: 24 },
  paddingVertical32: { paddingVertical: 32 },

  paddingLeft24: { paddingLeft: 24 },
  paddingLeft12: { paddingLeft: 12 },
  paddingRight12: { paddingRight: 24 },

  maxWidth90: { maxWidth: '90%' },
  flexRow: { flexDirection: 'row' },
  flexColumn: { flexDirection: 'column' },
  flexSpaceBetween: { justifyContent: 'space-between' },
  flexJustifyCenter: { justifyContent: 'center' },
  flexJustifyEnd: { justifyContent: 'flex-end' },
  flexAlignCenter: { alignItems: 'center' },
  flexAlignStart: { alignItems: 'flex-start' },
  flexAlignEnd: { alignItems: 'flex-end' },
  flexAlignSelfEnd: { alignSelf: 'flex-end' },
  flexAlignSelfStart: { alignSelf: 'flex-start' },

  Flex1: { flex: 1 },
  Flex12: { flex: 1.2 },
  Flex2: { flex: 2 },
  Flex012: { flex: 0.12 },
  Flex018: { flex: 0.18 },
  Flex02: { flex: 0.2 },
  Flex03: { flex: 0.3 },
  Flex025: { flex: 0.25 },
  Flex045: { flex: 0.45 },
  Flex05: { flex: 0.5 },
  Flex055: { flex: 0.55 },
  Flex06: { flex: 0.6 },
  Flex07: { flex: 0.7 },
  Flex075: { flex: 0.75 },
  Flex08: { flex: 0.8 },
  Flex09: { flex: 0.9 },
  Flex01: { flex: 0.1 },
  FlexGrow1: { flexGrow: 1 },
  FlexWrap: { flexWrap: 'wrap' },
  Opacity1: { opacity: 1 },
  Opacity0: { opacity: 0 },
  DialogTitle: {
    fontSize: 28,
    color: color.Black1,
  },
  DialogText: {
    fontSize: 16,
    color: color.Black1,
    lineHeight: 21,
  },
  Slogan: {
    flexDirection: 'row',
    width: SLOGAN_WIDTH,
    fontSize: 36,
    color: color.White,
    backgroundColor: 'transparent',
  },
  Label: {
    fontSize: 14,
    lineHeight: 22,
    color: color.White,
    backgroundColor: 'transparent',
  },
  ContainerContent: {
    paddingTop: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
    backgroundColor: color.White,
  },
  Container: {
    flex: 1,
    backgroundColor: color.White,
  },
  contentWrapper: {
    flex: 1,
    paddingVertical: 32,
    paddingHorizontal: 16,
    backgroundColor: color.Black1,
  },
  BackgroundImage: {
    flex: 1,
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  ImageFullScreen: {
    flex: 1,
    resizeMode: 'cover',
    // justifyContent: 'center',
    // backgroundColor: color.White,
  },
  Title: {
    fontSize: 32,
    textAlign: 'center',
    color: color.White,
    backgroundColor: 'transparent',
  },
  SubTitle: {
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: color.White,
    backgroundColor: 'transparent',
  },
  Logo: {
    width: 96,
    height: 60,
    // marginTop: 20,
    position: 'relative',
  },
  BtnSection: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bgTransparent: {
    backgroundColor: 'transparent',
  },
  bgBlack: {
    backgroundColor: color.Black1,
  },
  bgWhite: {
    backgroundColor: color.White,
  },
  bgLightGrey: {
    backgroundColor: color.LightGrey,
  },
  bgBlue: {
    backgroundColor: color.EgyptianBlue,
  },
  CornerTopRight: {
    top: Platform.OS === 'ios' ? 40 : 15,
    right: 20,
    position: 'absolute',
  },
  CornerTopLeft: {
    top: Platform.OS === 'ios' ? 40 : 15,
    left: 20,
    position: 'absolute',
  },
  CornerBottomRight: {
    bottom: 25,
    right: 20,
    position: 'absolute',
  },
  LeftTopCorner: {
    top: 0,
    left: 10,
    position: 'absolute',
  },
  threeDots: {
    height: 60,
    width: 60,
    paddingTop: 20,
    position: 'absolute',
    top: isIphoneX() ? 44 : 20,
    right: 0,
    alignItems: 'center',
  },
  Dot: {
    width: 5,
    height: 5,
    marginLeft: 1.5,
    marginRight: 1.5,
    borderRadius: 5,
    backgroundColor: color.White,
  },
  infoPopupContainer: {
    paddingTop: 64,
    paddingHorizontal: 16,
  },
  infoPopupIcon: {
    marginBottom: 24,
    width: 50,
    height: 50,
  },
  infoPopupImage: {
    width: 184,
    height: 253,
  },
  card: {
    borderRadius: 4,
    backgroundColor: color.White,
  },
  round: {
    borderRadius: 4,
  },
  table: {
    width: '100%',
  },
  tbody: {
    width: '100%',
  },
  tr: {
    width: '100%',
  },
  td: {
    padding: 8,
    borderWidth: 1,
    borderColor: color.black1,
    flex: 1,
  },
  height0: {
    height: 0,
  },
  fullWidth: {
    width: '100%',
  },
  screenWidth: {
    width: SCREEN_WIDTH,
  },
})
