import { padStart } from 'lodash'

export function rgba(hex, opacity) {
  const alpha = Math.round(opacity * 255)
  return `${hex}${padStart(alpha.toString(16), 2, '0')}`
}

export default {
  Grey: '#F3F3F3',
  Grey1: '#686966',
  Grey2: '#535452',
  Grey3: '#88959F',
  Grey4: '#F8FAF7',
  Grey5: '#E5E5E5',
  LightGrey: '#F5F5F9',
  DarkGrey: '#BABABA',

  White: '#FFFFFF',
  White1: '#FAFAFA',

  Black: '#000000',
  Black1: '#383838',
  Black2: '#4A4A4A',
  Black3: '#484848',

  Pink: '#E6007E',
  Pink1: '#9E015F',

  Blue: '#2D2E83',
  MidBlue: '#089CC9',
  LightBlue: '#A4D8DE',
  LightBlue2: '#95D6DB',
  ChartBlue: '#00C0F2',

  NaviBlue: '#293890',
  NaviBlueLight: '#516AFF',
  EgyptianBlue: '#21409A',
  DarkBlue: '#16216A',

  Green: '#38B000',
  DarkGreen: '#315350',
  DarkGreen2: '#054954',

  Red: '#E71619',
  Red2: '#9F041B',
  Red3: '#D0021B',

  Yellow: '#FFCF00',
  Yellow1: '#FCD404',

  OrangeYellow: '#ffa802',
  Trans: 'transparent',
}
