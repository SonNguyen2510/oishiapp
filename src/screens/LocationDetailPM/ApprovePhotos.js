import React, { useRef, useState, useEffect } from 'react'
import {
  SafeAreaView,
  StatusBar,
  FlatList,
  Platform,
  TouchableOpacity,
  View,
  Image,
  Alert,
} from 'react-native'
import Tabs, { Tab, TabList, TabPanel, TabPanels } from 'components/Tabs'
import { PreviewImage, Button, TextFont } from 'components'
import { Constant } from 'services'
import Style from 'variables/style'
import styles from '../AllPhotos/styles'
import FastImage from 'react-native-fast-image'
import throttle from 'lodash/throttle'
import isEmpty from 'lodash/isEmpty'

const { PM_APPROVED, OM_APPROVED, PENDING } = Constant

export default function ApprovePhotos(props) {
  const { allImages = [], onSubmmit = () => {} } = props
  const [tab, setTab] = useState(0)
  const [imageToView, setImageToView] = useState(0)

  const [pendingImages, setSelectedImages] = useState(
    allImages.filter(item => item.status === PENDING).map(item => ({ ...item, isSelected: false })),
  )
  const previewModal = useRef()
  const selectedImage = pendingImages.filter(item => item.isSelected)
  const pm_approvedImages = allImages.filter(item => item.status === PM_APPROVED)
  const om_approvedImages = allImages.filter(item => item.status === OM_APPROVED)

  useEffect(() => {
    if (allImages) {
      setSelectedImages(
        allImages.filter(item => item.status === PENDING).map(item => ({ ...item, isSelected: false })),
      )
    }
  }, [allImages])

  const openImagePreview = index => {
    setImageToView(index)
    previewModal?.current.toggle()
  }

  const onSelectImage = (image, index) => {
    let selected = pendingImages.map((item, idx) => {
      if (index === idx) {
        return { ...item, isSelected: !item.isSelected }
      } else return item
    })
    setSelectedImages(selected)
  }

  const onSelectAll = () => {
    const checkSelectedAll = pendingImages.filter(item => !item.isSelected)
    if (checkSelectedAll.length === 0)
      setSelectedImages(pendingImages.map(item => ({ ...item, isSelected: false })))
    else setSelectedImages(pendingImages.map(item => ({ ...item, isSelected: true })))
  }

  const onSubmit = () => {
    const imageSelected = selectedImage.map(item => `${item.project_location_image_id}`)
    Alert.alert(
      'Thông báo',
      `Duyệt ${selectedImage.length} hình ảnh đã chọn`,
      [
        {
          text: 'Cancel',
        },
        {
          text: 'OK',
          onPress: () => onSubmmit({ imageSelected }),
        },
      ],
      // { cancelable: false },
    )
  }

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        key={index}
        activeOpacity={0.8}
        style={[styles.emptyPhotoContainer]}
        onLongPress={() => openImagePreview(index)}
        onPress={() => (item.isSelected != undefined ? onSelectImage(item, index) : openImagePreview(index))}
      >
        {item.isSelected != undefined && (
          <Image
            style={[styles.unselectIcon, item.isSelected && styles.selected]}
            source={
              item.isSelected
                ? require('assets/images/selected-icon.png')
                : require('assets/images/unselect-icon.png')
            }
          />
        )}
        {item.isSelected && <View style={styles.selectedWrapper} />}
        <Image resizeMode="cover" source={item} style={[styles.emptyPhoto]} />
      </TouchableOpacity>
    )
  }

  const renderPhotoList = (data = []) => {
    if (isEmpty(data)) {
      return (
        <View style={[Style.flexAlignCenter, Style.paddingBottom24]}>
          <Image
            resizeMode="center"
            source={require('assets/images/empty-image.png')}
            style={[styles.emptyList]}
          />
          <TextFont style={[Style.smallBodyTextGrey, Style.paddingTop12]}>{'Chưa có hình ảnh nào'}</TextFont>
        </View>
      )
    } else
      return (
        <FlatList
          keyExtractor={(_, idx) => idx}
          data={data}
          numColumns={2}
          renderItem={renderItem}
          extraData={data}
          contentContainerStyle={[Style.paddingHorizontal8]}
          initialNumToRender={4}
          ListFooterComponent={() =>
            tab === 0 ? (
              <View style={[Style.padding16, Style.flexAlignCenter]}>
                <Button title={'Chọn tất cả'} type="blue" onPress={onSelectAll} />
              </View>
            ) : null
          }
        />
      )
  }

  const tapMapping = {
    0: {
      imageList: pendingImages,
    },
    1: {
      imageList: om_approvedImages,
    },
    2: {
      imageList: pm_approvedImages,
    },
  }

  return (
    <SafeAreaView style={[Style.Flex1]}>
      <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'} />
      <View style={[Style.Flex1, Style.paddingTop24, Style.paddingHorizontal16]}>
        <Tabs style={[Style.Flex1]} onChangeTab={tab => setTab(tab)} activeIdx={tab}>
          <TabList style={Style.bgWhite}>
            <Tab type="yellow" title={'Chờ duyệt'} label={pendingImages.length || null} />
            {props.isPMFLow && <Tab type="yellow" title={'OM duyệt'} />}
            <Tab type="yellow" title={'Đã duyệt'} />
          </TabList>
          <TabPanels style={Style.Flex1}>
            <TabPanel style={[Style.Flex1, Style.paddingTop8]}>{renderPhotoList(pendingImages)}</TabPanel>
            {props.isPMFLow && (
              <TabPanel style={[Style.Flex1, Style.paddingTop8]}>
                {renderPhotoList(om_approvedImages)}
              </TabPanel>
            )}
            <TabPanel style={[Style.Flex1, Style.paddingTop8]}>
              {renderPhotoList(props.isPMFLow ? pm_approvedImages : om_approvedImages)}
            </TabPanel>
          </TabPanels>
        </Tabs>
      </View>
      {tab === 0 && selectedImage.length != 0 && (
        <TouchableOpacity
          activeOpacity={0.8}
          style={[Style.paddingVertical24, Style.flexAlignCenter, Style.bgBlue]}
          onPress={throttle(onSubmit, 1500, { trailing: false })}
        >
          <TextFont isHTML>
            <TextFont textColor="white" style={[Style.headline5]}>
              Duyệt
            </TextFont>
            <TextFont textColor="white" fontWeight="bold" style={[Style.headline5]}>
              {` ${selectedImage.length} `}
            </TextFont>
            <TextFont textColor="white" style={[Style.headline5]}>
              hình ảnh đã chọn
            </TextFont>
          </TextFont>
        </TouchableOpacity>
      )}
      <PreviewImage showMarker={true} innerRef={previewModal} item={tapMapping[tab].imageList[imageToView]} />
    </SafeAreaView>
  )
}
