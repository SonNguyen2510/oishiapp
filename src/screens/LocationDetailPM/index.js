import { connect } from 'react-redux'
import { getLocationsDetailById, approveInfo, setFilterDate, getAllInventory } from 'actions/common'

import LocationDetailPM from './LocationDetailPM'

const mapStateToProps = ({ common, user }) => {
  return {
    isLoading: common.toJS().isLoading,
    filterDate: common.toJS().filterDate,
    locationDetail: common.toJS().locationDetail,
    userInfo: user.toJS().userInfo,
    getInventory: common.toJS().getInventory
  }
}

export default connect(
  mapStateToProps,
  { getLocationsDetailById, approveInfo, setFilterDate, getAllInventory },
)(LocationDetailPM)
