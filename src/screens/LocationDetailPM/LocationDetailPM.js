import React, { useRef, useState, useEffect, useLayoutEffect } from 'react'
import { View, SafeAreaView, Image, StatusBar, ScrollView, Platform, RefreshControl } from 'react-native'
import Tabs, { Tab, TabList } from 'components/Tabs'
import {
  TextFont,
  Loading,
  AvoidKeyboardView,
  EditStockInfo,
  EditActivityInfo,
  DateTimeInput,
  Inventory
} from 'components'
import { Util, Constant } from 'services'
import Style from 'variables/style'
import styles from 'screens/LocationDetail/styles'
import { ActivityTime, StockInfo } from 'screens/LocationDetail/MoreInfo'
import { LocationInfo } from 'screens/LocationDetail/LocationDetail'
import ApprovePhotos from './ApprovePhotos'
import isEmpty from 'lodash/isEmpty'

const { STATUS_CLOSED, STATUS_NEW, STATUS_OPENING, OM } = Constant

export default function LocationDetailClient(props) {
  const {
    locationDetail,
    getLocationsDetailById,
    filterDate,
    approveInfo,
    setFilterDate,
    route,
    userInfo,
    getAllInventory,
    getInventory
  } = props
  const { activity_info, images, stock_info, project_location_id, project_id } = locationDetail

  const status =
    activity_info?.activity_status ||
    (images?.checkin_images ? (images?.checkout_images ? STATUS_CLOSED : STATUS_OPENING) : STATUS_NEW)

  const { check_in_time, check_out_time, note, activity_info_id, check_out_by } = activity_info[0] || {}
  const {
    internal_sampling,
    internal_sale,
    internal_redemption1,
    internal_redemption2,
    external_sampling,
    external_sale,
    external_redemption1,
    external_redemption2,
  } = stock_info || {}

  //NEW UPDATE
  const [totalProduct, setTotalProduct] = useState(null)
  const [disabled, setDisabled] = useState(false)
  const inventoryModal = useRef()
  const { role } = userInfo

  useEffect(() => {
    if (getInventory && getInventory.product_code && getInventory.product_code.length > 0) {
      const data = getInventory.product_code
      setTotalProduct(data.length)
    }
  }, [getInventory])

  useEffect(() => {
    getAllInventory({ project_id, project_location_id, date: Util.getCurrentDate() })
  }, [route.params])

  //NEW UPDATE

  const editStockInfo = useRef()
  const editActivityInfo = useRef()

  const transformImagesList = (data = []) => {
    return data.map(({ image, status, created_date, check_type, project_location_image_id }) => ({
      uri: image.split('/admin').join(''),
      status,
      created_date,
      check_type,
      project_location_image_id,
    }))
  }

  const [tab, setTab] = useState(0)
  const [refreshing, setRefreshing] = useState(false)
  const [transformedImagesList, setTransformedImagesList] = useState([])
  const [activityTime, setActivityTime] = useState({
    check_in_time: Util.formatTime(check_in_time, 'LLLL'),
    check_out_time: Util.formatTime(check_out_time, 'LLLL'),
    activity_info_id,
    note,
  })

  const [locationStock, setLocationStock] = useState()

  useEffect(() => {
    if (images?.all_images) {
      setTransformedImagesList(transformImagesList(images?.all_images))
    } else setTransformedImagesList([])
  }, [images])

  useEffect(() => {
    if (!isEmpty(stock_info)) {
      const {
        internal_sampling,
        internal_sale,
        internal_redemption1,
        internal_redemption2,
        external_sampling,
        external_sale,
        external_redemption1,
        external_redemption2,
        stock_info_id,
      } = stock_info
      setLocationStock({
        internal_sampling,
        internal_sale,
        internal_redemption1,
        internal_redemption2,
        external_sampling,
        external_sale,
        external_redemption1,
        external_redemption2,
        stock_info_id,
      })
    } else setLocationStock({})

    if (!isEmpty(activity_info[0])) {
      setActivityTime({
        check_in_time: Util.formatTime(activity_info[0].check_in_time, 'LLLL'),
        check_out_time: Util.formatTime(activity_info[0].check_out_time, 'LLLL'),
        activity_info_id: activity_info[0].activity_info_id,
        note: activity_info[0].note,
      })
    } else setActivityTime({})
  }, [stock_info, activity_info[0]])

  useEffect(() => {
    if (route.params?.type === 'IMAGE') {
      setTab(1)
    }
  }, [route.params?.type])

  const onChange = (id, value, isStockInfo) => {
    if (isStockInfo) setLocationStock({ ...locationStock, [id]: value })
    else setActivityTime({ ...activityTime, [id]: value })
  }

  const onSubmmit = editedInfo => {
    const params = {
      project_id,
      project_location_id,
      date: (filterDate && Util.formatDate(filterDate, 'YYYY-MM-DD')) || Util.getCurrentDate('YYYY-MM-DD'),
      ...editedInfo,
    }
    closeAllModal()
    approveInfo(params, () => refreshLocationDetail())
  }

  const refreshLocationDetail = (noLoading, callback = () => {}) => {
    getLocationsDetailById(
      {
        project_location_id,
        date: (filterDate && Util.formatDate(filterDate, 'YYYY-MM-DD')) || Util.getCurrentDate(),
      },
      callback,
      noLoading,
    )
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)
    refreshLocationDetail(true, () => setRefreshing(false))
  }, [refreshing])

  const toggleEditStockModal = () => {
    editStockInfo.current?.toggle()
  }

  const toggleInventoryPopup = () => {
    inventoryModal.current.toggle()
  }

  const toggleEditActivitykModal = () => {
    editActivityInfo.current?.toggle()
  }

  const closeAllModal = () => {
    editStockInfo.current?.toggle(false)
    editActivityInfo.current?.toggle(false)
  }

  const handleSelectLocationDate = value => {
    getLocationsDetailById(
      {
        project_location_id,
        date: Util.formatDate(value, 'YYYY-MM-DD'),
      },
      () => {
        setFilterDate(value)
      },
    )
  }

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerRight: renderDatePicker,
      headerTitle: renderTabHeader,
      headerLeftContainerStyle: Style.paddingBottom24,
    })
  }, [status, filterDate, tab])

  const renderDatePicker = () => (
    <DateTimeInput
      iconOnly
      onChange={handleSelectLocationDate}
      mode="date"
      value={filterDate}
      viewStyle={[Style.paddingBottom24]}
    />
  )

  const renderTabHeader = props => {
    const date =
      (filterDate && Util.formatDate(filterDate, 'DD/MM/YYYY')) || Util.getCurrentDate('DD/MM/YYYY')

    return (
      <View {...props} style={[Style.paddingHorizontal48]}>
        <Tabs onChangeTab={tab => setTab(tab)} activeIdx={tab}>
          <TabList style={Style.bgBlsack} small>
            <Tab title={'Thông tin'} />
            <Tab title={'Hình Ảnh'} />
          </TabList>
        </Tabs>
        <TextFont
          style={[
            Style.textCenter,
            Style.noteText,
            Style.textGrey3,
            Style.paddingVertical4,
            Style.paddingTop8,
          ]}
        >
          {date}
        </TextFont>
      </View>
    )
  }

  if (tab === 1) {
    return (
      <>
        {!isEmpty(transformedImagesList) ? (
          <ApprovePhotos
            allImages={transformedImagesList}
            isPMFLow={userInfo?.role != OM}
            onSubmmit={onSubmmit}
          />
        ) : (
          <View style={[Style.flexAlignCenter, Style.bgBlasck, Style.paddingTop32]}>
            <Image
              resizeMode="center"
              source={require('assets/images/empty-image.png')}
              style={[styles.emptyImages]}
            />
            <TextFont style={[Style.smallBodyTextGrey]}>{'Chưa có hình ảnh nào được chụp'}</TextFont>
          </View>
        )}
        {props.isLoading && <Loading />}
      </>
    )
  }

  return (
    <SafeAreaView style={[Style.Flex1, Style.bgLightGrey, Style.flexSpaceBetween]}>
      <AvoidKeyboardView style={Style.Flex1}>
        <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'} />
        <ScrollView
          style={[Style.paddingHorizontal16]}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
        >
          <LocationInfo locationDetail={locationDetail} status={status} />
          {!isEmpty(activity_info[0]) && (
            <View>
              <ActivityTime
                check_in_time={check_in_time}
                check_out_time={check_out_time}
                onOpenEditActivityInfo={toggleEditActivitykModal}
                isPMFlow
              />
              {!!stock_info && (
                <StockInfo
                  disabled={disabled}
                  role={role}
                  totalProduct={totalProduct}
                  navigateToInventory={toggleInventoryPopup}
                />
              )}
              {!!note && (
                <View style={Style.paddingBottom48}>
                  <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom8]}>
                    <TextFont fontWeight="bold" style={[Style.headline5]}>
                      Ghi chú trong ngày
                    </TextFont>
                    <View style={Style.dashedLine} />
                  </View>
                  <View style={[Style.paddingVertical16]}>
                    <TextFont style={[Style.headline5, Style.textGrey3]}>{note}</TextFont>
                  </View>
                </View>
              )}
              {/* <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom24]}>
                <TextFont fontWeight="bold" style={[Style.headline5]}>
                  {'Hình ảnh đã duyệt'}
                </TextFont>
                <View style={Style.dashedLine} />
              </View> */}
              {/* {!isEmpty(transformedImagesList) ? (
                  <>
                    <PhotosList
                      data={transformedImagesList.slice(0, 6)}
                      // isPMFLow
                    />
                    <View style={[Style.paddingHorizontal16, Style.paddingBottom16, Style.flexAlignCenter]}>
                      <Button title={'Duyệt hình ảnh'} type="blue" onPress={navigateToApproveImage} />
                    </View>
                  </>
                ) : (
                  <View style={[Style.flexAlignCenter, Style.paddingBottom24]}>
                    <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom24]}>
                      <TextFont fontWeight="bold" style={[Style.headline5]}>
                        {'Hình ảnh tại địa điểm'}
                      </TextFont>
                      <View style={Style.dashedLine} />
                    </View>
                    <Image
                      resizeMode="center"
                      source={require('assets/images/empty-image.png')}
                      style={[styles.emptyImages]}
                    />
                    <TextFont style={[Style.smallBodyTextGrey, Style.paddingTop12]}>
                      {'Chưa có hình ảnh nào được chụp'}
                    </TextFont>
                  </View>
                )} */}
            </View>
          )}
        </ScrollView>
        {locationStock && (
          <EditStockInfo
            innerRef={editStockInfo}
            onChange={(id, value) => onChange(id, value, true)}
            locationStock={locationStock}
            onSubmmit={onSubmmit}
          />
        )}
        <EditActivityInfo
          onSubmmit={onSubmmit}
          innerRef={editActivityInfo}
          onChange={onChange}
          activityTime={activityTime}
          check_out_by={check_out_by}
          note={note}
        />
        {props.isLoading && <Loading />}
        <Inventory
          disabled={disabled}
          innerRef={inventoryModal}
          closeModal={() => inventoryModal.current.toggle()}
        />
      </AvoidKeyboardView>
    </SafeAreaView>
  )
}
