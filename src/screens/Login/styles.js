import { StyleSheet, Dimensions } from 'react-native'
const SCREEN_HEIGHT = Dimensions.get('screen').height
const SCREEN_WIDTH = Dimensions.get('screen').width

import color from 'variables/color'

export default StyleSheet.create({
  buttonContainer: {
    flex: 0.1,
    // flexDirection: 'row',
    justifyContent: 'flex-end',
    // alignItems: 'center',
    // marginBottom: -5,
    backgroundColor: color.EgyptianBlue,
  },
  imageLogo: {
    // width: 200,
    // height: 225,
    width: SCREEN_WIDTH / 1.8,
    height: SCREEN_HEIGHT / 2.8,
  },
})
