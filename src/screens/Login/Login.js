import React, { useState, useEffect } from 'react'
import { View, StatusBar, Platform, SafeAreaView, Image, ImageBackground, Keyboard } from 'react-native'
import {
  Input,
  TextFont,
  AvoidKeyboardView,
  InputGroup,
  Button,
  DismissKeyboardView,
  Loading,
} from 'components'
import Style from 'variables/style'
import styles from './styles'

export default function Landing(props) {
  const [isShowLogo, setShowLogo] = useState(true)
  const [userName, setUserName] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => setShowLogo(false))
    Keyboard.addListener('keyboardDidHide', () => setShowLogo(true))

    return () => {
      Keyboard.removeListener('keyboardDidShow')
      Keyboard.removeListener('keyboardDidHide')
    }
  }, [])

  const onPressLogin = () => {
    props.userLogin(
      {
        username: userName,
        password: password,
      },
      // () => props.navigation.replace('DashBoard'),
    )
    // props.navigation.replace('DashBoard')
  }

  return (
    // <ImageBackground style={[Style.ImageFullScreen, Style.bgWhite]} source={require('images/bg_login.png')}>
    <AvoidKeyboardView style={[Style.Flex1, Style.bgWhite]} setShowLogo={setShowLogo}>
      <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
      <SafeAreaView style={[Style.Flex1]}>
        <DismissKeyboardView
          style={[Style.flexAlignCenter, Style.flexJustifyCenter, Style.Flex1, Style.paddingHorizontal16]}
        >
          {isShowLogo && (
            <View style={[Style.Flex12, Style.flexAlignCenter, Style.flexJustifyCenter]}>
              <Image
                style={[styles.imageLogo]}
                resizeMode="contain"
                source={require('images/oishi-logo.png')}
              />
            </View>
          )}
          <View style={[Style.Flex1, Style.flexJustifyCenter, Style.paddingBottom24]}>
            <TextFont
              textColor="blue"
              fontWeight="medium"
              style={[Style.paddingBottom12, Style.headline1, Style.flexAlignSelfStart]}
            >
              Xin chào!
            </TextFont>
            <TextFont textColor="blue" style={[Style.headline5, Style.flexAlignSelfStart]}>
              Vui lòng nhập thông tin tài khoản đã đăng ký
            </TextFont>
            <View style={[]}>
              <InputGroup style={Style.marginBot16}>
                <Input placeholder="Tên đăng nhập" value={userName} onChange={text => setUserName(text)} />
                <Input
                  placeholder="Mật khẩu"
                  isPassword
                  showSecure
                  value={password}
                  onChange={text => setPassword(text)}
                />
              </InputGroup>
            </View>
          </View>
        </DismissKeyboardView>
        <Button
          onPress={onPressLogin}
          buttonStyle={Style.paddingVertical24}
          type="blue"
          title="Login"
          borderRadius={0}
        />
      </SafeAreaView>
      {props.isLoading && <Loading />}
    </AvoidKeyboardView>
    // </ImageBackground>
  )
}
