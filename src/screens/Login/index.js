import { connect } from 'react-redux'
import { userLogin } from 'actions/user'
import Landing from './Login'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
  }
}

export default connect(
  mapStateToProps,
  { userLogin },
)(Landing)
