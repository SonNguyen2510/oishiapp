import { connect } from 'react-redux'
import { getLocationsDetailById, setFilterDate, getAllInventory } from 'actions/common'

import LocationDetailClient from './LocationDetailClient'

const mapStateToProps = ({ common, user }) => {
  return {
    isLoading: common.toJS().isLoading,
    filterDate: common.toJS().filterDate,
    locationDetail: common.toJS().locationDetail,
    userInfo: user.toJS().userInfo,
    getInventory: common.toJS().getInventory,
  }
}

export default connect(
  mapStateToProps,
  { getLocationsDetailById, setFilterDate, getAllInventory },
)(LocationDetailClient)
