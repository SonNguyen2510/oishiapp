import React, { useLayoutEffect, useState, useRef, useEffect } from 'react'
import {
  View,
  SafeAreaView,
  StatusBar,
  ScrollView,
  Platform,
  RefreshControl,
  TouchableOpacity,
  Image,
} from 'react-native'
import Tabs, { Tab, TabList } from 'components/Tabs'
import { TextFont, Button, Loading, AvoidKeyboardView, DateTimeInput, Inventory } from 'components'
import { Util, Constant } from 'services'
import Style from 'variables/style'
import { ActivityTime, StockInfo } from 'screens/LocationDetail/MoreInfo'
import { LocationInfo } from 'screens/LocationDetail/LocationDetail'
import PhotosList from 'screens/LocationDetail/PhotosList'
import isEmpty from 'lodash/isEmpty'
import styles from 'screens/LocationDetail/styles'
import AllPhotos from 'screens/AllPhotos'
import { omit } from 'lodash'

const { STATUS_CLOSED, STATUS_NEW, STATUS_OPENING, PM_APPROVED, OM_APPROVED } = Constant

export default function LocationDetailClient(props) {
  const { locationDetail, getLocationsDetailById, filterDate, navigation, setFilterDate, getInventory, getAllInventory, userInfo, route } = props
  const { activity_info, images, stock_info, project_id, project_location_id } = locationDetail

  const status =
    activity_info?.activity_status ||
    (images?.checkin_images ? (images?.checkout_images ? STATUS_CLOSED : STATUS_OPENING) : STATUS_NEW)

  const { check_in_time, check_out_time, note } = activity_info[0] || {}
  const { external_sampling, external_sale, external_redemption1, external_redemption2 } = stock_info || {}

  const [tab, setTab] = useState(0)

  //NEW UPDATE
  const [totalProduct, setTotalProduct] = useState(null)
  const [disabled, setDisabled] = useState(false)
  const [hideProduct, setHideProduct] = useState(true)
  const inventoryModal = useRef()
  const { role } = userInfo

  useEffect(() => {
    if (getInventory && getInventory.product_code && getInventory.product_code.length > 0) {
      const data = getInventory.product_code
      for(const i in data) {
        if (data[i].details[0] && data[i].details[0].client_quantity) {
          setHideProduct(false)
        }
      }
      setTotalProduct(data.length)
    }
  }, [getInventory])

  useEffect(() => {
    getAllInventory({ project_id, project_location_id, date: Util.getCurrentDate() })
  }, [route.params])

  //NEW UPDATE

  const transformImagesList = (data = []) => {
    return data
      .filter(item => item.status === PM_APPROVED || item.status === OM_APPROVED)
      .map(({ image, status, created_date, check_type }) => ({
        uri: image.split('/admin').join(''),
        status,
        created_date,
        check_type,
      }))
  }

  const toggleInventoryPopup = () => {
    inventoryModal.current.toggle()
  }

  const handleSelectLocationDate = value => {
    getLocationsDetailById(
      {
        project_location_id,
        date: Util.formatDate(value, 'YYYY-MM-DD'),
      },
      () => {
        setFilterDate(value)
      },
    )
  }

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerRight: renderDatePicker,
      headerTitle: renderTabHeader,
      headerLeftContainerStyle: Style.paddingBottom24,
    })
  }, [status, filterDate, tab])

  const renderDatePicker = () => (
    <DateTimeInput
      iconOnly
      onChange={handleSelectLocationDate}
      mode="date"
      value={filterDate}
      viewStyle={[Style.paddingBottom24]}
    />
  )

  const renderTabHeader = props => {
    const date =
      (filterDate && Util.formatDate(filterDate, 'DD/MM/YYYY')) || Util.getCurrentDate('DD/MM/YYYY')

    return (
      <View {...props} style={[Style.paddingHorizontal48]}>
        <Tabs onChangeTab={tab => setTab(tab)} activeIdx={tab}>
          <TabList style={Style.bgBlsack} small>
            <Tab title={'Thông tin'} />
            <Tab title={'Hình Ảnh'} />
          </TabList>
        </Tabs>
        <TextFont
          style={[
            Style.textCenter,
            Style.noteText,
            Style.textGrey3,
            Style.paddingVertical4,
            Style.paddingTop8,
          ]}
        >
          {date}
        </TextFont>
      </View>
    )
  }

  const transformedImagesList = transformImagesList(images?.all_images)

  const [refreshing, setRefreshing] = React.useState(false)

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)
    getLocationsDetailById(
      {
        project_location_id,
        date: (filterDate && Util.formatDate(filterDate, 'YYYY-MM-DD')) || Util.getCurrentDate(),
      },
      () => setRefreshing(false),
      true,
    )
  }, [refreshing])

  if (tab === 1) {
    return (
      <>
        {!isEmpty(transformedImagesList) ? (
          <AllPhotos
            allImages={transformedImagesList}
            isClientFlow
            showMarker={true}
            // onSubmmit={onSubmmit}
          />
        ) : (
          <View style={[Style.flexAlignCenter, Style.bgBlasck, Style.paddingTop32]}>
            <Image
              resizeMode="center"
              source={require('assets/images/empty-image.png')}
              style={[styles.emptyImages]}
            />
            <TextFont style={[Style.smallBodyTextGrey]}>{'Chưa có hình ảnh nào được chụp'}</TextFont>
          </View>
        )}
        {props.isLoading && <Loading />}
      </>
    )
  }

  return (
    <SafeAreaView style={[Style.Flex1, Style.bgLightGrey, Style.flexSpaceBetween]}>
      <AvoidKeyboardView style={Style.Flex1}>
        <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
        <ScrollView
          style={[Style.paddingHorizontal16]}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
        >
          <LocationInfo locationDetail={locationDetail} status={status} />
          {!isEmpty(activity_info) && (
            <View>
              {activity_info[0]?.status === PM_APPROVED && (
                <ActivityTime check_in_time={check_in_time} check_out_time={check_out_time} />
              )}
              {!hideProduct && (
                <StockInfo
                  disabled={disabled}
                  role={role}
                  totalProduct={totalProduct}
                  navigateToInventory={toggleInventoryPopup}
                />
              )}
              {!!note && (
                <View style={Style.paddingBottom32}>
                  <TextFont fontWeight="bold" style={[Style.headline5, Style.paddingTop12]}>
                    Ghi chú trong ngày
                  </TextFont>
                  <View style={[Style.paddingVertical16]}>
                    <TextFont style={[Style.headline5, Style.textGrey3]}>{note}</TextFont>
                  </View>
                </View>
              )}
              {!isEmpty(transformedImagesList) && (
                <>
                  <PhotosList showMarker={true} data={transformedImagesList.slice(0, 6)} /> 
                  {transformedImagesList.length > 6 && (
                    <View style={[Style.paddingHorizontal16, Style.paddingVertical16, Style.flexAlignCenter]}>
                      <Button
                        title={'Xem tất cả ảnh'}
                        type="white"
                        onPress={() => navigation.navigate('AllPhotos', { allImages: transformedImagesList })}
                      />
                    </View>
                  )}
                </>
              )}
            </View>
          )}
        </ScrollView>
        {props.isLoading && <Loading />}
        <Inventory
          disabled={disabled}
          innerRef={inventoryModal}
          closeModal={() => inventoryModal.current.toggle()}
        />
      </AvoidKeyboardView>
    </SafeAreaView>
  )
}
