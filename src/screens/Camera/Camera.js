import React from 'react'
import { RNCamera } from 'react-native-camera'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  SafeAreaView,
} from 'react-native'
import Style from 'variables/style'
import color, { rgba } from 'variables/color'
import { Loading } from 'components'
import _ from 'lodash'
import moment from 'moment'
import Geolocation from '@react-native-community/geolocation'

const flashModeOrder = {
  off: 'auto',
  auto: 'on',
  on: 'off',
  // torch: 'off',
}

const wbOrder = {
  auto: 'sunny',
  sunny: 'cloudy',
  cloudy: 'shadow',
  shadow: 'fluorescent',
  fluorescent: 'incandescent',
  incandescent: 'auto',
}

const landmarkSize = 2

export default class CameraScreen extends React.Component {
  state = {
    flash: 'off',
    autoFocus: 'on',
    autoFocusPoint: {
      normalized: { x: 0.5, y: 0.5 }, // normalized values required for autoFocusPointOfInterest
      drawRectPosition: {
        x: Dimensions.get('window').width * 0.5,
        y: Dimensions.get('window').height * 0.5,
      },
    },
    type: 'back',
    whiteBalance: 'auto',
    ratio: '16:9',
    isCameraOn: true,
    currentImageIndex: 0,
    uploadedImages: 1,
    images: [],
  }

  toggleFacing() {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    })
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    })
  }

  toggleWB() {
    this.setState({
      whiteBalance: wbOrder[this.state.whiteBalance],
    })
  }

  toggleFocus() {
    this.setState({
      autoFocus: this.state.autoFocus === 'on' ? 'off' : 'on',
    })
  }

  touchToFocus(event) {
    const { pageX, pageY } = event.nativeEvent
    const screenWidth = Dimensions.get('window').width
    const screenHeight = Dimensions.get('window').height
    const isPortrait = screenHeight > screenWidth

    let x = pageX / screenWidth
    let y = pageY / screenHeight
    // Coordinate transform for portrait. See autoFocusPointOfInterest in docs for more info
    if (isPortrait) {
      x = pageY / screenHeight
      y = -(pageX / screenWidth) + 1
    }

    this.setState({
      autoFocusPoint: {
        normalized: { x, y },
        drawRectPosition: { x: pageX, y: pageY },
      },
    })
  }

  takePicture = async function() {
    let latitude = 0
    let longitude = 0
    Geolocation.requestAuthorization();
    Geolocation.getCurrentPosition(info => {
      latitude = info.coords.latitude
      longitude = info.coords.longitude
    })
    
    if (this.camera) {
      const options = {
        base64: true,
        quality: 0.3,
        // forceUpOrientation: true,
        // fixOrientation: true,
        skipProcessing: true,
      }
      const data = await this.camera.takePictureAsync(options)
      // console.log('takePicture ', data)
      this.setState(prevState => {
        const newImages = [...prevState.images]
        newImages[this.state.currentImageIndex] = { uri: data.uri, base64: data.base64, latitude, longitude }
        return { images: newImages, isCameraOn: false }
      })
    }
  }

  switchView = () => {
    this.setState(prevState => ({
      isCameraOn: !prevState.isCameraOn,
      currentImageIndex: prevState.currentImageIndex - 1,
    }))
  }

  removePicture = (cameraOff, callback = () => {}) => {
    const { currentImageIndex } = this.state
    this.setState(
      prevState => {
        const newImages = [...prevState.images]
        newImages.splice(currentImageIndex, 1)
        return { images: newImages, isCameraOn: !cameraOff }
      },
      () => callback(),
    )
  }

  uploadImage = (callback = () => {}) => {
    const { images, currentImageIndex } = this.state
    const currentPhoto = images[currentImageIndex]
    const fileName = currentPhoto.uri.split('.')
    const { project_location_id, project_id, location_name } = this.props.locationDetail
    const { check_type } = this.props.route?.params

    const photoToUpload = {
      fileBase64: currentPhoto.base64,
      name: `IMG_${moment().format('YYYYMMDD')}_${moment().format('HHMMSS')}.${
        fileName[fileName.length - 1]
      }`,
      type: 'image/jpeg',
      error: null,
    }

    let params = {
      file: [photoToUpload],
      project_location_id,
      project_id,
      check_type: check_type,
      location_name,
      date: moment().format('YYYY-MM-DD'),
    }
    this.props.uploadImage(params, () => callback(), true)
  }

  addAnotherClick = () => {
    const { check_type } = this.props.route?.params
    if (check_type === 'inprogress') {
      this.uploadImage()
      this.setState(({ isRetake, currentImageIndex }) => ({
        isCameraOn: true,
        currentImageIndex: isRetake ? currentImageIndex : currentImageIndex + 1,
      }))
    } else {
      if (this.props.route?.params?.notEmptyImage.length + this.state.images.length >= 6) this.goBack()
      else
        this.setState(({ isRetake, currentImageIndex }) => ({
          isCameraOn: true,
          currentImageIndex: isRetake ? currentImageIndex : currentImageIndex + 1,
        }))
    }
  }

  validateNavigateBack = notSave => {
    if (notSave) {
      this.removePicture(true, () => {
        this.goBack()
      })
      return
    }
    this.goBack()
  }

  goBack = () => {
    const { check_type } = this.props.route?.params

    this.props.navigation.navigate('LocationDetail', {
      toggleCheckIn: check_type != 'inprogress',
      takenImages: check_type != 'inprogress' && this.state.images,
    })
  }

  renderCamera = () => {
    const drawFocusRingPosition = {
      top: this.state.autoFocusPoint.drawRectPosition.y - 32,
      left: this.state.autoFocusPoint.drawRectPosition.x - 32,
    }

    return (
      <SafeAreaView style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref
          }}
          style={{
            flex: 1,
            justifyContent: 'space-between',
          }}
          quality={0.5}
          type={this.state.type}
          flashMode={this.state.flash}
          autoFocus={this.state.autoFocus}
          autoFocusPointOfInterest={this.state.autoFocusPoint.normalized}
          // zoom={this.state.zoom}
          whiteBalance={this.state.whiteBalance}
          ratio={this.state.ratio}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        >
          <View style={StyleSheet.absoluteFill}>
            <View style={[styles.autoFocusBox, drawFocusRingPosition]} />
            <TouchableWithoutFeedback onPress={this.touchToFocus.bind(this)}>
              <View style={{ flex: 1 }} />
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              flex: 0.1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <TouchableOpacity
              style={[styles.flipButton, Style.Flex01]}
              onPress={() => this.validateNavigateBack()}
            >
              <Image
                style={[styles.backButton, Style.marginLeft16]}
                source={require('images/backCamera.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.flipButton, Style.Flex02]}
              onPress={this.toggleFacing.bind(this)}
            >
              <Image
                style={styles.iconFlash}
                resizeMode="contain"
                source={require('images/flipCamera.png')}
              />
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.flipButton} onPress={this.toggleWB.bind(this)}>
              <Text style={styles.flipText}> WB: {this.state.whiteBalance} </Text>
            </TouchableOpacity> */}
          </View>
        </RNCamera>
        <View style={[Style.Flex018, Style.flexAlignCenter, Style.flexRow]}>
          <View style={[styles.bottomBtnWrapper]}>
            {this.state.images.length > 0 && (
              <TouchableOpacity onPress={() => this.validateNavigateBack()}>
                <Image
                  resizeMode="contain"
                  style={styles.currentImageButton}
                  source={this.state.images[this.state.currentImageIndex - 1]}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={[styles.bottomBtnWrapper]}>
            <View style={[styles.cameraButtonContainer]}>
              <Image
                resizeMode="contain"
                style={styles.cameraButtonBorder}
                source={require('images/cameraButtonBorder.png')}
              />
              <TouchableOpacity onPress={this.takePicture.bind(this)}>
                <Image
                  resizeMode="contain"
                  style={styles.cameraButton}
                  source={require('images/cameraButton.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={[styles.bottomBtnWrapper]}>
            <TouchableOpacity style={[Style.flexRow]} onPress={this.toggleFlash.bind(this)}>
              <Image
                style={styles.iconFlash}
                resizeMode="contain"
                source={
                  this.state.flash === 'off'
                    ? require('images/iconFlashOff.png')
                    : this.state.flash === 'on'
                    ? require('images/iconFlash.png')
                    : require('images/iconFlashAuto.png')
                }
              />
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    )
  }

  renderEditView = () => {
    const { images, currentImageIndex } = this.state

    return (
      <SafeAreaView style={[styles.editViewContainer]}>
        {/* <StatusBar /> */}
        <View style={[styles.imageContainer]}>
          <Image resizeMode="contain" style={[styles.imageDocumentScan]} source={images[currentImageIndex]} />
        </View>

        <View style={[Style.Flex018, Style.flexAlignCenter, Style.flexRow]}>
          <View style={[styles.bottomBtnWrapper]}>
            <TouchableOpacity style={[styles.flipButton]} onPress={() => this.validateNavigateBack(true)}>
              <Image style={styles.backButton} source={require('images/backCamera.png')} />
            </TouchableOpacity>
          </View>
          <View style={[styles.bottomBtnWrapper]}>
            <TouchableOpacity onPress={this.addAnotherClick}>
              <Image style={styles.saveButton} source={require('images/saveCamera.png')} />
            </TouchableOpacity>
          </View>
          <View style={[styles.bottomBtnWrapper]}>
            <TouchableOpacity onPress={() => this.removePicture(false)}>
              <Image style={styles.backButton} source={require('images/removeCamera.png')} />
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    )
  }

  render() {
    const { isCameraOn, images } = this.state
    return (
      <>
        {isCameraOn || _.isEmpty(images) ? this.renderCamera() : this.renderEditView()}
        {this.props.isLoading && <Loading />}
      </>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  editViewContainer: {
    flex: 1,
    backgroundColor: color.Black,
  },
  editContainer: {
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imageDocumentScan: {
    height: '100%',
    width: '100%',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  flipButton: {
    flex: 0.3,
    height: 40,
    marginHorizontal: 2,
    marginBottom: 10,
    marginTop: 10,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  currentImageButton: {
    height: 44,
    width: 44,
    borderRadius: 22,
    borderWidth: 2,
    borderColor: rgba(color.White, 0.6),
  },
  bottomBtnWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  autoFocusBox: {
    position: 'absolute',
    height: 64,
    width: 64,
    borderRadius: 99,
    borderWidth: 2,
    borderColor: 'white',
    opacity: 0.4,
  },
  flipText: {
    color: 'white',
    fontSize: 15,
  },
  zoomText: {
    position: 'absolute',
    bottom: 70,
    zIndex: 2,
    left: 2,
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  imageContainer: {
    flex: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: landmarkSize,
    height: landmarkSize,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  text: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#F00',
    justifyContent: 'center',
  },
  textBlock: {
    color: '#F00',
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  cameraButtonContainer: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    height: 66,
    width: 66.5,
  },
  cameraButtonBorder: {
    height: 66,
    width: 66,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  cameraButton: {
    height: 52,
    width: 52,
  },
  iconFlash: {
    height: 30,
    width: 30,
  },
  backButton: {
    height: 30,
    width: 30,
  },
  saveButton: {
    height: 70,
    width: 70,
  },
})
