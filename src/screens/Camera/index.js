import { connect } from 'react-redux'
import { userLogout } from 'actions/user'
import { uploadImage } from 'actions/common'
import CameraScreen from './Camera'

const mapStateToProps = ({ common, user }) => {
  const { isLoading, locationDetail } = common.toJS()
  return {
    userInfo: user.toJS().userInfo,
    locationDetail,
    isLoading,
  }
}

export default connect(
  mapStateToProps,
  { userLogout, uploadImage },
)(CameraScreen)
