import { connect } from 'react-redux'
import { userLogout } from 'actions/user'
import { searchLocation, getLocationsDetailById, filterProjectLocation } from 'actions/common'
import ProjectDetail from './ProjectDetail'

const mapStateToProps = ({ common, user }) => {
  return {
    isLoading: common.toJS().isLoading,
    filterDate: common.toJS().filterDate,
    searchLocationResult: common.toJS().searchLocationResult,
    userInfo: user.toJS().userInfo,
  }
}

export default connect(
  mapStateToProps,
  { userLogout, searchLocation, getLocationsDetailById, filterProjectLocation },
)(ProjectDetail)
