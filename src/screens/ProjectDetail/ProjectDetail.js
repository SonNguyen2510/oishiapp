import React, { useState, useRef, useEffect } from 'react'
import { View, SafeAreaView, ImageBackground, Image, TouchableOpacity, FlatList } from 'react-native'
import {
  Input,
  TextFont,
  Card,
  Button,
  Loading,
  AvoidKeyboardView,
  AddNewLocation,
  DismissKeyboardView,
  Filter,
  BackButton,
} from 'components'
import Style from 'variables/style'
import styles from './styles'
import isEmpty from 'lodash/isEmpty'
import { Util, Constant } from 'services'

export default function ProjectDetail(props) {
  const { projectInfo } = props.route.params

  const addNewLocation = useRef()
  const filterModal = useRef()

  const [searchTerm, setSearchTerm] = useState('')
  const [results, setResults] = useState([])

  const setSearchResult = ({ project_locations }) => {
    setResults(project_locations)
  }

  // const debouncedSearchTerm = Util.useDebounce(searchTerm, 500)

  // useEffect(() => {
  //   if (searchTerm) {
  //     const params = { project_id: projectInfo.project_id, search_text: searchTerm }
  //     props.searchLocation(params, setSearchResult)
  //   } else {
  //     setResults([])
  //   }
  // }, [searchTerm])

  useEffect(() => {
    if (props.userInfo?.role === Constant.CL) {
      const params = { project_id: projectInfo.project_id, search_text: '' }
      props.searchLocation(params, setSearchResult)
    }
  }, [props.userInfo?.role])

  const handleSearch = e => {
    if (e.nativeEvent?.text) {
      const params = { project_id: projectInfo.project_id, search_text: e.nativeEvent.text }
      props.searchLocation(params, setSearchResult)
    } else {
      setResults([])
    }
  }

  const toggleAddNewPopup = () => {
    addNewLocation.current?.toggle()
  }

  const toggleFilterModal = () => {
    filterModal.current?.toggle()
  }

  const navigateToProjectLocationDetail = project_location_id => {
    const date =
      props.userInfo?.role != Constant.SUP
        ? (props.filterDate && Util.formatDate(props.filterDate, 'YYYY-MM-DD')) || Util.getCurrentDate()
        : Util.getCurrentDate()
    props.getLocationsDetailById({ project_location_id, date }, () =>
      props.navigation.navigate('LocationDetail'),
    )
  }

  const filterLocation = params => {
    const additionalParams = {
      project_id: projectInfo.project_id,
    }
    toggleFilterModal()
    props.filterProjectLocation({ ...params, ...additionalParams }, setSearchResult)
  }

  const renderHeader = item => {
    const { channel, location_name } = item
    return (
      <View style={[Style.flexAlignStart]}>
        <Button title={channel} size="badge" borderRadius={5} type="yellow" />
        <TextFont textColor="darkBlue" fontWeight="bold" style={[Style.paddingTop8, Style.headline4]}>
          {location_name}
        </TextFont>
      </View>
    )
  }

  const renderBody = item => {
    const { location_address, location_city, location_district, location_ward } = item

    return (
      <View style={[Style.paddingTop12]}>
        <View style={[Style.paddingBottom12, Style.textLeft]}>
          <TextFont textColor="darkBlue" style={[Style.headline5, Style.textLeft]}>
            {`${location_address}, ${location_ward?.name || ''}, ${location_district?.name ||
              ''} \n${location_city.name || ''}`}
          </TextFont>
        </View>
      </View>
    )
  }

  const renderFooter = item => {
    const { start_date, end_date, end_time, start_time } = item

    return (
      <View style={[Style.flexRow, Style.flexAlignEnd, Style.flexSpaceBetween]}>
        <View style={[Style.paddingBottom12]}>
          <TextFont textColor="darkBlue" style={[Style.textGrey3, Style.paddingBottom4]}>
            Giờ hoạt động
          </TextFont>
          <TextFont textColor="darkBlue" style={[Style.headline5]}>
            {`${Util.formatDate(start_date)} - ${Util.formatDate(end_date)}`}
          </TextFont>
          <TextFont textColor="darkBlue" style={[Style.headline5]}>
            {`${Util.formatTime(start_time)} - ${Util.formatTime(end_time)}`}
          </TextFont>
        </View>
        <View style={[Style.paddingBottom12]}>
          <Image style={styles.cardButton} source={require('assets/images/cardButton.png')} />
        </View>
      </View>
    )
  }

  const renderItem = ({ item }) => {
    return (
      <Card
        header={renderHeader}
        content={renderBody}
        footer={renderFooter}
        data={item}
        onPress={() => navigateToProjectLocationDetail(item.project_location_id)}
        noBreakLine
      />
    )
  }

  return (
    <ImageBackground style={[Style.ImageFullScreen]} source={require('images/bg_dashboard.png')}>
      <AvoidKeyboardView style={[Style.Flex1]}>
        {/* <StatusBar barStyle="light-content" /> */}
        <SafeAreaView style={[Style.Flex1]}>
          <DismissKeyboardView style={[Style.Flex1]}>
            <View style={[styles.filterBtnWrapper, { zIndex: 1 }]}>
              {props.userInfo?.role != Constant.SUP && (
                <TouchableOpacity
                  onPress={toggleFilterModal}
                  hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
                >
                  <Image style={styles.filterIcon} source={require('assets/images/filterIcon.png')} />
                </TouchableOpacity>
              )}
            </View>
            <View style={[Style.LeftTopCorner, { zIndex: 1 }]}>
              <BackButton black={false} />
            </View>
            {props.userInfo?.role != Constant.CL && (
              <TouchableOpacity
                style={[Style.CornerBottomRight, { zIndex: 1 }, Style.paddingTop12]}
                onPress={toggleAddNewPopup}
              >
                <Image style={styles.addNewImage} source={require('assets/images/add-new-location.png')} />
              </TouchableOpacity>
            )}
            <View style={[Style.paddingHorizontal24, Style.paddingBottom16]}>
              {isEmpty(results) ? (
                <View style={[Style.paddingTop56, Style.flexAlignCenter, Style.paddingBottom16]}>
                  <Image resizeMode='contain' style={[styles.imageLogo]} source={require('images/oishi-logo.png')} />
                  <TextFont textColor="white" style={[Style.headline5, Style.paddingTop24]}>
                    {'Dự án đang chạy'}
                  </TextFont>
                  <TextFont
                    textColor="white"
                    fontWeight="bold"
                    style={[Style.paddingTop4, Style.headline3, Style.paddingBottom24, Style.textCenter]}
                  >
                    {projectInfo.name}
                  </TextFont>
                </View>
              ) : (
                <TextFont
                  textColor="white"
                  fontWeight="medium"
                  style={[Style.headline5, Style.paddingVertical16, Style.textCenter]}
                >
                  {'Tìm kiếm dự án'}
                </TextFont>
              )}
              <Input
                search
                noUnderLine
                placeholder="Tìm tên địa điểm"
                viewStyle={[styles.searchInput, Style.fullWidth]}
                inputStyle={[Style.textWhite]}
                placeholderTextColor={'grey'}
                value={searchTerm}
                onChange={text => setSearchTerm(text)}
                keyboardType="web-search"
                onSubmitEditing={handleSearch}
              />
            </View>
            {props.isLoading ? (
              <Loading />
            ) : (
              !isEmpty(results) && (
                <FlatList
                  keyExtractor={item => item.project_location_id}
                  data={results}
                  contentContainerStyle={[Style.paddingBottom32, Style.paddingHorizontal24]}
                  renderItem={renderItem}
                  extraData={results}
                />
              )
            )}
            <AddNewLocation innerRef={addNewLocation} project_id={projectInfo.project_id} />
            <Filter innerRef={filterModal} onFilter={filterLocation} />
          </DismissKeyboardView>
        </SafeAreaView>
      </AvoidKeyboardView>
    </ImageBackground>
  )
}
