import color, { rgba } from 'variables/color'
import { StyleSheet, Dimensions } from 'react-native'
import { Platform } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('screen').width

export default StyleSheet.create({
  imageLogo: {
    width: SCREEN_WIDTH / 2.2,
    height: SCREEN_WIDTH / 2.2,
  },
  searchInput: {
    backgroundColor: rgba(color.Black, 0.35),
    borderRadius: 10,
    paddingVertical: 2,
  },
  addNewImage: {
    width: 60,
    height: 60,
  },
  cardButton: {
    width: 40,
    height: 40,
  },
  filterIcon: {
    width: 30,
    height: 30,
  },
  filterBtnWrapper: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
})
