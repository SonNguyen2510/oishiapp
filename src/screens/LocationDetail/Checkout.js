import React from 'react'
import { View } from 'react-native'
import { TextFont, Button, Input, InputGroup } from 'components'
import Style from 'variables/style'
import isEmpty from 'lodash/isEmpty'
import { useNavigation } from '@react-navigation/native'
import PhotosList from './PhotosList'
import styles from './styles'
import { Constant } from 'services'

export default function Checkout(props) {
  const {
    renderNote,
    navigateToCamera,
    onChangeInput,
    images,
    totalProduct,
    navigateToInventory,
    role,
    disabled,
  } = props

  const navigation = useNavigation()

  const transformImageURL = data =>
    data.map(({ created_date, check_type, image }) => ({
      uri: image.split('/admin').join(''),
      check_type,
      created_date,
    }))

  // return (
  //   <View>
  //     <View style={Style.paddingBottom12}>
  //       <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom8]}>
  //         <TextFont fontWeight="bold" style={[Style.headline5]}>
  //           Kết quả thực hiện
  //         </TextFont>
  //         <View style={Style.dashedLine} />
  //       </View>
  //       <View style={[Style.paddingTop12, Style.Flex1, Style.flexSpaceBetween]}>
  //         <InputGroup>
  //           <Input
  //             onInputChange={onChangeInput}
  //             inputWrapperStyle={Style.bgWhite}
  //             placeholder="Please enter"
  //             label="Hàng sampling"
  //             labelRight="can"
  //             id="sampling"
  //             keyboardType="numeric"
  //           />
  //           <Input
  //             inputWrapperStyle={Style.bgWhite}
  //             onInputChange={onChangeInput}
  //             placeholder="Please enter"
  //             label="Hàng bán"
  //             labelRight="bottle/can"
  //             id="sale"
  //             keyboardType="numeric"
  //           />
  //           <Input
  //             inputWrapperStyle={Style.bgWhite}
  //             onInputChange={onChangeInput}
  //             placeholder="Please enter"
  //             label="Quà tặng 1"
  //             labelRight="pcs"
  //             id="redemption1"
  //             keyboardType="numeric"
  //           />
  //           <Input
  //             inputWrapperStyle={Style.bgWhite}
  //             onInputChange={onChangeInput}
  //             placeholder="Please enter"
  //             label="Quà tặng 2"
  //             labelRight="pcs"
  //             id="redemption2"
  //             keyboardType="numeric"
  //           />
  //         </InputGroup>
  //       </View>
  //     </View>
  //     {renderNote()}
  //     {!isEmpty(images) && (
  //       <>
  //         <PhotosList
  //           allowAddMore={navigateToCamera}
  //           data={transformImageURL(images.all_images.slice(0, 5))}
  //         />
  //         {images.all_images.length > 5 && (
  //           <View style={[Style.paddingHorizontal16, Style.paddingBottom16, Style.flexAlignCenter]}>
  //             <Button
  //               title={'Xem tất cả ảnh'}
  //               type="white"
  //               onPress={() =>
  //                 navigation.navigate('AllPhotos', { allImages: transformImageURL(images.all_images) })
  //               }
  //             />
  //           </View>
  //         )}
  //       </>
  //     )}
  //   </View>
  // )
  return (
    <View>
      <View style={(Style.paddingBottom12, Style.paddingTop12)}>
        <View style={Style.flexRow}>
          <TextFont fontWeight="bold" style={[Style.headline5]}>
            Hàng tồn kho
          </TextFont>
          <View style={Style.dashedLine} />
        </View>
        <TextFont
          style={[
            Style.bodyText,
            Style.paddingTop4,
            Style.marginBot4,
            { color: 'grey', marginBottom: !totalProduct ? 24 : 0 },
          ]}
        >
          {!totalProduct ? 'Không có hàng cần nhập' : `${totalProduct} SKUs chưa nhập`}
        </TextFont>
        {!totalProduct ? null : (
          <View style={[Style.fullWidth, Style.paddingVertical16]}>
            <Button
              onPress={navigateToInventory}
              title={
                role === Constant.PM || role === Constant.OM
                  ? 'Sửa thông tin hàng tồn kho'
                  : disabled
                  ? 'Đã nhập hàng tồn kho'
                  : `Nhập hàng tồn kho`
              }
              type={role === Constant.PM || role === Constant.OM ? 'white' : 'blue'}
            />
          </View>
        )}
      </View>
      {renderNote()}
      {!isEmpty(images) && (
        <>
          <PhotosList
            allowAddMore={navigateToCamera}
            data={transformImageURL(images.all_images.slice(0, 5))}
          />
          {images.all_images.length > 5 && (
            <View style={[Style.paddingHorizontal16, Style.paddingVertical16, Style.flexAlignCenter]}>
              <Button
                title={'Xem tất cả ảnh'}
                type="white"
                onPress={() =>
                  navigation.navigate('AllPhotos', { allImages: transformImageURL(images.all_images) })
                }
              />
            </View>
          )}
        </>
      )}
    </View>
  )
}
