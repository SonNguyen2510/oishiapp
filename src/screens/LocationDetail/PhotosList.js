import React, { useRef, useState } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { TextFont, PreviewImage } from 'components'
import Style from 'variables/style'
import styles from './styles'
import FastImage from 'react-native-fast-image'

export default function PhotosList(props) {
  const { data = [], allowAddMore, showMarker } = props
  const [imageToShow, setImageToShow] = useState(0)
  const previewModal = useRef()

  const imagesList = data.slice(0, allowAddMore ? 5 : 6)

  const onPressImage = index => {
    setImageToShow(index)
    previewModal?.current.toggle()
  }

  return (
    <>
      <View style={[Style.paddingVertical24]}>
        <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom24]}>
          <TextFont fontWeight="bold" style={[Style.headline5]}>
            {'Hình ảnh tại địa điểm'}
          </TextFont>
          <View style={Style.dashedLine} />
        </View>
        <View style={[Style.FlexWrap, Style.flexRow, Style.paddingLeft12]}>
          {allowAddMore && (
            <TouchableOpacity style={[styles.placeholderWrapper]} onPress={allowAddMore}>
              <Image source={require('images/placeholderCamera.png')} style={[styles.placeholderCamera]} />
            </TouchableOpacity>
          )}
          {imagesList.map((item, index) => {
            return (
              <TouchableOpacity style={[styles.photoWrapper]} key={index} onPress={() => onPressImage(index)}>
                <Image source={item} style={[styles.thumbPhoto]} />
              </TouchableOpacity>
            )
          })}
        </View>
      </View>
      <PreviewImage showMarker={showMarker} innerRef={previewModal} item={data[imageToShow]} />
    </>
  )
}
