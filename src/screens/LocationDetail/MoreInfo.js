import React from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { TextFont, Button } from 'components'
import Style from 'variables/style'
import { Util } from 'services'
import { useNavigation } from '@react-navigation/native'
import PhotosList from './PhotosList'
import isEmpty from 'lodash/isEmpty'
import styles from './styles'
import { Constant } from 'services'

export default function MoreInfo(props) {
  const { images, isClient, activity_info, activity_info_histories, stock_info, stock_info_histories, disabled, navigateToInventory, totalProduct, role } = props
  const { check_in_time, check_out_time, note } = activity_info[0] || {}
  const { internal_sampling, internal_sale, internal_redemption1, internal_redemption2 } =
    stock_info || stock_info_histories || {}

  const navigation = useNavigation()

  const transformImagesList = (data = []) => {
    let imageslist = []
    imageslist = data.map(({ created_date, check_type, image }) => ({
      uri: image.split('/admin').join(''),
      check_type,
      created_date,
    }))
    return imageslist
  }

  const transformedImagesList = transformImagesList(images?.all_images)

  return (
    <View>
      <ActivityTime check_in_time={check_in_time} check_out_time={check_out_time} />
      <View style={[Style.paddingBottom16]}>
        <TextFont style={[Style.textGrey3, Style.paddingBottom8]}>Lịch sử</TextFont>
        {activity_info_histories &&
          activity_info_histories.map((item, idx) => (
            <View key={idx} style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom4]}>
              <TextFont style={[Style.headline5]}>{Util.formatDate(item.date)}</TextFont>
              <TextFont style={[Style.headline5]}>
                {`${Util.formatTime(item.check_in_time)} đến ${Util.formatTime(item.check_out_time)}`}
              </TextFont>
            </View>
          ))}
      </View>
      <StockInfo
        role={role}
        totalProduct={totalProduct}
        navigateToInventory={navigateToInventory}
        disabled={disabled}
        sampling={internal_sampling || 0}
        sale={internal_sale || 0}
        redemption1={internal_redemption1 || 0}
        redemption2={internal_redemption2 || 0}
      />
      {!!note && (
        <View style={Style.paddingBottom32}>
          <View style={[Style.flexRow, Style.flexSpaceBetween]}>
            <TextFont fontWeight="bold" style={[Style.headline5]}>
              Ghi chú trong ngày
            </TextFont>
            <View style={Style.dashedLine} />
          </View>
          <View style={[Style.paddingVertical16]}>
            <TextFont style={[Style.headline5, Style.textGrey3]}>{note}</TextFont>
          </View>
        </View>
      )}
      {!isEmpty(transformedImagesList) && (
        <>
          <PhotosList data={transformedImagesList.slice(0, 6)} />
          {transformedImagesList.length > 6 && (
            <View style={[Style.paddingHorizontal16, Style.paddingBottom24, Style.flexAlignCenter]}>
              <Button
                title={'Xem tất cả ảnh'}
                type="white"
                onPress={() =>
                  navigation.navigate('AllPhotos', { isClient, allImages: transformedImagesList })
                }
              />
            </View>
          )}
        </>
      )}
    </View>
  )
}

export const ActivityTime = ({ check_in_time, check_out_time, isPMFlow, onOpenEditActivityInfo }) => (
  <View style={Style.paddingBottom40}>
    <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom8]}>
      <TextFont fontWeight="bold" style={[Style.headline5]}>
        Thông tin hoạt động
      </TextFont>
      <View style={Style.dashedLine} />
      {isPMFlow && (
        <TouchableOpacity
          onPress={onOpenEditActivityInfo}
          hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
        >
          <Image style={styles.editButton} source={require('assets/images/editButton.png')} />
        </TouchableOpacity>
      )}
    </View>
    <View style={[Style.Flex1, Style.flexRow, Style.paddingTop8]}>
      <View
        style={[Style.bgWhite, Style.Flex1, Style.marginRight8, Style.paddingVertical8, { borderRadius: 10 }]}
      >
        <View style={Style.flexAlignCenter}>
          <TextFont fontWeight="light" style={[Style.note, Style.textGrey3, Style.paddingBottom8]}>
            Giờ check-in
          </TextFont>
          <TextFont fontWeight="medium" style={[Style.headline2]}>
            {Util.formatTime(check_in_time)}
          </TextFont>
        </View>
      </View>
      <View style={[Style.bgWhite, Style.Flex1, Style.paddingVertical8, { borderRadius: 10 }]}>
        <View style={Style.flexAlignCenter}>
          <TextFont fontWeight="light" style={[Style.note, Style.textGrey3, Style.paddingBottom8]}>
            Giờ check-out
          </TextFont>
          <TextFont fontWeight="medium" style={[Style.headline2]}>
            {Util.formatTime(check_out_time)}
          </TextFont>
        </View>
      </View>
    </View>
  </View>
)

// export const StockInfo = ({
//   sampling,
//   sale,
//   redemption1,
//   redemption2,
//   isPMFlow,
//   noEditButton,
//   onOpenEditStockInfo,
// }) => (
//   <View style={[Style.paddingBottom24, Style.bgBlack1]}>
//     <View style={[Style.flexRow, Style.flexSpaceBetween]}>
//       <TextFont fontWeight="bold" style={[Style.headline5]}>
//         Kết quả thực hiện
//       </TextFont>
//       <View style={Style.dashedLine} />
//       {!noEditButton && isPMFlow && (
//         <TouchableOpacity
//           onPress={onOpenEditStockInfo}
//           hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
//         >
//           <Image style={styles.editButton} source={require('assets/images/editButton.png')} />
//         </TouchableOpacity>
//       )}
//     </View>
//     <View style={[Style.Flex1, Style.flexRow, Style.paddingTop8]}>
//       <View style={[Style.Flex1, Style.paddingVertical8]}>
//         <TextFont fontWeight="light" style={[Style.note, Style.textGrey3, Style.paddingBottom4]}>
//           Hàng sampling
//         </TextFont>
//         <TextFont isHTML>
//           <TextFont style={[Style.headline4]}>{sampling}</TextFont>
//           {isPMFlow && (
//             <TextFont textColor="green" style={[Style.headline4]}>
//               {` (${isPMFlow.external_sampling || 0})`}
//             </TextFont>
//           )}
//           <TextFont> can</TextFont>
//         </TextFont>
//       </View>
//       <View style={[Style.Flex1, Style.paddingVertical8]}>
//         <TextFont fontWeight="light" style={[Style.note, Style.textGrey3, Style.paddingBottom4]}>
//           Hàng bán
//         </TextFont>
//         <TextFont isHTML>
//           <TextFont style={[Style.headline4]}>{sale}</TextFont>
//           {isPMFlow && (
//             <TextFont textColor="green" style={[Style.headline4]}>
//               {`(${isPMFlow.external_sale || 0})`}
//             </TextFont>
//           )}
//           <TextFont> bottle/can</TextFont>
//         </TextFont>
//       </View>
//     </View>
//     <View style={[Style.Flex1, Style.flexRow, Style.paddingTop8]}>
//       <View style={[Style.Flex1, Style.paddingVertical8]}>
//         <TextFont fontWeight="light" style={[Style.note, Style.textGrey3, Style.paddingBottom4]}>
//           Quà tặng 1
//         </TextFont>
//         <TextFont isHTML>
//           <TextFont style={[Style.headline4]}>{redemption1}</TextFont>
//           {isPMFlow && (
//             <TextFont textColor="green" style={[Style.headline4]}>
//               {` (${isPMFlow.external_redemption1 || 0})`}
//             </TextFont>
//           )}
//           <TextFont> pcs</TextFont>
//         </TextFont>
//       </View>
//       <View style={[Style.Flex1, Style.paddingVertical8]}>
//         <TextFont fontWeight="light" style={[Style.note, Style.textGrey3, Style.paddingBottom4]}>
//           Quà tặng 2
//         </TextFont>
//         <TextFont isHTML>
//           <TextFont style={[Style.headline4]}>{redemption2}</TextFont>
//           {isPMFlow && (
//             <TextFont textColor="green" style={[Style.headline4]}>
//               {` (${isPMFlow.external_redemption2 || 0})`}
//             </TextFont>
//           )}
//           <TextFont> pcs</TextFont>
//         </TextFont>
//       </View>
//     </View>
//   </View>
// )

export const StockInfo = ({ totalProduct, role, navigateToInventory, disabled }) => (
  <View style={Style.paddingBottom12}>
    <TextFont fontWeight="bold" style={[Style.headline5]}>
      Hàng tồn kho
    </TextFont>
    <TextFont style={[Style.bodyText, Style.paddingTop4, { color: 'grey' }]}>
      {!totalProduct
        ? `Không có hàng ${role === Constant.CL ? 'đã' : 'cần'} nhập`
        : `${totalProduct} SKUs ${role === Constant.SUP ? 'chưa' : 'đã'} nhập`}
    </TextFont>
    {!totalProduct ? null : (
      <View
        style={[
          Style.fullWidth,
          Style.paddingVertical16,
          { color: role === Constant.SUP ? 'white' : 'black' },
        ]}
      >
        <Button
          onPress={navigateToInventory}
          title={
            role === Constant.SUP
              ? disabled
                ? 'Đã nhập hàng tồn kho'
                : `Nhập hàng tồn kho`
              : role === Constant.CL
              ? 'Xem hàng tồn'
              : 'Sửa thông tin hàng tồn kho'
          }
          type={role === Constant.SUP ? 'blue' : 'white'}
        />
      </View>
    )}
  </View>
)
