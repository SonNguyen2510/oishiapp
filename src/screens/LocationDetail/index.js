import { connect } from 'react-redux'
import { userLogout } from 'actions/user'
import { checkIn, checkOut, getLocationsDetailById, removeCheckin, getAllInventory } from 'actions/common'

import LocationDetail from './LocationDetail'

const mapStateToProps = ({ common, user }) => {
  return {
    isLoading: common.toJS().isLoading,
    locationDetail: common.toJS().locationDetail,
    getInventory: common.toJS().getInventory,
    userInfo: user.toJS().userInfo
  }
}

export default connect(
  mapStateToProps,
  { userLogout, checkIn, checkOut, getLocationsDetailById, removeCheckin, getAllInventory },
)(LocationDetail)
