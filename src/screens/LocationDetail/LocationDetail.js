import React, { useRef, useEffect, useState, useLayoutEffect } from 'react'
import {
  View,
  SafeAreaView,
  Image,
  StatusBar,
  ScrollView,
  Platform,
  RefreshControl,
  TouchableOpacity,
  Alert,
} from 'react-native'
import { TextFont, Button, Loading, CheckinCheckOut, Input, AvoidKeyboardView, Inventory } from 'components'
import { Util, Constant } from 'services'
import _ from 'lodash'
import Style from 'variables/style'
import styles from './styles'
import moment from 'moment'
import MoreInfo from './MoreInfo'
import Checkout from './Checkout'

const { STATUS_CLOSED, STATUS_NEW, STATUS_OPENING, GOOGLE_MAPS_KEY } = Constant

const statusMapping = {
  NEW: {
    title: 'Check-in',
    color: 'blue',
    statusTitle: 'Đang hoạt động',
  },
  OPENING: {
    title: 'Check-out',
    color: 'red',
    Action: Checkout,
    statusTitle: 'Đang hoạt động',
  },
  CLOSED: {
    Action: MoreInfo,
    statusTitle: 'Đã hoàn thành',
  },
}

export default function LocationDetail(props) {
  const str = 'ccccc213123coords2123.12312_213123.23123coords'
  var part = str.split('coords').pop().split('coords');
console.log('part',part)
  const {
    locationDetail,
    getLocationsDetailById,
    route,
    removeCheckin,
    getAllInventory,
    getInventory,
    userInfo,
  } = props
  const {
    project_id,
    project_location_id,
    location_name,
    activity_info,
    images,
    activity_info_histories,
    stock_info_histories,
    stock_info,
    is_allow_checkin = true,
  } = locationDetail

  const status =
    activity_info?.activity_status ||
    (images?.checkin_images ? (images?.checkout_images ? STATUS_CLOSED : STATUS_OPENING) : STATUS_NEW)

  const disa = activity_info && activity_info[0] ? activity_info[0].check_out_by : null

  const checkLengthImage = (data = []) => {
    if (data.length > 6) return data.slice(0, 6)
    else if (data.length <= 6) {
      const emptyImage = Array(6 - data.length).fill({ uri: '' })
      return [...data, ...emptyImage]
    }
  }

  const { role } = userInfo
  const [photos, setPhotos] = useState(checkLengthImage(Array(6).fill({ uri: '' })))
  const [note, setNote] = useState('')
  const [refreshing, setRefreshing] = React.useState(false)
  const [checkoutParams, setCheckoutParams] = useState({})
  const [totalProduct, setTotalProduct] = useState(null)
  const [disabled, setDisabled] = useState(false)
  const checkInModal = useRef()
  const inventoryModal = useRef()

  const notEmptyImage = photos.filter(item => item.uri)
  useEffect(() => {
    if (getInventory && getInventory.product_code && getInventory.product_code.length > 0) {
      const data = getInventory.product_code
      setTotalProduct(data.length)
      if (activity_info && activity_info[0]) {
        if (activity_info[0].check_out_by) {
          setDisabled(true)
        }
      }
    }
  }, [getInventory])

  useEffect(() => {
    const { params } = route
    getAllInventory({ project_id, project_location_id, date: Util.getCurrentDate() })
    if (params) {
      params.toggleCheckIn && toggleCheckInPopup()
      getLocationsDetailById({ project_location_id, date: Util.getCurrentDate() })
    }
  }, [route.params])

  useEffect(() => {
    const { params } = route
    if (params?.takenImages) {
      setPhotos(checkLengthImage([...params?.takenImages, ...notEmptyImage]))
    }
  }, [route.params?.takenImages])

  useLayoutEffect(() => {
    if (status === STATUS_OPENING) {
      props.navigation.setOptions({
        headerRight: renderRemoveCheckin,
      })
    } else {
      props.navigation.setOptions({
        headerRight: () => null,
      })
    }
  }, [status])

  const renderRemoveCheckin = props => (
    <TouchableOpacity
      onPress={() =>
        Alert.alert(
          'Xoá Check-in địa điểm này?',
          'Bạn có chắc chắn muốn xoá check-in địa điểm này? Thời gian và hinh ảnh sẽ bị xoá và không thể phục hồi.',
          [
            {
              text: 'Không',
            },
            {
              text: 'Xoá',
              style: 'destructive',
              onPress: handleRemoveCheckin,
            },
          ],
        )
      }
      style={[Style.paddingHorizontal16]}
      hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
      {...props}
    >
      <Image
        style={styles.removeCheckin}
        source={require('assets/images/remove-checkin.png')}
        resizeMode="contain"
      />
    </TouchableOpacity>
  )

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerTitle: renderHeaderTitle,
    })
  }, [])

  const renderHeaderTitle = () => {
    return (
      <TextFont style={Style.headline4} fontWeight="medium">
        {'Chi Tiết Địa Điểm'}
      </TextFont>
    )
  }

  const handleRemoveCheckin = () => {
    const params = {
      project_id,
      project_location_id,
      date: activity_info[0]?.date,
      activity_id: activity_info[0]?.activity_info_id,
      image_ids: images.checkin_images?.map(item => `${item.project_location_image_id}`),
    }
    removeCheckin(params, () => refreshLocationDetail())
  }

  const navigateToCamera = (check_type, isFlowAddMore) => {
    !isFlowAddMore && toggleCheckInPopup()
    props.navigation.navigate('Camera', { check_type, notEmptyImage })
  }

  const toggleCheckInPopup = () => {
    checkInModal.current.toggle()
  }

  const toggleInventoryPopup = () => {
    inventoryModal.current.toggle()
  }

  const onChangeInputCheckout = (id, value) => {
    setCheckoutParams({ ...checkoutParams, [id]: value })
  }

  const refreshLocationDetail = (clearNote, callback = () => {}, isPullToRefresh) => {
    getLocationsDetailById(
      { project_location_id, date: Util.getCurrentDate() },
      () => {
        !isPullToRefresh && setPhotos(checkLengthImage(Array(6).fill({ uri: '' }))), callback()
      },
      isPullToRefresh,
    )
    clearNote && setNote('')
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)
    refreshLocationDetail(false, () => setRefreshing(false), true)
  }, [refreshing])

  const onPressCheckIn = () => {
    const file = getImagesToUpload()
    let params = {
      project_location_id,
      project_id,
      note,
      file,
      location_name: location_name,
      check_in_time: moment().format('hh:mm:ss'),
      date: Util.getCurrentDate('YYYY-MM-DD'),
      check_type: 'checkin',
    }
    toggleCheckInPopup()
    props.checkIn(params, () => refreshLocationDetail(true))
  }

  const onPressCheckOut = () => {
    const file = getImagesToUpload()
    let params = {
      ...checkoutParams,
      file,
      project_location_id,
      project_id,
      note,
      location_name: location_name,
      check_out_time: moment().format('hh:mm:ss'),
      date: Util.getCurrentDate('YYYY-MM-DD'),
      check_type: 'checkout',
    }

    Alert.alert('Thông Báo', 'Bạn có chắc chắn muốn kết thúc ca?', [
      {
        text: 'Không',
      },
      {
        text: 'Có',
        onPress: () => {
          toggleCheckInPopup()
          props.checkOut(params, () => refreshLocationDetail())
        },
      },
    ])
  }

  const getImagesToUpload = () => {
    return photos
      .filter(item => item.uri)
      .map(image => {
        const fileName = image.uri.split('.')
        return {
          fileBase64: image.base64,
          name: `IMG_${Util.getCurrentDate('YYYYMMDD')}_${Util.getCurrentDate('HHMMSS')}_=${image.latitude}_${image.longitude}+.${
            fileName[fileName.length - 1]
          }`,
          type: 'image/jpeg',
          error: null,
        }
      })
  }

  const renderNote = () => (
    <>
      <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingBottom8]}>
        <TextFont fontWeight="bold" style={[Style.headline5]}>
          Ghi chú trong ngày
        </TextFont>
        <View style={Style.dashedLine} />
      </View>
      <View style={[Style.paddingVertical16]}>
        <Input
          noUnderLine
          noLabel
          placeholder="Ghi chú . . ."
          multiline={true}
          numberOfLines={4}
          value={note}
          onChange={text => setNote(text)}
        />
      </View>
    </>
  )
  const { title, color, Action } = statusMapping[status || STATUS_NEW] || {}
  const onPress = status === STATUS_NEW ? onPressCheckIn : onPressCheckOut
  return (
    <SafeAreaView style={[Style.Flex1, Style.bgLightGrey, Style.flexSpaceBetween]}>
      <AvoidKeyboardView style={Style.Flex1}>
        <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
        <ScrollView
          style={[Style.paddingHorizontal16]}
          contentContainerStyle={[Style.flexSpaceBetween, Style.FlexGrow1]}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
        >
          <LocationInfo locationDetail={locationDetail} status={status} />
          {!activity_info?.activity_status &&
            (Action && (
              <Action
                navigateToInventory={toggleInventoryPopup}
                disabled={disabled}
                role={role}
                totalProduct={totalProduct}
                renderNote={renderNote}
                navigateToCamera={() => navigateToCamera('inprogress', true)}
                onChangeInput={onChangeInputCheckout}
                activity_info={activity_info}
                images={images}
                activity_info_histories={activity_info_histories}
                stock_info_histories={stock_info_histories}
                stock_info={stock_info}
              />
            ))}
          {is_allow_checkin &&
            (status != STATUS_CLOSED && (
              <View style={[Style.fullWidth, Style.paddingHorizontal16, Style.paddingVertical16]}>
                <Button title={`Chụp hình ${title}`} type={color} onPress={toggleCheckInPopup} />
              </View>
            ))}
        </ScrollView>
        <CheckinCheckOut
          innerRef={checkInModal}
          data={photos}
          headerTitle={title}
          buttonColor={color}
          navigate={() => navigateToCamera(status === STATUS_NEW ? 'checkin' : 'checkout')}
          onPress={onPress}
          hasImage={!_.isEmpty(notEmptyImage)}
        />
        <Inventory
          disabled={disabled}
          innerRef={inventoryModal}
          closeModal={() => inventoryModal.current.toggle()}
        />
        {props.isLoading && <Loading />}
      </AvoidKeyboardView>
    </SafeAreaView>
  )
}

export const LocationInfo = ({ locationDetail, status }) => {
  const {
    channel,
    location_address,
    end_date,
    end_time,
    start_date,
    start_time,
    location_name,
    location_district,
    location_city,
    location_ward,
  } = locationDetail
  const { statusTitle } = statusMapping[status || 'NEW'] || {}
  return (
    <View style={[Style.paddingTop12]}>
      <Image
        source={{
          uri: `https://maps.googleapis.com/maps/api/staticmap?center=${location_address}, ${location_ward?.name ||
            ''}, ${location_district?.name || ''} ${location_city.name ||
            ''}&zoom=16&size=600x300&maptype=roadmap&key=${GOOGLE_MAPS_KEY}&markers=color:red|${location_address}`,
        }}
        style={[styles.mapImage, Style.fullWidth]}
      />
      <View style={[Style.paddingTop24]}>
        <View style={[Style.flexAlignStart, Style.paddingBottom12]}>
          <Button title={channel} size="badge" borderRadius={5} type="yellow" />
        </View>
        <View style={[Style.paddingBottom16]}>
          <TextFont fontWeight="bold" style={[Style.headline2, Style.paddingBottom4]}>
            {location_name}
          </TextFont>
          <TextFont style={[Style.headline5]}>{`${location_address}, ${location_ward?.name ||
            ''}, ${location_district?.name || ''} \n${location_city.name || ''}`}</TextFont>
        </View>
        <View style={[Style.paddingBottom16]}>
          <TextFont style={[Style.textGrey3, Style.paddingBottom4]}>Giờ hoạt động</TextFont>
          <TextFont style={[Style.headline5]}>
            {`${Util.formatDate(start_date)} - ${Util.formatDate(end_date)}`}
          </TextFont>
          <TextFont style={[Style.headline5]}>
            {`${Util.formatTime(start_time, 'hh:mm A')} - ${Util.formatTime(end_time, 'hh:mm A')}`}
          </TextFont>
        </View>
        <View style={[Style.paddingBottom16]}>
          <TextFont style={[Style.textGrey3, Style.paddingBottom8]}>Trạng thái sự kiện</TextFont>
          <View style={[Style.flexAlignStart, Style.paddingBottom12]}>
            <Button
              title={statusTitle}
              size="badge"
              borderRadius={5}
              type={status === STATUS_CLOSED ? 'darkGrey' : 'green'}
            />
          </View>
        </View>
      </View>
    </View>
  )
}
