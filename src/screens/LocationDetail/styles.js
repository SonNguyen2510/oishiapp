import { StyleSheet, Dimensions } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('screen').width

import color, { rgba } from 'variables/color'

export default StyleSheet.create({
  thumbPhoto: {
    width: SCREEN_WIDTH / 3.8,
    height: SCREEN_WIDTH / 3.8,
    borderRadius: 20,
  },
  photoWrapper: {
    paddingRight: 10,
    paddingVertical: 6,
  },
  placeholderWrapper: {
    paddingRight: 14,
    paddingVertical: 6,
  },
  placeholderCamera: {
    width: SCREEN_WIDTH / 3.8,
    height: SCREEN_WIDTH / 3.8,
    borderRadius: 20,
    backgroundColor: color.White,
  },
  mapImage: {
    width: SCREEN_WIDTH,
    height: SCREEN_WIDTH / 2.8,
    borderRadius: 20,
  },
  editButton: {
    width: 28,
    height: 28,
  },
  emptyImages: {
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
  },
  removeCheckin: {
    width: 28,
    height: 28,
  },
})
