import { StyleSheet, Dimensions } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('screen').width

import color, { rgba } from 'variables/color'

export default StyleSheet.create({
  emptyPhoto: {
    width: SCREEN_WIDTH / 2.4,
    height: (SCREEN_WIDTH * 0.75) / 2.6,
    borderRadius: 8,
  },
  emptyPhotoContainer: {
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  unselectIcon: {
    position: 'absolute',
    bottom: 10,
    right: 15,
    zIndex: 9,
    width: 24,
    height: 24,
  },
  selected: {
    width: 27,
    height: 27,
    bottom: 5,
    right: 14,
  },
  selectedWrapper: {
    backgroundColor: rgba(color.Black, 0.5),
    zIndex: 8,
    borderRadius: 8,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 5,
  },
  emptyList: {
    width: SCREEN_WIDTH / 1.5,
    height: SCREEN_WIDTH,
  },
})
