import React, { useRef, useState } from 'react'
import { SafeAreaView, StatusBar, FlatList, Platform, TouchableOpacity, Image } from 'react-native'
import { PreviewImage } from 'components'
import Style from 'variables/style'
import styles from './styles'
import FastImage from 'react-native-fast-image'

export default function ViewAllPhotos(props) {
  
  const { allImages, showMarker } = (props.isClientFlow && props) || props.route?.params
  const [imageToView, setImageToView] = useState(0)
  const [stateShowMarker, setShowMarker] = useState(false)
  const previewModal = useRef()

  const openImagePreview = index => {
    setImageToView(index)
    previewModal?.current.toggle()
  }

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        key={index}
        style={[styles.emptyPhotoContainer]}
        onPress={() => {
          if (showMarker) {
            const coords = item.uri.substring(item.uri.lastIndexOf('=') + 1, item.uri.lastIndexOf('+')).split('_')
            if (coords[0] && coords[1]) {
              setShowMarker(true)
            }
          }
          openImagePreview(index)
        }}
      >
        <Image source={item} style={[styles.emptyPhoto]} />
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView style={[Style.Flex1, Style.bgWhite, Style.flexSpaceBetween]}>
      <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
      <FlatList
        keyExtractor={(item, idx) => idx}
        data={allImages}
        contentContainerStyle={[Style.paddingVertical16, Style.paddingHorizontal24]}
        numColumns={2}
        renderItem={renderItem}
        extraData={allImages}
        initialNumToRender={4}
        // windowSize={10}
        // removeClippedSubviews={true}
      />
      <PreviewImage showMarker={stateShowMarker} innerRef={previewModal} item={allImages[imageToView]} />
    </SafeAreaView>
  )
}
