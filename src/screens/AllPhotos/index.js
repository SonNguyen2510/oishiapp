import { connect } from 'react-redux'
import ViewAllPhotos from './ViewAllPhotos'

const mapStateToProps = ({ common, user }) => {
  return {
    locationDetail: common.toJS().locationDetail,
    userInfo: user.toJS().userInfo,
  }
}

export default connect(
  mapStateToProps,
  null,
)(ViewAllPhotos)
