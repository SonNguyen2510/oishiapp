import React, { useEffect, useRef } from 'react'
import { View, SafeAreaView, ImageBackground, Image, FlatList, TouchableOpacity } from 'react-native'
import { Notification, TextFont, Card, Button, Loading } from 'components'
import { StockInfo } from 'screens/LocationDetail/MoreInfo'
import { Constant, Util } from 'services'
import isEmpty from 'lodash/isEmpty'
import Style from 'variables/style'
import styles from './styles'

const { NOTIFY_NEW, PM, OM } = Constant

export default function DashBoard(props) {
  const {
    allProjects,
    getDashboardData,
    userLogout,
    navigation,
    isLoading,
    notifications = [],
    updateNotification,
    getLocationsDetailById,
    userInfo,
    getNotification,
    setFilterDate,
  } = props

  const notificationModal = useRef()

  useEffect(() => {
    const allowNotification = userInfo?.role === PM || userInfo?.role === OM
    getDashboardData(allowNotification)
    return () => {
      // cleanup
    }
  }, [])

  const onLogoutPress = () => {
    userLogout({})
  }

  const navigateToProjectDetail = item => {
    // getLocationsById({ project_location_id: 11 }, () =>
    navigation.navigate('ProjectDetail', { projectInfo: item })
    // )
  }

  const navigateToProjectLocationDetail = ({ notification_id, project_location_id, date, type }) => {
    getLocationsDetailById({ project_location_id, date }, () =>
      navigation.navigate('LocationDetail', { type }),
    )
    updateNotification({ notification_id })
    getNotification()
    toggleNotification()
    setFilterDate(Util.formatDate(date, ''))
  }

  const toggleNotification = () => {
    notificationModal.current?.toggle()
  }

  const renderHeader = item => {
    const { name, index } = item
    return (
      <View>
        <TextFont textColor="darkBlue">{`Dự án ${index + 1}`}</TextFont>
        <TextFont textColor="darkBlue" fontWeight="bold" style={[Style.paddingTop8, Style.headline3]}>
          {name}
        </TextFont>
      </View>
    )
  }

  const renderBody = item => {
    const {
      sum_internal_sampling,
      sum_internal_sale,
      sum_internal_redemption1,
      sum_internal_redemption2,
      sum_external_sampling,
      sum_external_sale,
      sum_external_redemption1,
      sum_external_redemption2,
    } = item?.stock_info || {}

    if (userInfo?.role === PM || userInfo?.role === OM) {
      return (
        <View style={[Style.paddingTop24]}>
          <StockInfo
            sampling={sum_internal_sampling || 0}
            sale={sum_internal_sale || 0}
            redemption1={sum_internal_redemption1 || 0}
            redemption2={sum_internal_redemption2 || 0}
            isPMFlow={{
              external_sampling: sum_external_sampling || 0,
              external_sale: sum_external_sale || 0,
              external_redemption1: sum_external_redemption1 || 0,
              external_redemption2: sum_external_redemption2 || 0,
            }}
            noEditButton
          />
        </View>
      )
    } else {
      return <View style={[Style.paddingVertical32]} />
    }
  }

  const renderFooter = item => {
    return (
      <View style={[Style.paddingTop16]}>
        <Button
          size="small"
          type="yellow"
          title={`Có ${item.location_count || 'rất nhiều'} địa điểm trong dự án`}
          borderRadius={5}
        />
      </View>
    )
  }

  const renderItem = ({ item, index }) => {
    return (
      <Card
        header={renderHeader}
        content={renderBody}
        footer={renderFooter}
        data={{ ...item, index }}
        onPress={() => {
          navigateToProjectDetail(item)
        }}
        noBreakLine
      />
    )
  }

  const newNotify = notifications.filter(item => item.status === NOTIFY_NEW)

  return (
    <ImageBackground style={[Style.ImageFullScreen]} source={require('images/bg_dashboard.png')}>
      {/* <StatusBar barStyle="light-content" /> */}
      <SafeAreaView style={[Style.Flex1]}>
        <View style={[Style.CornerTopRight, { zIndex: 1 }, Style.paddingTop12]}>
          <Button onPress={onLogoutPress} title={'Logout'} size="small" type="yellow" />
        </View>

        {(userInfo?.role === PM || userInfo?.role === OM) && (
          <View style={[styles.notificationView, { zIndex: 1 }]}>
            <TouchableOpacity
              onPress={toggleNotification}
              hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
            >
              <Image style={styles.notification} source={require('assets/images/notification.png')} />
              {!isEmpty(newNotify) && (
                <Image style={styles.dot} source={require('assets/images/notification-dot.png')} />
              )}
            </TouchableOpacity>
          </View>
        )}
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={allProjects.projects}
          contentContainerStyle={[Style.paddingHorizontal24, Style.paddingVertical32]}
          ListHeaderComponent={() => (
            <View style={[Style.flexAlignCenter, Style.paddingBottom16, Style.paddingTop56]}>
              <Image resizeMode='contain' style={[styles.imageLogo]} source={require('images/oishi-logo.png')} />
              {/* <TextFont style={[Style.headline1, Style.textWhite]}>{'firstname'}</TextFont>
              <TextFont fontWeight="light" style={[Style.textWhite]}>{`email: ${'email'}`}</TextFont> */}
            </View>
          )}
          renderItem={renderItem}
        />
        <Notification
          innerRef={notificationModal}
          notifications={notifications}
          navigate={navigateToProjectLocationDetail}
        />
        {isLoading && <Loading />}
      </SafeAreaView>
    </ImageBackground>
  )
}
