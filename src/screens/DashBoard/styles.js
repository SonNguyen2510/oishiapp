import { StyleSheet, Dimensions, Platform } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('screen').width

export default StyleSheet.create({
  imageLogo: {
    width: SCREEN_WIDTH / 2.2,
    height: SCREEN_WIDTH / 2.2,
  },
  notification: {
    width: 40,
    height: 40,
  },
  notificationView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 50 : 25,
    left: 20,
  },
  dot: {
    position: 'absolute',
    width: 20,
    height: 20,
    right: 0,
  },
})
