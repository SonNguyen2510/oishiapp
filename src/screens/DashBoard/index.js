import { connect } from 'react-redux'
import { userLogout } from 'actions/user'
import {
  getDashboardData,
  getLocationsById,
  getLocationsDetailById,
  updateNotification,
  getNotification,
  setFilterDate,
} from 'actions/common'
import DashBoard from './DashBoard'

const mapStateToProps = ({ common, user }) => {
  const { isLoading, allProjects, notifications } = common.toJS()
  return {
    userInfo: user.toJS().userInfo,
    isLoading,
    notifications,
    allProjects,
  }
}

export default connect(
  mapStateToProps,
  {
    userLogout,
    getDashboardData,
    getLocationsById,
    getLocationsDetailById,
    updateNotification,
    getNotification,
    setFilterDate,
  },
)(DashBoard)
