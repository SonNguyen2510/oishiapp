import { Api } from 'services'
import endPoints from 'configs/endPoints'
import { Alert } from 'react-native'
export const TOGGLE_LOADING = 'common/TOGGLE_LOADING'
export const SET_ALL_PROJECTS = 'common/SET_ALL_PROJECTS'
export const SET_LOGIN_STATUS = 'common/SET_LOGIN_STATUS'
export const SET_PROJECT_LOCATION_DETAIL = 'common/SET_PROJECT_LOCATION_DETAIL'
export const SET_TAKEN_IMAGES = 'common/SET_TAKEN_IMAGES'
export const RESET_FILTER_DATE = 'common/RESET_FILTER_DATE'
export const SET_MASTER_DATA = 'common/SET_MASTER_DATA'
export const SET_FILTER_DATE = 'common/SET_FILTER_DATE'
export const SET_NOTIFICATION = 'common/SET_NOTIFICATION'
export const SET_POST_INVENTORY= 'common/SET_POST_INVENTORY'
export const SET_GET_INVENTORY= 'common/SET_GET_INVENTORY'

function handleError(error = [], callback = () => {}) {
  Alert.alert(
    'Error',
    `${error[0] || 'Server error'}`,
    [
      {
        text: 'OK',
        onPress: callback,
      },
    ],
    { cancelable: false },
  )
}

function toggleLoading(bool) {
  return {
    type: TOGGLE_LOADING,
    payload: bool,
  }
}

function setLoginStatus(bool) {
  return {
    type: SET_LOGIN_STATUS,
    payload: bool,
  }
}

function setAllProjects(data) {
  return {
    type: SET_ALL_PROJECTS,
    payload: data,
  }
}

function setProjectLocationDetail(data) {
  return {
    type: SET_PROJECT_LOCATION_DETAIL,
    payload: data,
  }
}

function resetFilterDate() {
  return {
    type: RESET_FILTER_DATE,
  }
}

function setMasterData(data) {
  return {
    type: SET_MASTER_DATA,
    payload: data,
  }
}

function setFilterDate(data) {
  return dispatch => {
    dispatch({
      type: SET_FILTER_DATE,
      payload: data,
    })
  }
}

function setNotification(data) {
  return {
    type: SET_NOTIFICATION,
    payload: data,
  }
}

function setPostInventory(data) {
  return {
    type: SET_POST_INVENTORY,
    payload: data,
  }
}

function setGetInventory(data) {
  return {
    type: SET_GET_INVENTORY,
    payload: data,
  }
}

function getDashboardData(allowNotification) {
  return dispatch => {
    Promise.all([dispatch(getAllProjects()), allowNotification && dispatch(getNotification())])
  }
}

function getAllInventory(params, callback = () => {}) {
  return async dispatch => {
    // dispatch(toggleLoading(true))
    try {
      const data = await Api.get(`${endPoints.getProduct}&project_id=${params.project_id}&location_id=${params.project_location_id}&date=${params.date}`)
      dispatch(setGetInventory(data))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error, () => dispatch(setGetInventory(false)))
      dispatch(toggleLoading(false))
    }
  }
}

function postQuantityInventory(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(`${endPoints.postProduct}${params.url}`, params.body)
      dispatch(setPostInventory(data))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error, () => dispatch(setPostInventory(false)))
      dispatch(toggleLoading(false))
    }
  }
}

function getAllProjects(callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.get(endPoints.getProject)
      dispatch(setAllProjects(data))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error, () => dispatch(setLoginStatus(false)))
      dispatch(toggleLoading(false))
    }
  }
}

function getLocationsDetailById(params, callback = () => {}, noLoading) {
  return async dispatch => {
    !noLoading && dispatch(toggleLoading(true))
    try {
      const data = await Api.get(
        `${endPoints.getLocationsDetailById}&project_location_id=${params.project_location_id}&date=${
          params.date
        }`,
      )
      dispatch(setProjectLocationDetail(data))
      // dispatch(setFilterDate(params.date))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function searchLocation(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.get(
        `${endPoints.searchLocation}&project_id=${params.project_id}&search_text=${params.search_text}`,
      )
      callback(data)
      dispatch(toggleLoading(false))
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function filterProjectLocation(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const { project_id, date, start_time, end_time, channel, city } = params || {}
      const data = await Api.get(
        `${
          endPoints.filterProjectLocation
        }&project_id=${project_id}&date=${date}&start_time=${start_time}&end_time=${end_time}&channel=${channel}&city=${city}`,
      )
      callback(data)
      // dispatch(setFilterDate(date))
      dispatch(toggleLoading(false))
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function checkIn(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.checkIn, params)
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function checkOut(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.checkout, params)
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function uploadImage(params, callback = () => {}, noLoading) {
  return async dispatch => {
    !noLoading && dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.uploadImages, params)
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function getProvincesAndChannels(callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const getProvincesData = await Api.get(endPoints.getProvinces)
      const getChannelData = await Api.get(endPoints.getChannel)
      dispatch(setMasterData({ provinces: getProvincesData.provinces, channels: getChannelData.channels }))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function getDistrictsByProvinceId(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.get(`${endPoints.getDistrictsByProvinceId}&province_id=${params.province_id}`)
      dispatch(setMasterData({ districts: data?.districts }))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function getWardByDistrictId(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.get(`${endPoints.getWardByDistrictId}&district_id=${params.district_id}`)
      dispatch(setMasterData({ wards: data?.wars }))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function addProjectLocation(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.addProjectLocation, params)
      // dispatch(setMasterData({ wards: data?.wars }))
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function approveInfo(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.approve, params)
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function getNotification(callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.get(endPoints.getNotification)
      dispatch(toggleLoading(false))
      dispatch(setNotification(data.notifications))
      callback(data)
    } catch (error) {
      // handleError(error, () => dispatch(setLoginStatus(false)))
      dispatch(toggleLoading(false))
    }
  }
}

function updateNotification(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.updateNotification, params)
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function removeCheckin(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.removeCheckin, params)
      dispatch(toggleLoading(false))
      callback(data)
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

export {
  toggleLoading,
  getAllProjects,
  handleError,
  setLoginStatus,
  getLocationsDetailById,
  searchLocation,
  filterProjectLocation,
  uploadImage,
  checkIn,
  checkOut,
  getProvincesAndChannels,
  getDistrictsByProvinceId,
  getWardByDistrictId,
  addProjectLocation,
  resetFilterDate,
  approveInfo,
  getNotification,
  updateNotification,
  getDashboardData,
  removeCheckin,
  setFilterDate,
  postQuantityInventory,
  getAllInventory
}
