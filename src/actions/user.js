import { Api } from 'services'
import endPoints from 'configs/endPoints'
import { toggleLoading, handleError, setLoginStatus, resetFilterDate } from 'actions/common'
import AsyncStorage from '@react-native-community/async-storage'

export const SET_USER_INFO = 'user/SET_USER_INFO'
export const SET_LOGIN_STATUS = 'user/SET_LOGIN_STATUS'

const storeValue = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    console.log('e: ', e)
    // saving error
  }
}

const storeMultiValue = async (firstPair, secondPair) => {
  try {
    await AsyncStorage.multiSet([firstPair, secondPair])
  } catch (e) {
    console.log('e: ', e)
    // saving error
  }
}

const getStoredValue = async key => {
  try {
    const value = await AsyncStorage.multiGet(key)
    if (value !== null) {
      return value
    }
  } catch (e) {
    console.log('(e: ', e)
    // error reading value
  }
}

const removeStoredValue = async (key, key2) => {
  try {
    if (key2) await AsyncStorage.multiRemove([key, key2], () => console.log('Done.multiRemove'))
    else await AsyncStorage.removeItem(key, () => console.log('Done.'))
  } catch (e) {
    console.log('e: ', e)
  }
}

function initOpenApp() {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const stored_value = await getStoredValue(['@token', '@userRole'])
      if (stored_value[0][1]) {
        const reponse = await Api.initToken(stored_value[0][1])
        const refreshed_token = reponse?.access_token
        await storeValue('@token', refreshed_token)
        dispatch(setUserInfo({ role: stored_value[1][1] }))
        dispatch(toggleLoading(false))
        dispatch(setLoginStatus(true))
      } else {
        await Api.initToken()
        dispatch(toggleLoading(false))
        dispatch(setLoginStatus(false))
      }
    } catch (error) {
      handleError(error)
    }
  }
}

function setUserInfo(data) {
  return {
    type: SET_USER_INFO,
    payload: data,
  }
}

function userLogin(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      const data = await Api.post(endPoints.login, params)
      const token = Api.getToken()
      await storeMultiValue(['@token', token], ['@userRole', data.user_group?.name])
      dispatch(setUserInfo({ role: data.user_group?.name }))
      dispatch(setLoginStatus(true))
      dispatch(toggleLoading(false))
      callback()
    } catch (error) {
      if (error[0] === 'User is logged.') dispatch(setLoginStatus(true))
      else handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

function userLogout(params, callback = () => {}) {
  return async dispatch => {
    dispatch(toggleLoading(true))
    try {
      await Api.post(endPoints.logout, params)
      await removeStoredValue('@token', '@userRole')
      dispatch(setLoginStatus(false))
      dispatch(toggleLoading(false))
      dispatch(resetFilterDate())
      callback()
    } catch (error) {
      handleError(error)
      dispatch(toggleLoading(false))
    }
  }
}

export { userLogin, userLogout, initOpenApp }
