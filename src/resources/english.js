export default {
  addressToDeliver: 'Vui lòng chọn khu vực giao hàng',
  timeDependYourArea: 'Thời gian giao hàng tuỳ thuộc vào khu vực của bạn',
  selectDistrict: 'Chọn quận',
  pleaseSelect: 'Vui lòng chọn',
  orderNow: 'Đặt hàng ngay',
  distric1: 'Quận 1',
  distric2: 'Quận 2',
  distric3: 'Quận 3',
  distric4: 'Quận 4',
  distric5: 'Quận 5',
  distric6: 'Quận 6',
  distric7: 'Quận 7',
  distric8: 'Quận 8',
  distric9: 'Quận 9',
  distric10: 'Quận 10',
  distric11: 'Quận 11',
  distric12: 'Quận 12',
  whatYouWantToBuy: 'Hôm nay bạn muốn mua gì?',
  organicVegetable: 'Rau Hữu Cơ',
  organicVegetable1: 'Củ Hữu Cơ',
  organicFruit: 'Quả Hữu Cơ',
  organicEgg: 'Trứng Hữu Cơ',
  organicMeat: 'Thịt Sạch',
  organicSeaFood: 'Hải Sản Sạch',
  newProduct: 'Sản phẩm mới',
  viewAll: 'Xem tất cả',
  discounting: 'Đang giảm giá',
  cart: 'Giỏ hàng',
  numberProduct: '{0} sản phẩm',
  provisionalSum: 'Tạm tính',
  discount: 'Giảm giá',
  deliveryFee: 'Phí giao hàng',
  addDiscountCode: 'Thêm mã giảm giá',
  sum: 'Tổng cộng',
  addMoreNote: 'Thêm ghi chú hoá đơn',
  startPayment: 'Bắt đầu thanh toán',
  shippingAdress: 'Địa chỉ giao hàng',
  infoBuyer: 'Thông tin người nhận hàng',
  name: 'Tên',
  phone: 'Số điện thoại',
  addressGetProduct: 'Địa chỉ nhận hàng',
  province: 'Tỉnh thành',
  district: 'Quận',
  ward: 'Phường',
  numHouse: 'Số nhà',
  streetName: 'Tên đường',
  note: 'Ghi chú',
  placeholderNote: 'Chung cư, số phòng, v.v...',
  continue: 'Tiếp tục',
  shippingMethod: 'Hình thức giao hàng',
  paymentMethod: 'Hình thức thanh toán',
  paymentMethod1: 'Thanh toán bằng thẻ quốc tế Visa, Master Card và JCB',
  paymentMethod2: 'Thanh toán bằng thẻ ATM nội địa',
  paymentMethod3: 'Thanh toán bằng ví MoMo',
  paymentMethod4: 'Thanh toán tiền mặt khi nhận hàng',
  edit: 'Chỉnh sửa',
  confirmInfo: 'Xác nhận thông tin',
  reciever: 'Người nhận hàng',
  address: 'địa chỉ',
  productDes: 'Mô tả sản phẩm',
  protect: 'Bảo quản',
  done: 'Xong',
  payAndOrder: 'Thanh toán và đặt hàng',
  show: 'Show',
  hide: 'Hide',
}
