import VietNam from './vietnam'
import English from './english'
import LocalizedStrings from 'react-native-localization'

const Languages = new LocalizedStrings({
  vn: VietNam,
  en: English,
})

export default Languages
