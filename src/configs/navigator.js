import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { BackButton } from 'components'
import { initOpenApp } from 'actions/user'
import color from 'variables/color'
import { Constant } from 'services'

import Login from 'screens/Login'
import DashBoard from 'screens/DashBoard'
import ProjectDetail from 'screens/ProjectDetail'
import LocationDetail from 'screens/LocationDetail'
import LocationDetailClient from 'screens/LocationDetailClient'
import LocationDetailPM from 'screens/LocationDetailPM'
import AllPhotos from 'screens/AllPhotos'
import GoogleMapImage from 'components/PreviewImage/GoogleMapImage'
import Camera from 'screens/Camera'

const Root = createStackNavigator()

const renderBackButton = props => <BackButton {...props} />

const customHeaderOptions = ({ height, backgroundColor, ...rest }) => ({
  ...rest,
  headerTitleAlign: 'center',
  headerBackTitleVisible: false,
  headerStyle: {
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    backgroundColor: backgroundColor || color.White,
    height,
  },
})

const AppNavigator = ({ isLogin, initOpenApp, userInfo }) => {
  useEffect(() => {
    initOpenApp()
  }, [])

  const userRoute = type => {
    switch (type) {
      case Constant.CL:
        return (
          <Root.Screen
            name="LocationDetail"
            component={LocationDetailClient}
            options={customHeaderOptions({
              height: 110, // Specify the height of your custom header
              headerTitleContainerStyle: { flex: 1 },
              headerBackImage: renderBackButton,
              backgroundColor: color.White,
            })}
          />
        )

      case Constant.SUP:
        return (
          <>
            <Root.Screen
              name="LocationDetail"
              component={LocationDetail}
              options={customHeaderOptions({
                headerBackImage: renderBackButton,
                backgroundColor: color.LightGrey,
              })}
            />
            <Root.Screen name="Camera" component={Camera} options={{ headerShown: false }} />
          </>
        )

      case Constant.PM:
        return (
          <Root.Screen
            name="LocationDetail"
            component={LocationDetailPM}
            options={customHeaderOptions({
              height: 110, // Specify the height of your custom header
              headerTitleContainerStyle: { flex: 1 },
              headerBackImage: renderBackButton,
              backgroundColor: color.White,
            })}
          />
        )

      case Constant.OM:
        return (
          <Root.Screen
            name="LocationDetail"
            component={LocationDetailPM}
            options={customHeaderOptions({
              height: 110, // Specify the height of your custom header
              headerTitleContainerStyle: { flex: 1 },
              headerBackImage: renderBackButton,
              backgroundColor: color.White,
            })}
          />
        )

      case Constant.ADMIN:
        return (
          <>
            <Root.Screen
              name="LocationDetail"
              component={LocationDetailPM}
              options={customHeaderOptions({
                height: 110, // Specify the height of your custom header
                headerTitleContainerStyle: { flex: 1 },
                headerBackImage: renderBackButton,
                backgroundColor: color.LightGrey,
              })}
            />
            <Root.Screen name="Camera" component={Camera} options={{ headerShown: false }} />
          </>
        )
    }
  }

  return (
    <NavigationContainer>
      <Root.Navigator headerMode="float">
        {isLogin ? (
          <>
            <Root.Screen name="DashBoard" component={DashBoard} options={{ headerShown: false }} />
            <Root.Screen name="ProjectDetail" component={ProjectDetail} options={{ headerShown: false }} />
            <Root.Screen name="GoogleMapImage" component={GoogleMapImage} options={{ headerShown: false }} />
            <Root.Screen
              name="AllPhotos"
              component={AllPhotos}
              options={customHeaderOptions({
                headerTitle: 'Tất cả hình ảnh',
                headerBackImage: renderBackButton,
              })}
            />
            {userRoute(userInfo?.role)}
          </>
        ) : (
          <Root.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false, animationTypeForReplace: 'pop' }}
          />
        )}
      </Root.Navigator>
    </NavigationContainer>
  )
}

const mapStateToProps = ({ common, user }) => {
  return {
    isLoading: common.toJS().isLoading,
    isLogin: common.toJS().isLogin,
    userInfo: user.toJS().userInfo,
  }
}

export default connect(
  mapStateToProps,
  { initOpenApp },
)(AppNavigator)
