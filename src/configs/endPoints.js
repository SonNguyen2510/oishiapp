//end point will define here

export default {
  getToken: '/admin/index.php?route=rest/rest_api/gettoken&grant_type=client_credentials',
  getProject: '/admin/index.php?route=rest/project/project',
  getLocationsDetailById: '/admin/index.php?route=rest/project_location/getProjectLocationDetailById',
  checkIn: '/admin/index.php?route=rest/project_location/checkIn',
  searchLocation: '/admin/index.php?route=rest/project_location/getProjectLocationByName',
  uploadImages: '/admin/index.php?route=rest/project_location/uploadImages',
  checkout: '/admin/index.php?route=rest/project_location/checkOut',
  filterProjectLocation: '/admin/index.php?route=rest/project_location/filterprojectlocations',
  login: '/admin/index.php?route=rest/login/login',
  logout: '/admin/index.php?route=rest/logout/logout',
  getProvinces: '/admin/index.php?route=rest/location/getprovinces',
  getDistrictsByProvinceId: '/admin/index.php?route=rest/location/getdistrictsbyprovinceid',
  getWardByDistrictId: '/admin/index.php?route=rest/location/getwarsbydistrictid&district_id=',
  getChannel: '/admin/index.php?route=rest/location/getchannels',
  addProjectLocation: '/admin/index.php?route=rest/project_location/addprojectlocation',
  approve: '/admin/index.php?route=rest/project_location/approve',
  getNotification: '/admin/index.php?route=rest/notification/getNotifications',
  updateNotification: '/admin/index.php?route=rest/notification/update',
  removeCheckin: '/admin/index.php?route=rest/project_location/removeCheckIn',
  getProduct: '/admin/index.php?route=rest/project_product_code/getProjectProductCode',
  postProduct: '/admin/index.php?route=rest/project_product_code/'
}
