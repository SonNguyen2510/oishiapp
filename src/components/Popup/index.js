import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  StatusBar,
  SafeAreaView,
  Modal,
  Platform,
} from 'react-native'
import isBoolean from 'lodash/isBoolean'
import Style from 'variables/style'
import color from 'variables/color'

import styles from './styles'

class Popup extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpened: props.isOpened,
    }
  }

  toggle = flag => {
    this.setState(({ isOpened }) => {
      const openState = isBoolean(flag) ? flag : !isOpened
      return { isOpened: openState }
    })
  }

  onClosePress = () => {
    const { onCloseBtnPress } = this.props

    typeof onCloseBtnPress === 'function' ? onCloseBtnPress() : this.toggle(false)
  }

  render() {
    const { isOpened } = this.state
    const {
      children,
      isScrollable,
      backgroundColor,
      theme,
      modalStyle,
      renderBottomButton,
      animationType,
      hideClose,
      showMarker,
      onPressMarker,
    } = this.props
    const Container = isScrollable ? ScrollView : View
    return (
      <Modal visible={isOpened} onRequestClose={this.toggle} animationType={animationType}>
        {/* <StatusBar translucent /> */}
        <SafeAreaView style={[Style.Flex1, { backgroundColor }, modalStyle]}>
          <Container style={{ flex: 1 }} contentContainerStyle={[Style.FlexGrow1]}>
            {children}
          </Container>
          {showMarker ? (
            <TouchableOpacity
              style={[styles.closeBtn, Style.CornerTopLeft]}
              onPress={this.onClosePress}
              hitSlop={{ top: 20, bottom: 20, right: 20, left: 20 }}
            >
              <Image
                style={[styles.closeIcon, { tintColor: 'grey' }]}
                source={require('assets/images/btnBackBlack.png')}
              />
            </TouchableOpacity>
          ) : null}

          {renderBottomButton && renderBottomButton()}
          {hideClose ? null : (
            <TouchableOpacity
              style={[styles.closeBtn, Style.CornerTopRight]}
              onPress={() => {
                if (showMarker) {
                  this.toggle(false)
                  onPressMarker()
                } else {
                  this.onClosePress()
                }
              }}
              hitSlop={{ top: 20, bottom: 20, right: 20, left: 20 }}
            >
              {showMarker ? (
                <Image style={styles.closeIcon} source={require('assets/images/markerImage.png')} />
              ) : (
                <Image
                  style={styles.closeIcon}
                  source={
                    theme === 'light'
                      ? require('assets/images/removeCamera.png')
                      : require('assets/images/icon-close-dark.png')
                  }
                />
              )}
            </TouchableOpacity>
          )}
        </SafeAreaView>
      </Modal>
    )
  }
}

Popup.propTypes = {
  isOpened: PropTypes.bool,
  isScrollable: PropTypes.bool,
  children: PropTypes.node.isRequired,
  theme: PropTypes.string,
  onCloseBtnPress: PropTypes.func,
  modalStyle: PropTypes.object,
}

Popup.defaultProps = {
  isOpened: false,
  isScrollable: true,
  backgroundColor: color.White,
  theme: 'light',
  animationType: 'slide',
  renderBottomButton: () => {},
}

export default Popup
