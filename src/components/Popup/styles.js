import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
  closeBtn: {
    // position: 'absolute',
    zIndex: 1,
  },
  closeIcon: {
    width: 28,
    height: 28,
  },
  modal: {
    margin: 0,
  },
})
