import { StyleSheet, Dimensions } from 'react-native'
import color, { rgba } from 'variables/color'

const SCREEN_WIDTH = Dimensions.get('screen').width

export default StyleSheet.create({
  loadingContainer: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    zIndex: 100,
  },
  loadingBackground: {
    width: SCREEN_WIDTH / 3,
    height: SCREEN_WIDTH / 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: rgba(color.Black, 0.7),
  },
  loadingCard: {
    backgroundColor: color.White1,
    height: 189.5,
    justifyContent: 'center',
    borderRadius: 3,
  },
})
