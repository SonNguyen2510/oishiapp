import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import Color from 'variables/color'

import styles from './styles'

function Loading(props) {
  const { style, inline, size = 'large', color = Color.EgyptianBlue } = props

  if (inline) {
    return <ActivityIndicator style={style} animating={true} size={size} color={color} />
  }

  return (
    <View style={styles.loadingContainer}>
      <View style={styles.loadingBackground}>
        <ActivityIndicator style={style} animating={true} size={size} color={color} />
      </View>
    </View>
  )
}

export default Loading
