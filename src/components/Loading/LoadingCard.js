import React from 'react'
import { ActivityIndicator, View } from 'react-native'

import color from 'variables/color'

import styles from './styles'

function LoadingCard(props) {
  const { style } = props
  return (
    <View style={styles.loadingCard}>
      <ActivityIndicator style={[style]} animating={true} size="large" color={color.Pink1} />
    </View>
  )
}

export default LoadingCard
