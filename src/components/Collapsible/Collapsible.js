import _ from 'lodash'
import React, { PureComponent } from 'react'
import { View, Image, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native'
import PropTypes from 'prop-types'

import Style from 'variables/style'
import TextFont from 'components/TextFont'
import color, { rgba } from 'variables/color'
import Languages from 'resources/languages'

import CollapsibleContent from './CollapsibleContent'

const styles = StyleSheet.create({
  border: {
    borderBottomColor: rgba(color.Black1, 0.08),
    borderBottomWidth: 1,
  },
  dashBorder: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    height: 2,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: rgba(color.Black1, 0.11),
  },
  borderWhite: {
    borderColor: rgba(color.Grey3, 0.5),
    borderBottomColor: rgba(color.Grey3, 0.5),
  },
  borderLess: {
    borderBottomColor: 'transparent',
    borderColor: 'transparent',
  },
  headerImage: {
    width: 15,
    height: 15,
  },
  header: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 16,
  },
  topMask: {
    height: 1,
    backgroundColor: color.White1,
    position: 'absolute',
    top: -1,
    left: 0,
    right: 0,
  },
  rightMask: {
    width: 1,
    backgroundColor: color.White1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: -1,
  },
  leftMask: {
    width: 1,
    backgroundColor: color.White1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: -1,
  },
})

export default class Collapsible extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      collapsed: _.isBoolean(props.collapsed) ? props.collapsed : true,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (_.isBoolean(nextProps.collapsed)) {
      this.setState({ collapsed: nextProps.collapsed })
    }
  }

  addEvent = () => {
    const { trackLabel, trackCategory } = this.props
    // trackLabel &&
    //   Analytics.logEvent({
    //     ...{ action: 'click', label: this.props.trackLabel },
    //     ...(trackCategory && { category: trackCategory }),
    //   })
  }

  toggle = () => {
    this.setState({ collapsed: !this.state.collapsed }, () => this.addEvent())
  }

  renderArrowIcon = () => {
    const { textWhite, loading, error } = this.props
    const { collapsed } = this.state
    const arrowIcon = collapsed ? require('images/dropdownArrow.png') : require('images/arrow-up.png')
    const arrowIconWhite = collapsed
      ? require('images/dropdownArrowWhite.png')
      : require('images/upArrowWhite.png')

    if (loading) {
      return <ActivityIndicator animating={true} size={'small'} color={color.Grey3} />
    }

    if (error) {
      return (
        <TextFont fontWeight="medium" style={Style.textGrey}>
          {Languages.groupPolicyDocumentError}
        </TextFont>
      )
    }

    return (
      <Image
        resizeMode="contain"
        style={styles.headerImage}
        source={textWhite ? arrowIconWhite : arrowIcon}
      />
    )
  }

  render() {
    const {
      children,
      subCollapsibleHeader,
      headerText,
      headerStyle,
      borderLess,
      customHeader,
      overflowFix,
      dashBorder,
      textWhite,
      loading,
      error,
    } = this.props
    const { collapsed } = this.state

    // const subIcon = collapsed ? require('images/iconPlusSmall.png') : require('images/iconMinusSmall.png')

    return (
      <View>
        {headerText && (
          <TouchableOpacity onPress={loading || error ? () => {} : this.toggle}>
            <View style={[styles.header, overflowFix && { marginHorizontal: overflowFix }, headerStyle]}>
              <TextFont fontWeight="medium" style={[Style.bodyText, textWhite && Style.textWhite]}>
                {headerText}
              </TextFont>
              {this.renderArrowIcon()}
            </View>
          </TouchableOpacity>
        )}
        {!_.isEmpty(subCollapsibleHeader) && (
          <TouchableOpacity onPress={this.toggle}>
            <View style={[styles.header, { paddingVertical: 16 }]}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image resizeMode="contain" style={styles.headerImage} source={subIcon} />
                <View style={{ height: 35, flex: 1 }}>
                  <Image
                    style={{ flex: 1, height: undefined, marginLeft: -50, alignSelf: 'flex-start' }}
                    resizeMode="contain"
                    source={subCollapsibleHeader.image}
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}

        {customHeader && (
          <TouchableOpacity
            onPress={this.toggle}
            style={[{ overflow: 'visible' }, overflowFix && { marginHorizontal: overflowFix }]}
          >
            {_.isFunction(customHeader) ? customHeader({ collapsed }) : customHeader}
          </TouchableOpacity>
        )}
        <CollapsibleContent collapsed={collapsed}>{children}</CollapsibleContent>

        <View
          style={[
            dashBorder ? styles.dashBorder : styles.border,
            overflowFix && { marginHorizontal: overflowFix },
            textWhite && styles.borderWhite,
            borderLess && styles.borderLess,
          ]}
        >
          {!borderLess && dashBorder && <View style={styles.topMask} />}
          {!borderLess && dashBorder && <View style={styles.leftMask} />}
          {!borderLess && dashBorder && <View style={styles.rightMask} />}
        </View>
      </View>
    )
  }
}

Collapsible.defaultProps = {
  overflowFix: 8,
  loading: false,
  error: false,
}

Collapsible.propTypes = {
  children: PropTypes.any,
  overflowFix: PropTypes.number,
  loading: PropTypes.bool,
  error: PropTypes.bool,
}
