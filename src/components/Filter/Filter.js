import React, { useState } from 'react'
import { View } from 'react-native'
import { TextFont, Button, Popup, Dropdown, InputGroup, DateTimeInput } from 'components'
import Style from 'variables/style'
import { Util } from 'services'

export default function Filter(props) {
  const { onFilter, innerRef, masterData, filterDate, setFilterDate } = props

  const [channel, setChannel] = useState('')
  const [city, setCity] = useState('')
  const [date, setDate] = useState(filterDate)
  const [start_time, setStartTime] = useState('')
  const [end_time, setEndTime] = useState('')

  const transformMasterData = (data = []) =>
    data.map(item => ({ value: item.id || item.code, label: item.name }))

  const resetFilter = () => {
    setDate('')
    setStartTime('')
    setEndTime('')
    setChannel('')
    setCity('')
  }

  const onPressApplyFilter = () => {
    const params = {
      city,
      channel,
      date: (date && Util.formatDate(date, 'YYYY-MM-DD')) || '',
      start_time: (start_time && Util.formatTime(start_time, 'HH:mm:ss')) || '',
      end_time: (end_time && Util.formatTime(end_time, 'HH:mm:ss')) || '',
    }
    setFilterDate(date)
    onFilter(params)
  }

  const renderInput = () => {
    return (
      <View style={[Style.paddingTop12, Style.Flex1, Style.flexSpaceBetween]}>
        <InputGroup>
          <DateTimeInput
            label="Thời gian thực hiện"
            onChange={value => setDate(value)}
            mode="date"
            value={date}
          />
          <Dropdown
            onChange={value => setChannel(value)}
            value={channel}
            options={transformMasterData(masterData?.channels)}
            label="Chọn kênh thực hiện"
          />
          <Dropdown
            onChange={value => setCity(value)}
            value={city}
            options={transformMasterData(masterData?.provinces)}
            label="Thành phố"
          />
          <View style={[Style.flexRow, Style.flexSpaceBetween]}>
            <DateTimeInput
              viewStyle={{ width: '46%' }}
              label="Thời gian bắt đầu"
              onChange={value => setStartTime(value)}
              mode="time"
              value={start_time}
            />
            <DateTimeInput
              viewStyle={{ width: '46%' }}
              label="Thời gian kết thúc"
              onChange={value => setEndTime(value)}
              mode="time"
              value={end_time}
            />
          </View>
        </InputGroup>
      </View>
    )
  }

  return (
    <Popup ref={innerRef} theme="dark">
      <View style={[Style.Flex1, Style.flexSpaceBetween, Style.paddingTop16, Style.paddingHorizontal16]}>
        <View style={[Style.flexAlignCenter, Style.paddingVertical16]}>
          <TextFont fontWeight="medium" style={[Style.headline4]}>
            Lọc địa điểm
          </TextFont>
        </View>
        {renderInput()}
        <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingTop32, Style.paddingBottom16]}>
          <Button
            containerViewStyle={{ width: '50%' }}
            title={'Thiết lập lại'}
            type="grey"
            onPress={resetFilter}
          />
          <Button
            containerViewStyle={{ width: '46%' }}
            title={'Áp dụng'}
            type="blue"
            onPress={onPressApplyFilter}
          />
        </View>
      </View>
    </Popup>
  )
}
