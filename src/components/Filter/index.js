import { connect } from 'react-redux'
import { setFilterDate } from 'actions/common'

import Filter from './Filter'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
    masterData: common.toJS().masterData,
    filterDate: common.toJS().filterDate,
  }
}

export default connect(
  mapStateToProps,
  { setFilterDate },
)(Filter)
