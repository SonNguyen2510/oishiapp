import { StyleSheet, Platform, StatusBar } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'

import color from 'variables/color'

export default StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2,
    backgroundColor: color.White,
    shadowOpacity: 0.2,
    shadowRadius: 1,
    shadowColor: color.Black,
    shadowOffset: { height: 2, width: 2 },
    ...ifIphoneX(
      {
        paddingTop: 44,
      },
      {
        paddingTop: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
      },
    ),
  },
  btnOuter: {
    flex: Platform.OS === 'ios' ? 2.1 : 0,
  },
  btnBack: {
    ...Platform.select({
      ios: {
        width: 28,
        height: 28,
        margin: 10,
        marginLeft: 16,
      },
      android: {
        width: 16,
        height: 16,
        margin: 16,
      },
    }),
  },
  headerTxt: {
    flex: 7.7,
    fontSize: 17,
    color: color.DarkGreen2,
    textAlign: Platform.OS === 'ios' ? 'center' : 'left',
  },
  secondaryButtonStyle: {
    flex: 2.2,
  },
  secondaryTextStyle: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: color.White,
    textAlign: 'right',
  },
  secondaryTextStyleWithIcon: {
    paddingRight: 16,
  },
  noIcon: {
    paddingVertical: 16,
  },
  containerNoIcon: {
    paddingHorizontal: 16,
  },
  stickyHeader: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
})
