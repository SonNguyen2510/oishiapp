import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity } from 'react-native'

import color from 'variables/color'
import Style from 'variables/style'
import { TextFont } from 'components'

function TabHeader(props) {
  const { title, date } = props

  return (
    <View>
      <TextFont style={Style.headline4} fontWeight="medium">
        {date}
      </TextFont>
      <TextFont style={[Style.textCenter, Style.noteText, Style.textGrey3, Style.paddingBottom4]}>
        {title}
      </TextFont>
    </View>
  )
}

TabHeader.propTypes = {}

TabHeader.defaultProps = {}

export default TabHeader
