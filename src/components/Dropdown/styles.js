import { StyleSheet, Dimensions } from 'react-native'
import color, { rgba } from 'variables/color'

const SCREEN_HEIGHT = Dimensions.get('screen').height

export default StyleSheet.create({
  wrapper: {
    padding: 16,
    backgroundColor: color.Grey,
    borderRadius: 10,
    height: 60,
  },
  isInGroup: {
    marginBottom: 16,
  },
  // isLastChild: {
  //   borderBottomLeftRadius: 3,
  //   borderBottomRightRadius: 3,
  // },
  // isFirstChild: {
  //   borderTopLeftRadius: 3,
  //   borderTopRightRadius: 3,
  // },
  label: {
    fontSize: 14,
    lineHeight: 14,
    paddingBottom: 5,
    color: rgba(color.Black1, 0.4),
  },
  selectedLabel: {
    color: color.Black1,
  },
  disabled: {
    color: rgba(color.Black1, 0.3),
  },
  value: {
    fontSize: 16,
    lineHeight: 28,
    color: color.Black,
  },
  placeholder: {
    color: rgba(color.Black1, 0.4),
    fontSize: 16,
  },
  content: {
    margin: 0,
    backgroundColor: rgba(color.Black, 0.4),
  },
  avoidKeyboardView: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    backgroundColor: color.White,
  },
  searchContainer: {
    padding: 16,
  },
  searchResultContainer: {
    maxHeight: 288,
  },
  scrollView: {
    width: '100%',
    maxHeight: SCREEN_HEIGHT / 2.5,
    backgroundColor: color.White,
    paddingBottom: 48,
  },
  optionTouchable: {
    paddingHorizontal: 24,
  },
  optionContent: {
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: rgba(color.Grey3, 0.14),
  },
  optionText: {
    fontSize: 16,
    lineHeight: 23,
    color: color.Black1,
  },
  optionSelected: {
    color: color.EgyptianBlue,
  },
  optionGreyOut: {
    color: color.Grey5,
  },
  arrow: {
    width: 14,
    height: 8,
    position: 'absolute',
    right: 0,
    top: 10,
  },
  borderBottom: {
    borderBottomColor: rgba(color.Grey, 0.7),
    borderBottomWidth: 0.5,
  },
  noBorderBottom: {
    borderBottomWidth: 0,
  },
  unitText: {
    color: color.Grey4,
  },
  disabledSelected: {
    color: rgba(color.Black1, 0.4),
  },
  fundResultContainer: {
    maxHeight: 450,
  },
  paging: {
    paddingVertical: 7,
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: rgba(color.Black1, 0.2),
  },
  pagingActive: {
    backgroundColor: color.MidBlue,
  },
  disablePaging: {
    opacity: 0.3,
  },
  pagingWrapper: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: color.White1,
    borderWidth: 1,
    borderColor: color.White1,
  },
  noLabel: {
    marginTop: 18,
  },
  calendar: {
    width: 25,
    height: 25,
    position: 'absolute',
    right: 16,
    top: 18,
  },
  datePicker: {
    width: 34,
    height: 34,
  },
})
