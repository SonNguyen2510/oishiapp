import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity, Image, Alert } from 'react-native'
import { find } from 'lodash'

import Languages from 'resources/languages'
import TextFont from 'components/TextFont'
import Style from 'variables/style'
import Content from './Content'
import styles from './styles'

export default class Dropdown extends React.PureComponent {
  static propTypes = {
    label: PropTypes.string,
    options: PropTypes.array.isRequired,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    customStyle: PropTypes.shape({
      wrapperStyle: PropTypes.object,
      labelStyle: PropTypes.object,
      textStyle: PropTypes.object,
    }),
    customArrowIcon: PropTypes.any,
  }

  static defaultProps = {
    label: '',
    placeholder: Languages.pleaseSelect,
    onChange: () => {},
    onDropdownChange: () => {},
    customStyle: {
      wrapperStyle: {},
      labelStyle: {},
      textStyle: {},
    },
    value: '',
  }

  constructor(props) {
    super(props)

    this.state = {
      value: props.value,
      isOpened: false,
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value !== state.value) {
      return { value: props.value }
    }
    return null
  }

  handleSelect = (value, label) => {
    if (!this.props.disabled) {
      this.setState(
        {
          value,
          isOpened: false,
        },
        () => {
          this.props.onDropdownChange(this.props.id, value, label)
          this.props.onChange(value)
        },
      )
    }
  }

  toggleDropdown = () => {
    const { disabled, isCheckEmpty, textAlertEmpty } = this.props
    if (isCheckEmpty) {
      Alert.alert('', textAlertEmpty)
    } else {
      if (!disabled) {
        this.setState({ isOpened: !this.state.isOpened })
      }
    }
  }

  render() {
    const { value, isOpened } = this.state
    const {
      label,
      options,
      isInGroup,
      isFirstChild,
      isLastChild,
      placeholder,
      disabled,
      customStyle,
      customArrowIcon,
      disableSelected,
    } = this.props
    const { wrapperStyle, labelStyle, textStyle, viewStyle } = customStyle
    const selectedLabel = find(options, option => option.value === value)

    return (
      <View
        style={[
          isInGroup && styles.borderBottom,
          isLastChild && styles.noBorderBottom,
          Style.paddingTop16,
          !label && styles.noLabel,
          viewStyle,
        ]}
      >
        {!!label && (
          <TextFont style={[styles.label, selectedLabel && styles.selectedLabel, labelStyle]}>
            {label}
          </TextFont>
        )}
        <View
          style={[
            styles.wrapper,
            isFirstChild && styles.isFirstChild,
            isLastChild && styles.isLastChild,
            isInGroup && styles.isInGroup,
            wrapperStyle,
          ]}
        >
          <TouchableOpacity onPress={this.toggleDropdown}>
            <View>
              <TextFont
                style={[
                  styles.value,
                  !selectedLabel && styles.placeholder,
                  disabled && styles.disabled,
                  textStyle,
                ]}
              >
                {selectedLabel ? selectedLabel.label : placeholder}
              </TextFont>
              <Image
                style={styles.arrow}
                source={customArrowIcon || require('assets/images/dropdownArrow.png')}
              />
            </View>
          </TouchableOpacity>
        </View>
        <Content
          isOpened={isOpened}
          options={options}
          disableSelected={disableSelected}
          selectedValue={value}
          onSelect={this.handleSelect}
          toggleDropdown={this.toggleDropdown}
        />
      </View>
    )
  }
}
