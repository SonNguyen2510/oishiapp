import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Platform,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  Image,
  StatusBar,
} from 'react-native'
import { TextFont } from 'components'
import DateTimePicker from '@react-native-community/datetimepicker'
import styles from './styles'
import Style from 'variables/style'
import { Util } from 'services'

const DateTimeInput = props => {
  const {
    label,
    onChange = () => {},
    value,
    mode,
    viewStyle = {},
    backgroundColor,
    editable = true,
    iconOnly,
  } = props

  const dateTimeFormat = (value, format) =>
    mode === 'date' ? Util.formatDate(value, format) : Util.formatTime(value, format)

  const formatedValue = value ? new Date(value) : new Date()
  const [date, setDate] = useState(formatedValue)
  const [show, setShow] = useState(false)

  const onChangeDateTime = (event, selectedDate) => {
    const currentDate = selectedDate || date
    if (event.type === 'set' && Platform.OS === 'android') {
      setShow(!show)
      onChange(currentDate)
    }
    if (event.type === 'dismissed' && Platform.OS === 'android') {
      setShow(false)
    }
    setDate(currentDate)
  }

  const toggleModal = () => {
    setShow(!show)
    onChange(date)
  }

  const renderDateTimePicker = () => {
    if (Platform.OS === 'android' && show) {
      return (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="spinner"
          locale="es-ES"
          onChange={onChangeDateTime}
        />
      )
    }

    if (Platform.OS === 'ios' && show) {
      return (
        <Modal visible={Platform.OS === 'ios' && show} transparent>
          <View style={[Style.Flex1, Style.flexJustifyEnd, styles.content]}>
            <TouchableWithoutFeedback style={[styles.content]} onPress={toggleModal}>
              <View style={Style.Flex1} />
            </TouchableWithoutFeedback>
            <View style={styles.scrollView}>
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                locale="vi-VN"
                onChange={onChangeDateTime}
              />
            </View>
          </View>
        </Modal>
      )
    }
  }

  const Component = editable ? TouchableOpacity : View

  return (
    <>
      <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
      {iconOnly ? (
        <TouchableOpacity
          onPress={() => setShow(!show)}
          style={[Style.paddingHorizontal16, viewStyle]}
          hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
          {...props}
        >
          <Image
            style={styles.datePicker}
            source={require('assets/images/calendar-picker-icon.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
      ) : (
        <View style={[Style.paddingVertical16, viewStyle]}>
          <TextFont style={[styles.label, value && styles.selectedLabel]}>{label}</TextFont>
          <Component onPress={() => setShow(!show)}>
            <View style={[styles.wrapper, backgroundColor && { backgroundColor }]}>
              <TextFont style={[styles.value, (!value || !editable) && styles.placeholder]}>
                {dateTimeFormat(date)}
              </TextFont>
            </View>
            {mode === 'date' && (
              <Image style={styles.calendar} source={require('assets/images/Calendar.png')} />
            )}
          </Component>
        </View>
      )}
      {renderDateTimePicker()}
    </>
  )
}

DateTimeInput.propTypes = {
  children: PropTypes.any,
  value: PropTypes.any,
  onChange: PropTypes.func,
  mode: PropTypes.oneOf(['time', 'date']),
}

export default DateTimeInput
