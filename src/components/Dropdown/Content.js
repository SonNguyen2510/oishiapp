import React from 'react'
import PropTypes from 'prop-types'
import { View, FlatList, TouchableOpacity, Modal, TouchableWithoutFeedback } from 'react-native'
import Languages from 'resources/languages'
import Style from 'variables/style'
import color from 'variables/color'
import TextFont from 'components/TextFont'

import styles from './styles'

class Content extends React.PureComponent {
  renderOption = ({ item }) => {
    const { selectedValue, onSelect, disableSelected } = this.props
    const { label, value } = item
    return (
      <TouchableOpacity
        style={styles.optionTouchable}
        onPress={() => (disableSelected ? {} : onSelect(value, label))}
      >
        <View style={styles.optionContent}>
          <TextFont
            style={[
              styles.optionText,
              value === selectedValue && styles.optionSelected,
              disableSelected && styles.disabledSelected,
            ]}
          >
            {label}
          </TextFont>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { options, isOpened, toggleDropdown } = this.props

    return (
      <Modal visible={isOpened} onRequestClose={toggleDropdown} transparent animationType="fade">
        <View style={[Style.Flex1]}>
          <TouchableWithoutFeedback accessibilityRole="button" style={[Style.Flex1]} onPress={toggleDropdown}>
            <View style={[Style.Flex1, styles.content]} />
          </TouchableWithoutFeedback>
          <View style={[styles.scrollView]}>
            <FlatList
              data={options}
              renderItem={this.renderOption}
              keyExtractor={(item, index) => `${index}`}
            />
          </View>
        </View>
      </Modal>
    )
  }
}

Content.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
  ).isRequired,
  selectedValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onSelect: PropTypes.func.isRequired,
}

export default Content
