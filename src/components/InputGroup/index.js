import _ from 'lodash'
import React from 'react'
import { View } from 'react-native'

function InputGroup({ children, style }) {
  const validChilds = _.compact(children)
  const childCount = React.Children.count(validChilds)
  return (
    <View style={style}>
      {React.Children.map(validChilds, (comp, index) =>
        React.cloneElement(comp, {
          isInGroup: true,
          isFirstChild: index === 0,
          isLastChild: index === childCount - 1,
        }),
      )}
    </View>
  )
}

InputGroup.defaultProps = {
  style: {},
}

export default InputGroup
