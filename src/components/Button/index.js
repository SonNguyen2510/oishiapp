import React from 'react'
import { TouchableHighlight, View, Animated } from 'react-native'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { TextFont } from 'components'
import color from 'variables/color'

const sizeMapping = {
  big: [16, 'medium', 14, 32],
  medium: [14, 'medium', 10, 24],
  small: [14, 'regular', 6, 14],
  calculate: [20, 'medium', 6, 14],
  badge: [14, 'medium', 2, 10],
}

const typeMapping = {
  darkGrey: [color.DarkGrey, color.White1],
  blue: [color.EgyptianBlue, color.White1],
  white: [color.White, color.Black1],
  red: [color.Red, color.White1],
  grey: [color.Grey, color.Black],
  yellow: [color.Yellow1, color.Black1],
  green: [color.Green, color.White],
  transparent: [color.Trans, color.DarkGreen],
}

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableHighlight)

export let WrapComp

export default class StyledButton extends React.PureComponent {
  handlePress = e => {
    this.props.onPress(e)
  }

  onPress = _.throttle(this.handlePress, this.props.throttleTime, { trailing: false })

  render() {
    const {
      type,
      size,
      borderRadius,
      fitText,
      buttonStyle,
      containerViewStyle,
      title,
      animated,
      disabled,
    } = this.props
    const [fontSize, fontWeight, paddingVertical, paddingHorizontal] = sizeMapping[size]
    const [backgroundColor, color] = typeMapping[disabled ? 'grey' : type]

    WrapComp = animated ? AnimatedTouchable : TouchableHighlight

    return (
      <WrapComp
        style={[fitText && { marginLeft: 0, marginRight: 0 }, containerViewStyle]}
        onPress={disabled ? null : this.onPress}
        underlayColor="transparent"
      >
        <View
          style={[
            {
              paddingVertical,
              paddingHorizontal,
              borderRadius: borderRadius,
              backgroundColor,
            },
            fitText && { alignSelf: 'flex-start' },
            buttonStyle,
          ]}
        >
          <TextFont
            fontWeight={fontWeight}
            style={{
              textAlign: 'center',
              color,
              fontSize,
            }}
          >
            {title}
          </TextFont>
        </View>
      </WrapComp>
    )
  }
}

StyledButton.propTypes = {
  type: PropTypes.string,
  size: PropTypes.string,
  fitText: PropTypes.bool,
}

StyledButton.defaultProps = {
  type: 'green',
  size: 'big',
  borderRadius: 50,
  throttleTime: 2000,
  onPress: () => {},
}
