import React, { Component } from 'react'
import { TextInput, Platform } from 'react-native'
// import VMasker from 'vanilla-masker'

const input = (() => {
  let increment = 0
  const invisibleCharsArr = [
    String.fromCharCode(28),
    String.fromCharCode(29),
    String.fromCharCode(30),
    String.fromCharCode(31),
  ]

  return {
    getChar: () => {
      increment += 1
      const mod = increment % invisibleCharsArr.length
      return invisibleCharsArr[mod]
    },
  }
})()

export default class MaskedInput extends Component {
  constructor(props) {
    super(props)
    const { value = '' } = this.props
    this.state = { value }
  }

  onChangeText = e => {
    const { mask = '', clientIdValidation } = this.props
    // const maskedValue = VMasker.toPattern(e.nativeEvent.text, mask)

    // const prefixedValue = Platform.OS === 'ios' && clientIdValidation ? input.getChar() + maskedValue : maskedValue
    // e.nativeEvent.text = prefixedValue
    // this.setState({ value: prefixedValue })
    this.props.onChange(e)
  }

  render() {
    return (
      <TextInput
        {...this.props}
        ref={this.props.innerRef}
        value={this.state.value}
        onChange={this.onChangeText}
      />
    )
  }
}
