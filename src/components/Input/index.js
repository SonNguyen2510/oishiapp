import React from 'react'
import PropTypes from 'prop-types'
import { View, TextInput, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'

import TextFont from 'components/TextFont'
import Languages from 'resources/languages'
import color, { rgba } from 'variables/color'
import Style from 'variables/style'

// import TextInputMask from './TextInputMask'
import CalculateGroup from './CalculateGroup'
import styles from './styles'

class Input extends React.Component {
  constructor(props) {
    super(props)

    const { defaultValue, value } = props
    this.state = {
      isFocus: false,
      hasValue: defaultValue || value,
      showSecure: props.isPassword ? true : false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.autoUpdateValue) {
      const { defaultValue, value } = nextProps
      this.setState({
        hasValue: defaultValue || value,
      })
    }
  }

  focus = () => {
    this.input.focus()
  }

  blur = () => {
    this.input.blur()
  }

  toggleSecure = () => {
    this.setState({ showSecure: !this.state.showSecure })
  }

  handleChange = e => {
    if (this.props.disabled) return
    const { onChange, onInputChange, id, index, onIndexInputChange } = this.props
    const text = e.nativeEvent.text
    onIndexInputChange && onIndexInputChange(index, id, text)
    onInputChange && onInputChange(id, text)
    onChange && onChange(text)
  }

  handleChangeCalculator = value => {
    const { onInputChange, id } = this.props
    onInputChange && onInputChange(id, value)
  }

  handleFocus = e => {
    this.props.onFocus(e)

    this.setState({ isFocus: true })
  }

  handleEndEditing = e => {
    this.props.onEndEditing(e)

    this.setState({
      isFocus: false,
      hasValue: !!e.nativeEvent.text,
    })
  }

  handleInfoTouch = e => {
    const { isPassword, onInfoTouch } = this.props

    if (isPassword) {
      this.toggleSecure()
    }

    onInfoTouch(e)
  }

  handleEnterClick = () => {
    const { onEnterClick } = this.props
    onEnterClick && onEnterClick(this.input._lastNativeText)
  }

  render() {
    const { isFocus, hasValue, showSecure } = this.state
    const {
      label,
      editable,
      isInGroup,
      isFirstChild,
      isLastChild,
      viewStyle,
      secureTextEntry,
      isPassword,
      isInRow,
      isInFirstRow,
      isInLastRow,
      isFirstChildInRow,
      isLastChildInRow,
      children,
      infoText,
      mask,
      search,
      onCancelClick,
      onEnterClick,
      labelLeft,
      labelRight,
      calculate,
      noUnderLine,
      inputStyle,
      placeholderTextColor,
      multiline,
      numberOfLines = 1,
      noLabel,
      inputWrapperStyle,
      iconStyle,
      ...otherProps
    } = this.props

    const textAreaHeight = numberOfLines * 30

    return (
      <TouchableWithoutFeedback onPress={this.focus}>
        <View
          style={[
            isInGroup && styles.borderBottom,
            isLastChild && styles.noBorder,
            isInRow && styles.borderLeft,
            isFirstChildInRow && styles.noBorderLeft,
            isInLastRow && styles.noBorderBottom,
            isFirstChildInRow && isInLastRow && styles.noBorder,
            !noUnderLine && isFocus && { paddingBottom: -3 },
            viewStyle,
          ]}
        >
          {!noLabel &&
            (!search && (
              <TextFont
                style={[
                  styles.label,
                  !(isFocus || hasValue) && styles.labelHasValue,
                  !editable && styles.disable,
                ]}
              >
                {label}
              </TextFont>
            ))}
          <View
            style={[
              styles.wrapper,
              isInGroup && styles.isInGroup,
              isInRow && styles.isInRow,
              isFirstChild && styles.isFirstChild,
              isLastChild && styles.isLastChild,
              isInFirstRow && isLastChildInRow && styles.lastChildInFirstRow,
              isInFirstRow && isFirstChildInRow && styles.firstChildInFirstRow,
              isInLastRow && isLastChildInRow && styles.lastChildInLastRow,
              isInLastRow && isFirstChildInRow && styles.firstChildInLastRow,
              !noUnderLine && isFocus && styles.isFocus,
              search && styles.searchWrapper,
              multiline && styles.textAreaWrapper,
              multiline && { height: textAreaHeight },
              inputWrapperStyle,
            ]}
          >
            {search && (
              <Image
                resizeMode="contain"
                style={[styles.searchIcon, iconStyle]}
                source={require('images/ic_search.png')}
              />
            )}
            {onCancelClick && (
              <TouchableOpacity
                style={styles.searchCancel}
                onPress={onCancelClick}
                hitSlop={{ top: 13, left: 10, right: 16, bottom: 13 }}
              >
                <TextFont fontWeight="medium" style={Style.bodyText}>
                  {Languages.cancel}
                </TextFont>
              </TouchableOpacity>
            )}
            {onEnterClick && (
              <TouchableOpacity
                style={styles.searchCancel}
                onPress={this.handleEnterClick}
                hitSlop={{ top: 13, left: 10, right: 16, bottom: 13 }}
              >
                <TextFont fontWeight="medium" style={Style.bodyText}>
                  {Languages.enter}
                </TextFont>
              </TouchableOpacity>
            )}
            {mask ? null : (
              <TextInput
                ref={node => (this.input = node)}
                {...otherProps}
                editable={editable}
                style={[
                  styles.input,
                  inputStyle,
                  !editable && styles.disable,
                  labelRight && { width: '80%' },
                  search && styles.searchInput,
                  (hasValue && labelLeft) || (isFocus && labelLeft) || (!editable && labelLeft)
                    ? styles.hasTextLeft
                    : null,
                ]}
                onFocus={this.handleFocus}
                multiline={multiline}
                onChange={this.handleChange}
                onEndEditing={this.handleEndEditing}
                secureTextEntry={secureTextEntry || showSecure}
                placeholderTextColor={placeholderTextColor || rgba(color.Black1, 0.4)}
              />
            )}
            {children}
            {calculate && (
              <CalculateGroup
                onChange={this.handleChangeCalculator}
                basePoint={calculate}
                value={otherProps.value}
                style={styles.calculateGroup}
              />
            )}
            {/* {!editable && !calculate && (
              <Image style={styles.disableIcon} source={require('images/disableInput.png')} />
            )} */}
            {!!infoText && (
              <TouchableOpacity style={styles.infoTouchable} onPress={this.handleInfoTouch}>
                <TextFont style={styles.infoText}>{infoText}</TextFont>
              </TouchableOpacity>
            )}
            {isPassword && (
              <TouchableOpacity style={styles.infoTouchable} onPress={this.handleInfoTouch}>
                <TextFont uppercase style={styles.infoText}>
                  {showSecure ? Languages.show : Languages.hide}
                </TextFont>
              </TouchableOpacity>
            )}
            {labelRight && (
              <View style={[styles.explainRight, !editable ? styles.editableHasTextRight : null]}>
                <TextFont style={[styles.explainText, (isFocus || hasValue) && { color: color.Black }]}>
                  {labelRight}
                </TextFont>
              </View>
            )}
            {(hasValue && labelLeft) || (isFocus && labelLeft) || (!editable && labelLeft) ? (
              <View style={styles.explainLeft}>
                <TextFont style={styles.explainText}>{labelLeft}</TextFont>
              </View>
            ) : null}
          </View>
          {/* {!noUnderLine &&
            (isFocus && (
              <View
                style={[
                  {
                    backgroundColor: color.EgyptianBlue,
                    height: 3,
                    borderBottomLeftRadius: 10,
                    borderBottomRightRadius: 10,
                  },
                  isInGroup && { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 },
                  isLastChild && { borderBottomLeftRadius: 3, borderBottomRightRadius: 3 },
                  isInLastRow && isFirstChildInRow && { borderBottomLeftRadius: 3 },
                  isInLastRow && isLastChildInRow && { borderBottomRightRadius: 3 },
                ]}
              />
            ))} */}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

Input.defaultProps = {
  label: '',
  autoCorrect: false,
  autoCapitalize: 'none',
  editable: true,
  infoText: '',
  viewStyle: {},
  underlineColorAndroid: 'transparent',
  search: false,
  onInfoTouch: () => {},
  onInputChange: () => {},
  onIndexInputChange: () => {},
  onChange: () => {},
  onFocus: () => {},
  onEndEditing: () => {},
  calculate: false,
}

Input.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  editable: PropTypes.bool,
  infoText: PropTypes.string,
  onInfoTouch: PropTypes.func,
  onChange: PropTypes.func,
  onInputChange: PropTypes.func,
  onIndexInputChange: PropTypes.func,
  viewStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  isInGroup: PropTypes.bool,
  isFirstChild: PropTypes.bool,
  isLastChild: PropTypes.bool,
  isInRow: PropTypes.bool,
  isInLastRow: PropTypes.bool,
  isFirstChildInRow: PropTypes.bool,
  isLastChildInRow: PropTypes.bool,
  noUnderLine: PropTypes.bool,
  calculate: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
}

export default Input
