import React from 'react'
import { View } from 'react-native'

import { Button } from 'components'
import Style from 'variables/style'

const CalculateGroup = props => {
  const { basePoint, onChange, value: baseValue, style: styleWrapper } = props

  const BaseButton = (props) => (
    <Button 
      size="calculate"
      type="blue"
      borderRadius={0}
      {...props}
    />
  )

  const handleChange = diffValue => {
    const newValue = parseInt(baseValue) + parseInt(diffValue);
    if (newValue < 0) return;
    onChange(newValue)
  }

  return (
    <View style={[Style.Flex1, styleWrapper]}>
      <BaseButton title="−" onPress={() => handleChange(-basePoint)} />
      <BaseButton title="+" onPress={() => handleChange(basePoint)} />
    </View>
  )
}

export default CalculateGroup
