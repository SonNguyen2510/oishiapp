import { StyleSheet } from 'react-native'

import color, { rgba } from 'variables/color'
import common from 'variables/common'

export default StyleSheet.create({
  wrapper: {
    padding: 15,
    backgroundColor: color.Grey,
    borderRadius: 10,
    height: 60,
  },
  searchWrapper: {
    paddingLeft: 44,
    paddingRight: 56,
    height: 50,
    backgroundColor: rgba(color.Black1, 0.09),
  },
  searchIcon: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: 12.5,
    left: 12,
  },
  searchCancel: {
    position: 'absolute',
    top: 13,
    right: 16,
  },
  isInGroup: {
    marginBottom: 16,
  },
  isFocus: {
    paddingBottom: -3,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomWidth: 3,
    borderColor: color.EgyptianBlue,
  },
  label: {
    fontSize: 14,
    lineHeight: 14,
    // color: color.EgyptianBlue,
    paddingBottom: 5,
    paddingLeft: 5,
  },
  labelHasValue: {
    color: rgba(color.Black1, 0.4),
  },
  input: {
    width: '100%',
    height: 35,
    color: color.Black,
    padding: 0,
    fontSize: 16,
    fontFamily: common.fontRegular,
  },
  searchInput: {
    height: 24,
  },
  hasTextLeft: {
    paddingLeft: 35,
  },
  disable: {
    color: rgba(color.Black1, 0.4),
  },
  disableIcon: {
    width: 14,
    height: 14,
    position: 'absolute',
    right: 19,
    top: 25,
  },
  calculateGroup: {
    height: '100%',
    position: 'absolute',
    right: 0,
  },
  infoText: {
    fontSize: 14,
    lineHeight: 22,
    color: color.EgyptianBlue,
  },
  explainText: {
    fontSize: 14,
    lineHeight: 22,
    color: rgba(color.Black1, 0.4),
  },
  explainRight: {
    position: 'absolute',
    right: 19,
    top: 19,
  },
  editableHasTextRight: {
    right: 40,
  },
  explainLeft: {
    position: 'absolute',
    left: 16,
    top: 31,
  },
  infoTouchable: {
    position: 'absolute',
    right: 19,
    top: 21,
  },
  textAreaWrapper: {
    backgroundColor: color.White,
  },
  noBorderBottom: { borderBottomWidth: 0 },
  noBorderLeft: { borderLeftWidth: 0 },
  noBorder: { borderWidth: 0, borderLeftWidth: 0, borderBottomWidth: 0 },
  borderBottom: { borderBottomColor: rgba(color.Grey, 0.7), borderBottomWidth: 0.5 },
  borderRight: { borderRightColor: rgba(color.Grey, 0.7), borderRightWidth: 0.5 },
  borderLeft: { borderLeftColor: rgba(color.Grey, 0.7), borderLeftWidth: 0.5 },
})
