import { ceil } from 'lodash'
import { StyleSheet, Dimensions } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('screen').width

import color from 'variables/color'

export default StyleSheet.create({
  emptyPhoto: {
    width: SCREEN_WIDTH / 2.6,
    height: (SCREEN_WIDTH * 0.75) / 2.6,
    borderRadius: 8,
  },
  emptyPhotoContainer: {
    paddingHorizontal: 10,
    paddingVertical: 6,
  },
  iconBack: {
    height: 24,
    width: 24,
  },
  button: {
    flex: 1,
    paddingHorizontal: 24,
  },
  icon: {
    height: 40,
    width: 40,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#DCDCE0',
  },
  wrap: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: '#cecece',
    width: '100%',
    flexDirection: 'row',
  },
  input: {
    fontSize: 16,
    fontWeight: '500',
    color: 'black',
  },
  buttonRole: {
    height: 40,
    backgroundColor: color.EgyptianBlue,
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  bottomButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: Dimensions.get('screen').height / 14,
  },
  iconClose: { height: 22, width: 22, marginTop: 2 },
})
