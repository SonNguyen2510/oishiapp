import React, { useState, useRef, useEffect } from 'react'
import { Image, View, TouchableOpacity, FlatList, TextInput, SafeAreaView } from 'react-native'
import { TextFont, Loading, Popup, Input, AvoidKeyboardView, DismissKeyboardView } from 'components'
import Style from 'variables/style'
import styles from './styles'
import moment from 'moment'
import color from 'variables/color'
import { Util } from 'services'
import { isIphoneX } from 'react-native-iphone-x-helper'
import { Constant } from 'services'

export default function Inventory(props) {
  const {
    innerRef,
    closeModal,
    getInventory,
    userInfo,
    locationDetail,
    postInventory,
    postQuantityInventory,
    getAllInventory,
    disabled,
  } = props
  const [search, setSearch] = useState('')
  const [total, setTotal] = useState([])
  const [data, setData] = useState([])
  const [selectRole, setSelectRole] = useState(1)
  const [onPress, setOnPress] = useState(false)
  const [isSaved, setIsSave] = useState(false)
  const inputRef = useRef([])
  const [disabledUpdate, setDisabledUpdate] = useState(false)
  const { project_id, project_location_id } = locationDetail
  const { role } = userInfo

  useEffect(() => {
    if (postInventory && postInventory.message && postInventory.message === 'Updated is done.' && onPress) {
      setOnPress(false)
      if (role === Constant.SUP) {
        closeModal()
      } else {
        setDisabledUpdate(true)
      }
      setTimeout(() => {
        getAllInventory({ project_id, project_location_id, date: Util.getCurrentDate() })
      }, 500)
    }
  }, [postInventory])

  useEffect(() => {
    if (getInventory && getInventory.product_code) {
      const products = getInventory.product_code
      for (const i in products) {
        if (products[i] && products[i].details[0] && products[i].details[0].internal_quantity) {
          setIsSave(true)
        }
      }
      setTotal(products)
      setData(products)
    }
  }, [getInventory])

  const functionSearch = text => {
    if (text) {
      const tempData = []
      for (const i in total) {
        if (total[i].product_code.includes(text.toUpperCase())) {
          tempData.push(total[i])
        }
      }
      setData(tempData)
    } else {
      setData(total)
    }
    setSearch(text)
  }

  const onSave = () => {
    const query = `&project_id=${project_id}&location_id=${project_location_id}&date=${Util.getCurrentDate()}`
    const body = []
    let url = ''
    for (const i in data) {
      const id = data[i].project_product_code_id
      if (role === Constant.PM || role === Constant.OM) {
        const obj = {
          project_product_code_id: id,
        }
        if (selectRole === 1) {
          url = `saveInternalQuantity${query}`
          obj.internal_quantity = data[i].details[0] ? data[i].details[0].internal_quantity : null
        } else {
          url = `saveClientQuantity${query}`
          if(data[i].details[0]) {
            obj.client_quantity = data[i].details[0].client_quantity || data[i].details[0].internal_quantity
          } else {
            obj.client_quantity = null
          }
        }
        if (obj.internal_quantity || obj.client_quantity) {
          body.push(obj)
        }
      } else {
        const obj = {
          project_product_code_id: id,
          quantity: data[i].init_quantity,
        }
        url = `saveSupQuantity${query}`
        if (obj.quantity) {
          body.push(obj)
        }
      }
    }
    setOnPress(true)
    postQuantityInventory({ url, body })
  }

  const renderItem = ({ item }) => {
    let value = ''
    if (role === Constant.CL) {
      value = item.details[0] ? item.details[0].client_quantity : 'N/A'
    } else if (role === Constant.SUP) {
      value = item.details[0] ? item.details[0].internal_quantity : item.init_quantity
    } else {
      if (selectRole === 1) {
        value = item.details[0] ? item.details[0].internal_quantity : 'N/A'
      } else {
        value = item.details[0] ? item.details[0].client_quantity : 'N/A'
        if (!value) {
          value = item.details[0].internal_quantity
        }
      }
    }
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => inputRef.current[item.product_code].focus()}
        key={item.product_code}
        style={[styles.wrap, Style.paddingVertical16]}
      >
        <View style={[Style.flexRow, Style.flexAlignCenter, { flex: 1 }]}>
          <Image
            source={{
              uri: item.product_code_image ? item.product_code_image : 'https://i.imgur.com/9opIMog.png',
            }}
            resizeMode="contain"
            style={styles.icon}
          />
          <TextFont style={[Style.textBlack, Style.textBold, Style.marginLeft16, { fontSize: 16 }]}>
            {item.product_code}
          </TextFont>
        </View>
        <View style={[Style.flexRow, Style.flexAlignCenter]}>
          <TextInput
            ref={el => (inputRef.current[item.product_code] = el)}
            editable={role === Constant.CL ? false : role === Constant.SUP && disabled ? false : true}
            keyboardType="number-pad"
            value={value || ''}
            placeholderTextColor="black"
            onChangeText={text => {
              const tempArray = Object.assign([], data)
              for (const i in tempArray) {
                if (tempArray[i].product_code === item.product_code) {
                  if (role === Constant.SUP) {
                    tempArray[i].init_quantity = text
                    if (tempArray[i].details[0]) {
                      tempArray[i].details[0].internal_quantity = text
                    }
                  } else {
                    if (selectRole === 1) {
                      if (tempArray[i].details[0]) {
                        tempArray[i].details[0].internal_quantity = text
                      } else {
                        const tempObj = {
                          internal_quantity: text,
                        }
                        tempArray[i].details.push(tempObj)
                      }
                    } else {
                      if (tempArray[i].details[0]) {
                        tempArray[i].details[0].client_quantity = text
                      } else {
                        const tempObj = {
                          client_quantity: text,
                        }
                        tempArray[i].details.push(tempObj)
                      }
                    }
                  }
                  break
                }
              }
              setDisabledUpdate(false)
              setData(tempArray)
            }}
            maxLength={6}
            allowFontScaling={false}
            style={styles.input}
            placeholder="0"
          />
          <TextFont style={[Style.textBlack, Style.headline5, Style.marginLeft16, { fontWeight: '500' }]}>
            {item.product_unit}
          </TextFont>
        </View>
      </TouchableOpacity>
    )
  }

  let extraOffset = 0
  if (isIphoneX()) {
    extraOffset = 47
  }

  return (
    <Popup backgroundColor="#F5F5F9" isScrollable={false} hideClose={true} ref={innerRef} theme="dark">
      <AvoidKeyboardView keyboardVerticalOffset={extraOffset} style={[Style.Flex1]}>
        <DismissKeyboardView style={[Style.Flex1]}>
          <View style={[Style.paddingTop8, Style.flexRow]}>
            {role === Constant.PM || role === Constant.OM ? (
              <View style={styles.button} />
            ) : (
              <TouchableOpacity onPress={closeModal} style={styles.button}>
                <Image style={styles.iconBack} source={require('images/btnBackBlack.png')} />
              </TouchableOpacity>
            )}
            <TextFont fontWeight="medium" style={[Style.headline4]}>
              {role === Constant.PM || role === Constant.OM ? 'Sửa kết quả thực hiện' : `Hàng tồn kho`}
            </TextFont>
            {role === 'CLIENT' ? (
              <View style={styles.button} />
            ) : (
              <TouchableOpacity
                onPress={() => {
                  if (role === Constant.PM || role === Constant.OM) {
                    closeModal()
                  } else {
                    onSave()
                  }
                }}
                style={[styles.button, { alignItems: 'flex-end' }]}
              >
                {role === Constant.PM || role === Constant.OM ? (
                  <Image style={styles.iconClose} source={require('images/icon-close-grey.png')} />
                ) : disabled ? null : (
                  <TextFont fontWeight="medium" style={[Style.headline4, { color: color.EgyptianBlue }]}>
                    {`Lưu`}
                  </TextFont>
                )}
              </TouchableOpacity>
            )}
          </View>
          {role === Constant.PM || role === Constant.OM ? (
            <View style={[Style.marginTop16, Style.paddingHorizontal24]}>
              <View
                style={[
                  Style.flexRow,
                  Style.paddingVertical4,
                  Style.paddingHorizontal4,
                  { backgroundColor: 'white', borderRadius: 8 },
                ]}
              >
                <TouchableOpacity
                  onPress={() => {setSelectRole(1), setDisabledUpdate(false)}}
                  style={[
                    styles.buttonRole,
                    { backgroundColor: selectRole === 1 ? color.EgyptianBlue : 'white' },
                  ]}
                >
                  <TextFont style={{ color: selectRole === 1 ? 'white' : 'black' }}>
                    Chỉnh sửa nội bộ
                  </TextFont>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {setSelectRole(2), setDisabledUpdate(false)}}
                  style={[
                    styles.buttonRole,
                    { backgroundColor: selectRole === 2 ? color.EgyptianBlue : 'white' },
                  ]}
                >
                  <TextFont style={{ color: selectRole === 2 ? 'white' : 'black' }}>
                    Hiển thị cho khách
                  </TextFont>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
          <View style={[Style.Flex1, Style.paddingHorizontal24, Style.marginTop16]}>
            <Input
              iconStyle={{ tintColor: 'black' }}
              search
              value={search}
              noUnderLine
              placeholder="Tìm kiếm"
              viewStyle={[styles.searchInput, Style.fullWidth]}
              inputStyle={[Style.textBlack]}
              placeholderTextColor={'grey'}
              onChange={text => functionSearch(text)}
            />
            {data && data.length > 0 ? (
              <FlatList
              showsVerticalScrollIndicator={false}
                keyExtractor={item => item.product_code}
                data={data}
                contentContainerStyle={[Style.paddingBottom32, Style.marginTop16]}
                renderItem={renderItem}
                extraData={data}
              />
            ) : null}
          </View>
          {role === Constant.PM || role === Constant.OM ? (
            <TouchableOpacity
              onPress={onSave}
              disabled={disabledUpdate}
              style={[
                styles.bottomButton,
                {
                  backgroundColor: disabledUpdate
                    ? '#BEBEBE'
                    : selectRole === 1
                    ? color.EgyptianBlue
                    : color.Green,
                },
              ]}
            >
              <TextFont style={[Style.textWhite, { fontSize: 16, lineHeight: 24 }]}>Cập nhật</TextFont>
            </TouchableOpacity>
          ) : null}
          {props.isLoading && <Loading />}
        </DismissKeyboardView>
      </AvoidKeyboardView>
    </Popup>
  )
}
