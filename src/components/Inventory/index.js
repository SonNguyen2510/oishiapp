import { connect } from 'react-redux'
import Inventory from './Inventory'
import {postQuantityInventory, getAllInventory} from 'actions/common'

const mapStateToProps = ({ common, user }) => {
  return {
    isLoading: common.toJS().isLoading,
    getInventory: common.toJS().getInventory,
    userInfo: user.toJS().userInfo,
    locationDetail: common.toJS().locationDetail,
    postInventory: common.toJS().postInventory
  }
}

export default connect(
  mapStateToProps,
  {postQuantityInventory, getAllInventory},
)(Inventory)
