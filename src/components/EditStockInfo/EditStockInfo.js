import React, { useState } from 'react'
import { View } from 'react-native'
import { TextFont, Button, Popup, Input, InputGroup } from 'components'
import Tabs, { Tab, TabList, TabPanel, TabPanels } from 'components/Tabs'
import Style from 'variables/style'
import color from 'variables/color'

const tabMapping = {
  0: {
    buttonColor: 'blue',
    textInputColor: color.Black,
    type: 'internal',
  },
  1: {
    buttonColor: 'green',
    textInputColor: color.Green,
    type: 'external',
  },
}
export default function EditStockInfo(props) {
  const { innerRef, onChange, locationStock, onSubmmit } = props
  const [tab, setTab] = useState(0)
  const { buttonColor, textInputColor, type } = tabMapping[tab]

  const handleSubmit = () => {
    onSubmmit({ stock_info: locationStock })
  }

  const renderInput = () => {
    return (
      <View style={[Style.paddingTop32, Style.Flex1, Style.flexSpaceBetween]}>
        <InputGroup>
          <Input
            onInputChange={onChange}
            inputWrapperStyle={Style.bgWhite}
            placeholder="Please enter"
            label="Hàng Sampling"
            labelRight="can"
            id={`${type}_sampling`}
            value={locationStock[`${type}_sampling`]}
            keyboardType="numeric"
            inputStyle={{ color: textInputColor }}
          />
          <Input
            inputWrapperStyle={Style.bgWhite}
            onInputChange={onChange}
            placeholder="Please enter"
            label="Hàng bán"
            labelRight="bottle/can"
            id={`${type}_sale`}
            value={locationStock[`${type}_sale`]}
            keyboardType="numeric"
            inputStyle={{ color: textInputColor }}
          />
          <Input
            inputWrapperStyle={Style.bgWhite}
            onInputChange={onChange}
            placeholder="Please enter"
            label="Quà tặng 1"
            labelRight="pcs"
            id={`${type}_redemption1`}
            value={locationStock[`${type}_redemption1`]}
            keyboardType="numeric"
            inputStyle={{ color: textInputColor }}
          />
          <Input
            inputWrapperStyle={Style.bgWhite}
            onInputChange={onChange}
            placeholder="Please enter"
            label="Quà tặng 2"
            labelRight="pcs"
            id={`${type}_redemption2`}
            value={locationStock[`${type}_redemption2`]}
            keyboardType="numeric"
            inputStyle={{ color: textInputColor }}
          />
        </InputGroup>
      </View>
    )
  }

  return (
    <Popup ref={innerRef} theme="dark" backgroundColor={color.LightGrey}>
      <View style={[Style.Flex1, Style.flexSpaceBetween, Style.paddingTop16, Style.paddingHorizontal16]}>
        <View style={[Style.flexAlignCenter, Style.paddingBottom16]}>
          <TextFont fontWeight="medium" style={[Style.headline4]}>
            Sửa kết quả thực hiện
          </TextFont>
        </View>
        <View style={[Style.Flex1, Style.paddingTop24]}>
          <Tabs style={[Style.Flex1]} onChangeTab={tab => setTab(tab)} activeIdx={tab}>
            <TabList style={Style.bgWhite}>
              <Tab title={' Chỉnh sửa nội bộ'} />
              <Tab title={'Hiển thị cho khách'} />
            </TabList>
            <TabPanels>
              <TabPanel style={[Style.Flex1]}>{renderInput()}</TabPanel>
              <TabPanel style={[Style.Flex1]}>{renderInput()}</TabPanel>
            </TabPanels>
          </Tabs>
        </View>
        <View style={[Style.paddingTop32, Style.paddingBottom16]}>
          <Button title={'Cập nhật'} type={buttonColor} onPress={handleSubmit} />
        </View>
      </View>
    </Popup>
  )
}
