import { connect } from 'react-redux'
import EditStockInfo from './EditStockInfo'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
  }
}

export default connect(
  mapStateToProps,
  null,
)(EditStockInfo)
