import { connect } from 'react-redux'
import CheckinCheckOut from './CheckinCheckOut'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
  }
}

export default connect(
  mapStateToProps,
  null,
)(CheckinCheckOut)
