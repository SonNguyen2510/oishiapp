import { StyleSheet, Dimensions } from 'react-native'

const SCREEN_WIDTH = Dimensions.get('screen').width

import color from 'variables/color'

export default StyleSheet.create({
  emptyPhoto: {
    width: SCREEN_WIDTH / 2.6,
    height: (SCREEN_WIDTH * 0.75) / 2.6,
    borderRadius: 8,
  },
  emptyPhotoContainer: {
    paddingHorizontal: 10,
    paddingVertical: 6,
  },
})
