import React, { useState, useRef } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { TextFont, Button, Popup, PreviewImage } from 'components'
import Style from 'variables/style'
import styles from './styles'
import moment from 'moment'
import FastImage from 'react-native-fast-image'

export default function CheckinCheckOut(props) {
  const { data, navigate, innerRef, onPress, hasImage, headerTitle, buttonColor } = props
  const [imageToShow, setImageToShow] = useState(0)
  const previewModal = useRef()

  const onPressImage = index => {
    setImageToShow(index)
    previewModal?.current.toggle()
  }

  const navigateToCamera = () => {
    navigate && navigate()
  }

  const renderModalBottomButton = () => (
    <View style={[Style.fullWidth, Style.paddingHorizontal16, Style.paddingBottom16]}>
      <Button
        title={`${headerTitle} (${moment().format('hh:mm')})`}
        type={hasImage ? buttonColor : 'darkGrey'}
        // onPress={hasImage ? onPress : () => {}}
        onPress={onPress}
      />
    </View>
  )

  const renderPhotoContent = () => {
    return (
      <View style={[Style.paddingTop32, Style.FlexWrap, Style.flexRow, Style.flexJustifyCenter]}>
        {data.map((item, index) => {
          const onPress = item.uri ? () => onPressImage(index) : navigateToCamera
          return (
            <TouchableOpacity key={index} style={[styles.emptyPhotoContainer]} onPress={onPress}>
              <Image
                source={item.uri ? item : require('images/empty_photo.png')}
                style={[styles.emptyPhoto]}
              />
            </TouchableOpacity>
          )
        })}
      </View>
    )
  }

  return (
    <Popup ref={innerRef} renderBottomButton={renderModalBottomButton} theme="dark">
      <View style={[Style.Flex1, Style.flexSpaceBetween, Style.paddingTop16]}>
        <View style={[Style.flexAlignCenter, Style.paddingTop8]}>
          <TextFont fontWeight="medium" style={[Style.headline4]}>
            {`Chụp hình ${headerTitle}`}
          </TextFont>
        </View>
        <View style={[Style.Flex1, Style.paddingHorizontal16, Style.flexAlignCenter]}>
          {renderPhotoContent()}
        </View>
      </View>
      <PreviewImage innerRef={previewModal} item={data[imageToShow]} />
    </Popup>
  )
}
