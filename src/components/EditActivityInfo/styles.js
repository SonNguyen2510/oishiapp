import { StyleSheet, Dimensions } from 'react-native'
import color, { rgba } from 'variables/color'
const SCREEN_HEIGHT = Dimensions.get('screen').height

export default StyleSheet.create({
  wrapper: {
    padding: 15,
    backgroundColor: color.Grey,
    borderRadius: 10,
    height: 60,
  },
  scrollView: {
    // position: 'absolute',
    width: '100%',
    // bottom: 0,
    maxHeight: SCREEN_HEIGHT / 2,
    backgroundColor: color.White,
  },
  content: {
    margin: 0,
    backgroundColor: rgba(color.Black, 0.4),
  },
  value: {
    fontSize: 16,
    lineHeight: 32,
    color: color.Black,
  },
  placeholder: {
    color: rgba(color.Black1, 0.4),
    fontSize: 16,
  },
  calendar: {
    width: 25,
    height: 25,
    position: 'absolute',
    right: 16,
    top: 18,
  },
  label: {
    fontSize: 14,
    lineHeight: 14,
    paddingBottom: 5,
    color: rgba(color.Black1, 0.4),
    // color: color.Black1,
  },
})
