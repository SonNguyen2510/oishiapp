import { connect } from 'react-redux'
import EditActivityInfo from './EditActivityInfo'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
  }
}

export default connect(
  mapStateToProps,
  null,
)(EditActivityInfo)
