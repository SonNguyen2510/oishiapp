import React from 'react'
import { View } from 'react-native'
import { TextFont, Button, Popup, Input, DateTimeInput, InputGroup } from 'components'
import { Util } from 'services'
import Style from 'variables/style'
import color from 'variables/color'
import moment from 'moment'

export default function EditActivityInfo(props) {
  const { innerRef, onChange, activityTime, onSubmmit, check_out_by } = props
  const { check_in_time, check_out_time, note } = activityTime || {}

  const handleSubmit = () => {
    onSubmmit({
      activity_info: {
        ...activityTime,
        check_in_time: moment(check_in_time).format('HH:mm'),
        check_out_time: moment(check_out_time).format('HH:mm'),
      },
    })
  }

  const renderInput = () => {
    const viewStyle = { width: '46%' }

    return (
      <View style={[Style.Flex1]}>
        <InputGroup>
          <View style={[Style.flexRow, Style.flexSpaceBetween]}>
            <DateTimeInput
              viewStyle={viewStyle}
              label="Giờ check in"
              onChange={value => onChange('check_in_time', value)}
              mode="time"
              value={check_in_time}
              backgroundColor={color.White}
            />
            <DateTimeInput
              viewStyle={viewStyle}
              label="Giờ check out"
              onChange={value => onChange('check_out_time', value)}
              mode="time"
              backgroundColor={color.White}
              value={check_out_time}
              editable={!!check_out_by}
            />
          </View>
          <View style={[Style.paddingVertical8]}>
            <TextFont style={[Style.bodyText, Style.textGrey3]}>Ghi chú trong ngày</TextFont>
          </View>

          <Input
            noUnderLine
            noLabel
            placeholder="Ghi chú . . ."
            multiline={true}
            numberOfLines={4}
            value={note}
            id="note"
            onInputChange={onChange}
            editable={!!note}
          />
        </InputGroup>
      </View>
    )
  }

  return (
    <Popup ref={innerRef} theme="dark" backgroundColor={color.LightGrey}>
      <View style={[Style.Flex1, Style.flexSpaceBetween, Style.paddingTop16, Style.paddingHorizontal16]}>
        <View style={[Style.flexAlignCenter, Style.paddingBottom16]}>
          <TextFont fontWeight="medium" style={[Style.headline4]}>
            Sửa kết quả thực hiện
          </TextFont>
        </View>
        <View style={[Style.Flex1, Style.paddingTop24]}>{renderInput()}</View>
        <View style={[Style.paddingTop32, Style.paddingBottom16]}>
          <Button title={'Cập nhật'} type="blue" onPress={handleSubmit} />
        </View>
      </View>
    </Popup>
  )
}
