import React from 'react'
import { Keyboard } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper'
import PropTypes from 'prop-types'
import { Panel } from 'components'
import Button from 'components/Button'
import Style from 'variables/style'
import styles from './styles'

class BottomButton extends React.Component {
  state = {
    isShown: false,
  }

  componentWillMount() {
    const { isUnitTest } = this.props
    if (isIphoneX() || isUnitTest) {
      this.keyboardShowListener = Keyboard.addListener('keyboardWillShow', this.handleShowKeyboard)
      this.keyboardHideListener = Keyboard.addListener('keyboardWillHide', this.handleHideKeyboard)
    }
  }

  componentWillUnmount() {
    if (this.keyboardShowListener) {
      this.keyboardShowListener.remove()
      this.keyboardHideListener.remove()
    }
  }

  handleShowKeyboard = () => {
    this.setState({ isShown: true })
  }

  handleHideKeyboard = () => {
    this.setState({ isShown: false })
  }

  onPress = e => {
    Keyboard.dismiss()
    this.props.onPress(e)
  }

  render() {
    const { isShown } = this.state
    return (
      <Panel customStyle={[Style.paddingHorizontal16, !isShown && styles.iPhoneXStyle]}>
        <Button
          {...this.props}
          onPress={this.onPress}
          containerViewStyle={[styles.container, this.props.containerViewStyle]}
          buttonStyle={[this.props.buttonStyle]}
        />
      </Panel>
    )
  }
}

BottomButton.defaultProps = {
  onPress: () => {},
}

export default BottomButton
