import { StyleSheet } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'

export default StyleSheet.create({
  container: {
    width: '100%',
    marginLeft: 0,
    justifyContent: 'flex-end',
  },
  iPhoneXStyle: {
    ...ifIphoneX({
      paddingBottom: 32,
    }),
  },
})
