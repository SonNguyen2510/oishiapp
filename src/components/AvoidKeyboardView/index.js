import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardAvoidingView, Platform } from 'react-native'

export default function AvoidKeyboardView(props) {
  return <KeyboardAvoidingView {...props} behavior={Platform.OS === 'ios' ? props.behavior : undefined} />
}

AvoidKeyboardView.propTypes = {
  behavior: PropTypes.string,
}

AvoidKeyboardView.defaultProps = {
  behavior: 'padding',
}
