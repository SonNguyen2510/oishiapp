import React, { useState } from 'react'
import { View, Text, Image, Animated, Platform, TouchableOpacity } from 'react-native'
import styles from './styles'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import { TextFont } from 'components'
import Languages from 'resources/languages'
import Style from 'variables/style'

const HEADER_EXPANDED_HEIGHT = Platform.OS === 'ios' ? 145 : 155
const HEADER_COLLAPSED_HEIGHT = Platform.OS === 'ios' ? 75 : 85

function MenuHeader(props) {
  const { totalProductCount, handleOpenCart } = props
  const [scrollY] = useState(new Animated.Value(0))

  const headerHeight = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
    extrapolate: 'clamp',
  })

  const marginView = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [25, 15],
    extrapolate: 'clamp',
  })

  const heroTitleOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  })

  const onScrollAnimation = ({ nativeEvent }) => {
    const { y } = nativeEvent.contentOffset
    scrollY.setValue(y)
  }

  return (
    <View>
      <Animated.View
        style={[styles.headerContainer, Style.ContainerContent, Style.screenWidth, { height: headerHeight }]}
      >
        <Animated.View style={[styles.iconMenuView, { marginTop: marginView }]}>
          <View style={[Style.flexRow, Style.Flex12, Style.flexSpaceBetween]}>
            {/* <Image resizeMode="contain" style={[styles.menuIcon]} source={require('assets/icon/menu.png')} /> */}
            <Image
              resizeMode="contain"
              style={[styles.locationIcon]}
              // source={require('assets/icon/location.png')}
            />
          </View>
          <View style={[Style.Flex2, Style.marginLeft8]}>
            <Text style={[Style.textBold, Style.textDarkGreen, { fontSize: 18 }]}>{Languages.distric1}</Text>
          </View>
          <View style={[Style.flexRow, Style.Flex2, Style.flexSpaceBetween]}>
            {/* <Image style={[styles.scanIcon]} resizeMode="contain" source={require('assets/icon/scan.png')} /> */}
            <TouchableOpacity onPress={props.handleOpenLogin}>
              <Image
                style={[styles.profileIcon]}
                resizeMode="contain"
                // source={require('assets/icon/profile.png')}
              />
            </TouchableOpacity>

            <View>
              <TouchableOpacity onPress={handleOpenCart}>
                <Image
                  style={[styles.cartIcon]}
                  resizeMode="contain"
                  // source={require('assets/icon/cart.png')}
                />
                {totalProductCount ? (
                  <View style={styles.numberCart}>
                    <TextFont textColor="white" fontWeight="bold" fontSize={10}>
                      {props.totalProductCount}
                    </TextFont>
                  </View>
                ) : null}
              </TouchableOpacity>
            </View>
          </View>
        </Animated.View>
        <Animated.View style={[styles.searchView, { opacity: heroTitleOpacity }]}>
          <TextInput style={[styles.searchInput]} placeholder={Languages.whatYouWantToBuy} />
          {/* <Image
              style={[styles.searchIcon]}
              resizeMode='contain'
              source={require('assets/icon/search.png')}
            /> */}
        </Animated.View>
      </Animated.View>
      <ScrollView
        contentContainerStyle={{ paddingTop: HEADER_EXPANDED_HEIGHT }}
        scrollEventThrottle={16}
        onScroll={onScrollAnimation}
      >
        {props.children}
      </ScrollView>
    </View>
  )
}

MenuHeader.propTypes = {}

MenuHeader.defaultProps = {}

export default MenuHeader
