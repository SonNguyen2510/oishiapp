import { StyleSheet, Platform } from 'react-native'
import color from 'variables/color'

export default StyleSheet.create({
  headerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'white',
    zIndex: 9999,
    borderBottomWidth: 0.5,
    borderColor: color.Grey5,
  },
  iconMenuView: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginBottom: 10,
  },
  menuIcon: {
    height: 24,
    width: 28,
    marginHorizontal: 5,
  },
  locationIcon: {
    height: 24,
    width: 19,
    marginHorizontal: 5,
  },
  scanIcon: {
    height: 24,
    width: 32,
    marginHorizontal: 5,
  },
  profileIcon: {
    height: 24,
    width: 20,
    marginHorizontal: 5,
  },
  cartIcon: {
    height: 24,
    width: 24,
    marginHorizontal: 5,
  },
  searchView: {
    paddingHorizontal: 20,
    paddingTop: Platform.OS == 'ios' ? 5 : 12,
  },
  searchIcon: {
    height: 20,
    width: 20,
  },
  searchInput: {
    borderRadius: 25,
    height: 40,
    backgroundColor: color.Grey5,
    textAlign: 'center',
  },
  numberCart: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    backgroundColor: color.Red,
    width: 20,
    height: 20,
    borderRadius: 13,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
