import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { Popup, Button, TextFont } from 'components'
import { Util } from 'services'
import ImageViewer from 'react-native-image-zoom-viewer'
import styles from './styles'
import FastImage from 'react-native-fast-image'
import Style from 'variables/style'
import color from 'variables/color'
import moment from 'moment'
import { useNavigation } from '@react-navigation/native'
import MapView, { Marker } from 'react-native-maps'

const statusMapping = {
  inprogress: {
    color: color.Green,
    title: 'Inprogress',
  },
  checkin: {
    color: color.EgyptianBlue,
    title: 'Check-in',
  },
  checkout: {
    color: color.Red,
    title: 'Check-out',
  },
}
export default function GoogleMapImage(props) {
  const { route } = props
  const item = route.params
  const styles = StyleSheet.create({
    headerWrap: {
      height: 100,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
    },
    closeIcon: {
      width: 28,
      height: 28,
      marginTop: 10,
    },
    marker: {
      width: 60,
      height: 60
    
    }
  })
  const coords = item.uri.substring(item.uri.lastIndexOf('=') + 1, item.uri.lastIndexOf('+')).split('_')

  const latitude = coords[0]
  const longitude = coords[1]
  console.log('lat', latitude, longitude)

  const renderHeaderInfo = () => {
    const navigation = useNavigation()
    if (item.check_type || item.created_date)
      return (
        <View style={styles.headerWrap}>
          <TouchableOpacity
            style={[styles.closeBtn, Style.CornerTopLeft]}
            onPress={() => navigation.goBack()}
            hitSlop={{ top: 20, bottom: 20, right: 20, left: 20 }}
          >
            <Image
              style={[styles.closeIcon, { tintColor: 'grey' }]}
              source={require('assets/images/btnBackBlack.png')}
            />
          </TouchableOpacity>
          <TextFont textColor="black" fontWeight="medium" style={[Style.headline4, { marginTop: 30 }]}>
            {`${moment(item.created_date, 'YYYY-MM-DD HH:mm:ss')
              .utcOffset(-10)
              .format('hh:mm A')} ${Util.formatDate(item.created_date, 'DD/MM/YYYY')} `}
          </TextFont>
        </View>
      )
    else return null
  }

  return (
    <View style={{ flex: 1 }}>
      {renderHeaderInfo()}
      <MapView
        style={{ flex: 1 }}
        region={{ latitude, longitude, longitudeDelta: 0.004, latitudeDelta: 0.004 }}
      >
        <Marker tracksViewChanges={false} coordinate={{ latitude, longitude }}>
          <Image
            resizeMode='contain'
            style={[styles.marker]}
            source={require('assets/images/red_marker.png')}
          />
        </Marker>
      </MapView>
    </View>
  )
}
