import React from 'react'
import { View, Image } from 'react-native'
import { Popup, Button, TextFont } from 'components'
import { Util } from 'services'
import ImageViewer from 'react-native-image-zoom-viewer'
import styles from './styles'
import FastImage from 'react-native-fast-image'
import Style from 'variables/style'
import color from 'variables/color'
import moment from 'moment'
import { useNavigation } from '@react-navigation/native'

const statusMapping = {
  inprogress: {
    color: color.Green,
    title: 'Inprogress',
  },
  checkin: {
    color: color.EgyptianBlue,
    title: 'Check-in',
  },
  checkout: {
    color: color.Red,
    title: 'Check-out',
  },
}
export default function PreviewImage(props) {
  const navigation = useNavigation();
  const { item = {}, innerRef, imagesList, showMarker } = props

  const images = [{ url: item.uri || item.url }]

  const renderHeaderInfo = () => {
    if (item.check_type || item.created_date)
      return (
        <View style={[Style.flexAlignCenter, Style.paddingTop12]}>
          <View
            style={[
              Style.marginTop8,
              Style.paddingVertical4,
              Style.paddingHorizontal8,
              { borderRadius: 6, backgroundColor: statusMapping[item.check_type].color },
            ]}
          >
            <TextFont textColor="white">{statusMapping[item.check_type].title}</TextFont>
          </View>
          <TextFont textColor="darkGrey" fontWeight="medium" style={[Style.headline6]}>
            {`${moment(item.created_date, 'YYYY-MM-DD HH:mm:ss')
              .utcOffset(-10)
              .format('hh:mm A')} - ${Util.formatDate(item.created_date, 'DD/MM/YYYY')} `}
          </TextFont>
        </View>
      )
    else return null
  }

  return (
    <Popup onPressMarker={() => navigation.navigate('GoogleMapImage', item)} ref={innerRef} animationType="fade" backgroundColor="black" theme="light" showMarker={showMarker}>
      <ImageViewer
        imageUrls={imagesList || images}
        // enableSwipeDown
        // swipeDownThreshold={400}
        useNativeDriver
        saveToLocalByLongPress={false}
        renderIndicator={() => null}
        onCancel={() => innerRef.current.toggle()}
        renderImage={props => {
          return <Image {...props} style={[styles.imagePreview]} resizeMode="contain" />
        }}
        renderHeader={renderHeaderInfo}
      />
    </Popup>
  )
}
