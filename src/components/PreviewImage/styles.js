import { StyleSheet } from 'react-native'

import color from 'variables/color'

export default StyleSheet.create({
  imagePreview: {
    height: '95%',
  },
  emptyPhotoContainer: {
    paddingHorizontal: 8,
    paddingVertical: 6,
  },
  checkTypeBagde: {
    borderRadius: 8,
  },
})
