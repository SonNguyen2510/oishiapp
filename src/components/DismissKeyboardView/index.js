import React from 'react'
import { TouchableWithoutFeedback, Keyboard, View } from 'react-native'

const DismissKeyboardHOC = Comp => {
  // eslint-disable-next-line react/display-name
  return ({ children, ...props }) => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <Comp {...props}>{children}</Comp>
    </TouchableWithoutFeedback>
  )
}
const DismissKeyboardView = DismissKeyboardHOC(View)

export default DismissKeyboardView
