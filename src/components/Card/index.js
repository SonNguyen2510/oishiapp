import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity, Image } from 'react-native'

import Style from 'variables/style'
import color from 'variables/color'
import { TextFont, Button } from 'components'

import styles from './styles'

export default function Card(props) {
  const { style, header, content, data, footer, onPress, noBreakLine } = props
  const Component = onPress ? TouchableOpacity : View
  return (
    <Component style={[styles.container, style]} activeOpacity={0.8} onPress={onPress}>
      <View style={styles.contentContainer}>
        {header(data)}
        {!noBreakLine && <View style={[Style.borderBottom]} />}
        {content(data)}
        {!noBreakLine && <View style={[Style.borderBottom]} />}
        {footer(data)}
      </View>
    </Component>
  )
}

Card.propTypes = {
  onPress: PropTypes.func,
  header: PropTypes.func,
  content: PropTypes.func,
  footer: PropTypes.func,
  noBreakLine: PropTypes.bool,
}

Card.defaultProps = {
  noBreakLine: false,
  header: () => {},
  content: () => {},
  footer: () => {},
  onPress: () => {},
}
