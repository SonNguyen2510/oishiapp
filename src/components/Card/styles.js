import { StyleSheet } from 'react-native'

import Color from 'variables/color'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.White1,
    borderRadius: 10,
    marginBottom: 32,
  },
  contentContainer: {
    padding: 16,
  },
  backgroundImage: {
    // flex: 1,
    position: 'absolute',
    borderRadius: 3,
    height: '100%',
    width: '100%',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  closeButton: {
    position: 'absolute',
    right: 16,
    top: 16,
  },
  closeIcon: {
    height: 10,
    width: 10,
  },
})
