import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import { Text } from 'react-native'
import common from 'variables/common'
import color from 'variables/color'

const fontMapping = {
  regular: common.fontRegular,
  bold: common.fontBold,
  medium: common.fontMedium,
}

const textColorMapping = {
  white: color.White,
  blue: color.EgyptianBlue,
  darkBlue: color.DarkBlue,
  green: color.Green,
  darkGrey: color.DarkGrey,
}

export default class TextFont extends React.PureComponent {
  handlePress = e => {
    this.props.onPress(e)
  }

  onPress = this.props.onPress ? _.throttle(this.handlePress, 2000, { trailing: false }) : () => {}

  render() {
    const {
      fontWeight,
      style,
      children,
      uppercase,
      capitalize,
      onPress,
      isHTML,
      textColor,
      fontSize,
      numberOfLines,
    } = this.props
    const renderedText = _.isString(children)
      ? children
      : _.isArray(children)
      ? children.join('')
      : _.toString(children)
    const listStyle = {
      fontFamily: fontMapping[fontWeight],
      backgroundColor: 'transparent',
      color: textColorMapping[textColor] || color.Black1,
      fontSize: fontSize,
    }

    if (_.isFunction(onPress)) {
      return (
        <Text style={[listStyle, style]} allowFontScaling={false} onPress={this.onPress}>
          {!isHTML
            ? uppercase
              ? renderedText.toUpperCase()
              : capitalize
              ? _.capitalize(renderedText)
              : renderedText
            : children}
        </Text>
      )
    }

    return (
      <Text style={[listStyle, style]} allowFontScaling={false} numberOfLines={numberOfLines}>
        {!isHTML
          ? uppercase
            ? renderedText.toUpperCase()
            : capitalize
            ? _.capitalize(renderedText)
            : renderedText
          : children}
      </Text>
    )
  }
}

TextFont.propTypes = {
  fontWeight: PropTypes.string,
  onPress: PropTypes.func,
  uppercase: PropTypes.bool,
  capitalize: PropTypes.bool,
}

TextFont.defaultProps = {
  fontWeight: 'regular',
  style: {},
  uppercase: false,
  capitalize: false,
}
