import React from 'react'
import { View, StyleSheet } from 'react-native'
import color from 'variables/color'

const styles = StyleSheet.create({
  panel: {
    marginTop: 5,
    marginBottom: 5,
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: color.White,
  },
  noMarginTop: {
    marginTop: 0,
  },
  noPadding: {
    paddingTop: 0,
    paddingBottom: 0,
  },
})

function Panel(props) {
  const { noMarginTop, customStyle, noPadding } = props
  return (
    <View
      style={[styles.panel, noMarginTop && styles.noMarginTop, noPadding && styles.noPadding, customStyle]}
    >
      {props.children}
    </View>
  )
}

export default Panel
