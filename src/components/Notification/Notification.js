import React from 'react'
import { View, StatusBar, Platform, Image, TouchableOpacity } from 'react-native'
import { TextFont, Popup } from 'components'
import { Util, Constant } from 'services'
import isEmpty from 'lodash/isEmpty'

import styles from './styles'
import Style from 'variables/style'

export default function Notification(props) {
  const { innerRef, notifications, navigate } = props
  const newNotify = notifications.filter(item => item.status === Constant.NOTIFY_NEW)
  const readNotify = notifications.filter(item => item.status === Constant.NOTIFY_READ)

  const handleSelectNotify = item => {
    navigate(item)
  }

  const renderNotificationList = (data, read) => {
    return (
      <View style={[Style.paddingBottom16, Style.paddingHorizontal24]}>
        {data.map(item => renderItem(item, read))}
      </View>
    )
  }

  const renderItem = (item, read) => {
    const { activity_count, image_count, project_location } = item
    if (!project_location) {
      return null;
    }
    const {
      location_name,
      location_address,
      location_district,
      location_ward,
      location_city,
    } = project_location
    return (
      <TouchableOpacity onPress={() => handleSelectNotify(item)} key={item.notification_id}>
        <View style={[Style.flexRow, Style.paddingTop16]}>
          <View style={[Style.paddingTop8]}>
            <Image
              style={[styles.info]}
              source={
                image_count
                  ? read
                    ? require('assets/images/read-notify-2.png')
                    : require('assets/images/image-notify.png')
                  : read
                  ? require('assets/images/read-notify.png')
                  : require('assets/images/notification-info.png')
              }
            />
          </View>
          <View style={[Style.paddingHorizontal16]}>
            <TextFont isHTML>
              <TextFont
                fontWeight="medium"
                textColor={read ? 'darkGrey' : 'blue'}
                style={[Style.headline5, Style.paddingTop8]}
              >
                Đang có
              </TextFont>
              <TextFont
                fontWeight="bold"
                textColor={read ? 'darkGrey' : 'blue'}
                style={[Style.headline5, Style.paddingTop8]}
              >
                {` ${activity_count || image_count} ${(activity_count && 'thông tin') ||
                  (image_count && 'hình ảnh')} `}
              </TextFont>
              <TextFont
                fontWeight="medium"
                textColor={read ? 'darkGrey' : 'blue'}
                style={[Style.headline5, Style.paddingTop8]}
              >
                đang chờ
              </TextFont>
            </TextFont>
            <TextFont style={[Style.headline6]} textColor={read && 'darkGrey'}>
              {`${location_name || ''} ${location_address || ''} ${location_ward || ''} ${location_district ||
                ''} ${location_city || ''}`}
            </TextFont>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <Popup ref={innerRef} theme="dark">
      <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
      <View style={[Style.Flex1, Style.bgLightGrey]}>
        <View style={[Style.flexAlignCenter, Style.paddingVertical16, Style.bgWhite]}>
          <TextFont fontWeight="medium" style={[Style.headline4]}>
            Thông báo
          </TextFont>
        </View>
        {!isEmpty(newNotify) && (
          <>
            <View style={[Style.bgWhite, Style.flexAlignCenter]}>
              <TextFont fontWeight="medium" style={[Style.smallBodyTextGrey, Style.paddingVertical8]}>
                Chưa đọc
              </TextFont>
            </View>
            {renderNotificationList(newNotify)}
          </>
        )}
        {!isEmpty(readNotify) && (
          <>
            <View style={[Style.bgWhite, Style.flexAlignCenter]}>
              <TextFont fontWeight="medium" style={[Style.smallBodyTextGrey, Style.paddingVertical8]}>
                Đã đọc
              </TextFont>
            </View>
            {renderNotificationList(readNotify, true)}
          </>
        )}
      </View>
    </Popup>
  )
}
