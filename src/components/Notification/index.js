import { connect } from 'react-redux'
import Notification from './Notification'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
  }
}

export default connect(
  mapStateToProps,
  null,
)(Notification)
