import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Style from 'variables/style'
import { TextFont } from 'components'
import color, { rgba } from 'variables/color'

const mappingStyle = {
  yellow: [color.Yellow, color.Black],
  blue: [color.EgyptianBlue, color.White],
}
export default class Tab extends React.PureComponent {
  static propTypes = {
    uppercase: PropTypes.bool,
    title: PropTypes.string,
    active: PropTypes.bool,
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }

  static defaultProps = {
    uppercase: false,
    textStyle: {},
    style: {},
    containerStyle: {},
    type: 'blue',
  }

  onActivate = e => {
    this.props.onActivate(e)
  }

  render() {
    const {
      title,
      active,
      textStyle,
      disabled,
      textWhite,
      uppercase,
      style,
      containerStyle,
      type,
      label,
      small,
    } = this.props

    const [backgroundColor, textColor] = mappingStyle[type]

    return (
      <TouchableOpacity onPress={this.onActivate} disabled={disabled} style={[containerStyle, Style.Flex1]}>
        <View
          style={[
            style,
            styles.tab,
            active && !disabled && { backgroundColor, borderRadius: 12 },
            small && { borderRadius: 10, paddingVertical: 6 },
            label && styles.labelWrapper,
          ]}
        >
          <TextFont
            style={[
              {
                fontSize: 15,
                color: active ? textColor : color.Black,
              },
              textStyle,
              textWhite && { color: active ? color.White : rgba(color.White, 0.6) },
            ]}
            fontWeight="medium"
            uppercase={uppercase}
          >
            {title}
          </TextFont>
          {label && (
            <View style={styles.label}>
              <TextFont textColor="white">{label}</TextFont>
            </View>
          )}
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  tab: {
    alignItems: 'center',
    alignContent: 'center',
    paddingVertical: 10,
  },
  labelWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  label: {
    backgroundColor: color.Red,
    borderRadius: 100,
    paddingHorizontal: 4,
    paddingVertical: 2,
    marginHorizontal: 4,
  },
})
