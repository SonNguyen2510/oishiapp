import Tabs from './Tabs'
import Tab from './Tab'
import TabList from './TabList'
import TabPanel from './TabPanel'
import TabPanels from './TabPanels'

export default Tabs
export { Tab, TabList, TabPanel, TabPanels }
