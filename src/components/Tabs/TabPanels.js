import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import _ from 'lodash'

export default function TabPanels(props) {
  const { children, activeIdx, style } = props
  const falseyChildrenRemoved = _.compact(children)
  return <View style={[style]}>{falseyChildrenRemoved[activeIdx]}</View>
}

TabPanels.propType = {
  style: PropTypes.object,
  children: PropTypes.arrayOf(PropTypes.element),
}

TabPanels.defaultProps = {
  style: {},
}
