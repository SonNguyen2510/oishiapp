import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import { View, ScrollView, StyleSheet } from 'react-native'
import Tab from './Tab'
import color from 'variables/color'

const styles = StyleSheet.create({
  tabList: {
    flexDirection: 'row',
    backgroundColor: color.Grey,
    borderRadius: 15,
    padding: 5,
  },
  tabListSmall: {
    flexDirection: 'row',
    backgroundColor: color.Grey,
    borderRadius: 10,
    padding: 3,
  },
})

export default function TabList({ style = {}, activeIdx, onActivate, children, scrollable, small }) {
  const childrenList = React.Children.map(_.compact(children), (child, index) => {
    if (child.type === Tab) {
      return React.cloneElement(child, {
        active: index === activeIdx,
        small,
        onActivate: () => {
          onActivate(index)
        },
      })
    }
    return child
  })

  if (scrollable)
    return (
      <ScrollView
        style={[{ flexDirection: 'row', backgroundColor: color.White }, style]}
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {childrenList}
      </ScrollView>
    )

  return <View style={[small ? styles.tabListSmall : styles.tabList, style]}>{childrenList}</View>
}

TabList.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element),
}
