import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'

export default function TabPanel({ children, style }) {
  return <View style={style}>{children}</View>
}

TabPanel.propTypes = {
  children: PropTypes.element,
}

TabPanel.defaultProps = {
  style: {},
}
