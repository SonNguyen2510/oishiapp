import React from 'react'
import { View } from 'react-native'

import TabList from './TabList'
import TabPanels from './TabPanels'

export default class Tabs extends React.Component {
  static defaultProps = {
    activeIdx: 0,
    onChangeTab: () => {},
    style: {},
  }

  constructor(props) {
    super(props)

    const { activeIdx } = props

    this.state = {
      activeIdx,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        activeIdx: this.props.activeIdx,
      })
    }, 500)
  }

  activateTab = index => {
    this.setState({ activeIdx: index }, () => {
      this.props.onChangeTab(this.state.activeIdx)
    })
  }

  renderChildren = () => {
    const { activeIdx } = this.state
    return React.Children.map(this.props.children, child => {
      if (child.type === TabPanels) {
        return React.cloneElement(child, { activeIdx })
      } else if (child.type === TabList) {
        return React.cloneElement(child, {
          activeIdx,
          onActivate: this.activateTab,
        })
      }

      return child
    })
  }

  render() {
    return <View style={this.props.style}>{this.renderChildren()}</View>
  }
}
