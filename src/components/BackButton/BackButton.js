import React from 'react'
import { Image, TouchableOpacity, StyleSheet } from 'react-native'
import { useNavigation } from '@react-navigation/native'

export default function BackButton(props) {
  const navigation = useNavigation()
  const {
    style = {},
    imageStyle = {},
    onPress = () => navigation.goBack(),
    black = true,
    closeButton = false,
  } = props

  return (
    <TouchableOpacity style={style} onPress={onPress} hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}>
      <Image
        style={[styles.btnBack, imageStyle]}
        source={
          closeButton
            ? require('assets/images/closeBtn.png')
            : black
            ? require('assets/images/btnBackBlack.png')
            : require('assets/images/btnBackWhite.png')
        }
      />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  btnBack: {
    width: 20,
    height: 20,
    margin: 16,
  },
  backButton: {
    height: 30,
    width: 30,
  },
})
