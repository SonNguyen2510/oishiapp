import { connect } from 'react-redux'
import {
  getProvincesAndChannels,
  getDistrictsByProvinceId,
  getWardByDistrictId,
  addProjectLocation,
} from 'actions/common'
import AddNewLocation from './AddNewLocation'

const mapStateToProps = ({ common }) => {
  return {
    isLoading: common.toJS().isLoading,
    masterData: common.toJS().masterData,
  }
}

export default connect(
  mapStateToProps,
  { getProvincesAndChannels, getDistrictsByProvinceId, getWardByDistrictId, addProjectLocation },
)(AddNewLocation)
