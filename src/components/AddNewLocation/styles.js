import { StyleSheet } from 'react-native'
import color, { rgba } from 'variables/color'

export default StyleSheet.create({
  searchInput: {
    backgroundColor: rgba(color.Black, 0.35),
    borderRadius: 10,
    paddingVertical: 2,
  },
})
