import React, { useState, useEffect } from 'react'
import { Alert, View } from 'react-native'
import { TextFont, Button, Popup, Input, DateTimeInput, Dropdown, InputGroup } from 'components'
import Style from 'variables/style'
import _ from 'lodash'
import { Util } from 'services'

const defaultForm = {
  location_name: '',
  location_address: '',
  channel: '',
  location_city: '',
  location_district: '',
  location_ward: '',
}
export default function AddNewLocation(props) {
  const {
    innerRef,
    getProvincesAndChannels,
    getDistrictsByProvinceId,
    getWardByDistrictId,
    masterData,
    addProjectLocation,
    project_id,
  } = props

  const [address, setAddress] = useState(defaultForm)
  const [start_date, setStartDate] = useState('')
  const [end_date, setEndDate] = useState('')
  const [start_time, setStartTime] = useState('')
  const [end_time, setEndTime] = useState('')

  useEffect(() => {
    _.isEmpty(masterData) && getProvincesAndChannels()
  }, [])

  useEffect(() => {
    address.location_city && getDistrictsByProvinceId({ province_id: address.location_city })
  }, [address.location_city])

  useEffect(() => {
    address.location_district && getWardByDistrictId({ district_id: address.location_district })
  }, [address.location_district])

  const transformMasterData = (data = []) =>
    data.map(item => ({ value: item.id || item.code, label: item.name }))

  const onPressAddProject = () => {
    const params = {
      ...address,
      project_id,
      start_date: Util.formatDate(start_date, 'YYYY-MM-DD'),
      end_date: Util.formatDate(end_date, 'YYYY-MM-DD'),
      start_time: Util.formatTime(start_time, 'HH:mm:ss'),
      end_time: Util.formatTime(end_time, 'HH:mm:ss'),
    }

    addProjectLocation(params, handleMessage)
  }

  const handleMessage = (data = {}) => {
    const message = data.split(':')
    Alert.alert(message[0], `${message[1]}`, [
      {
        text: 'OK',
        onPress: () => {
          resetForm()
          innerRef.current?.toggle()
        },
      },
    ])
  }

  const handleInputAddressForm = (id, value) => {
    setAddress({ ...address, [id]: value })
  }

  const resetForm = () => {
    setStartDate('')
    setEndDate('')
    setStartTime('')
    setEndTime('')
    setAddress(defaultForm)
  }

  const renderInput = () => {
    const viewStyle = { width: '46%' }
    return (
      <View style={[Style.paddingTop32, Style.Flex1, Style.flexSpaceBetween]}>
        <InputGroup>
          <Input
            onInputChange={handleInputAddressForm}
            id="location_name"
            isInGroup
            value={address.location_name}
            placeholder="Tên địa điểm"
            label="Tên địa điểm"
          />
          <Input
            onInputChange={handleInputAddressForm}
            value={address.location_address}
            id="location_address"
            isInGroup
            placeholder="Địa chỉ địa điểm"
            label="Địa chỉ"
          />
          <Dropdown
            isInGroup
            id="channel"
            value={address.channel}
            options={transformMasterData(masterData?.channels)}
            onDropdownChange={handleInputAddressForm}
            label="Chọn kênh thực hiện"
          />
          <Dropdown
            id="location_city"
            onDropdownChange={handleInputAddressForm}
            value={address.location_city}
            options={transformMasterData(masterData?.provinces)}
            label="Thành phố"
          />
          <Dropdown
            value={address.location_district}
            onDropdownChange={handleInputAddressForm}
            id="location_district"
            options={transformMasterData(masterData?.districts)}
            label="Quận"
          />
          <Dropdown
            value={address.location_ward}
            onDropdownChange={handleInputAddressForm}
            id="location_ward"
            options={transformMasterData(masterData?.wards)}
            label="Phường / Huyện"
          />
          <View style={[Style.flexRow, Style.flexSpaceBetween]}>
            <DateTimeInput
              viewStyle={viewStyle}
              label="Ngày bắt đầu"
              onChange={value => setStartDate(value)}
              mode="date"
              value={start_date}
            />
            <DateTimeInput
              viewStyle={viewStyle}
              label="Ngày kết thúc"
              onChange={value => setEndDate(value)}
              mode="date"
              value={end_date}
            />
          </View>
          <View style={[Style.flexRow, Style.flexSpaceBetween]}>
            <DateTimeInput
              viewStyle={viewStyle}
              label="Thời gian bắt đầu"
              onChange={value => setStartTime(value)}
              mode="time"
              value={start_time}
            />
            <DateTimeInput
              viewStyle={viewStyle}
              label="Thời gian kết thúc"
              onChange={value => setEndTime(value)}
              mode="time"
              value={end_time}
            />
          </View>
        </InputGroup>
      </View>
    )
  }

  return (
    <Popup ref={innerRef} theme="dark">
      <View style={[Style.Flex1, Style.flexSpaceBetween, Style.paddingTop16, Style.paddingHorizontal16]}>
        <View style={[Style.flexAlignCenter, Style.paddingVertical16]}>
          <TextFont fontWeight="medium" style={[Style.headline4]}>
            Thêm địa điểm mới
          </TextFont>
        </View>
        {renderInput()}
        <View style={[Style.flexRow, Style.flexSpaceBetween, Style.paddingTop32, Style.paddingBottom16]}>
          <Button
            containerViewStyle={{ width: '48%' }}
            title={'Thiết lập lại'}
            type="grey"
            onPress={resetForm}
          />
          <Button
            // containerViewStyle={{ width: '48%' }}
            title={'Thêm địa điểm'}
            type="blue"
            onPress={onPressAddProject}
          />
        </View>
      </View>
    </Popup>
  )
}
