import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

export default class RadioGroup extends Component {
  state = {
    value: this.props.value,
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value !== state.value) {
      return { value: props.value }
    }
    return null
  }

  getValue = () => {
    return this.state.value
  }

  onRadioChecked = value => {
    this.setState({ value })

    const { id, onRadioChecked, onChange } = this.props
    onRadioChecked(id, value)
    onChange(value)
  }

  render() {
    const { value } = this.state
    const children = React.Children.map(this.props.children, child => {
      if (!child) return child
      return React.cloneElement(child, {
        bgDark: this.props.bgDark,
        checked: value === child.props.value,
        onRadioChecked: value => {
          this.onRadioChecked(value)
        },
      })
    })
    return <View style={styles.container}>{children}</View>
  }
}

const styles = StyleSheet.create({
  container: {},
})

RadioGroup.propTypes = {
  children: PropTypes.any,
  bgDark: PropTypes.bool,
  onChange: PropTypes.func,
  onRadioChecked: PropTypes.func,
}

RadioGroup.defaultProps = {
  onRadioChecked: () => {},
  onChange: () => {},
}
