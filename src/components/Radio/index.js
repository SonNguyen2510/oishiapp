import React from 'react'
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import PropTypes from 'prop-types'

import { TextFont } from 'components'
import color, { rgba } from 'variables/color'
import Style from 'variables/style'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  radioStyle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  unChecked: {
    backgroundColor: rgba(color.Black1, 0.05),
  },
  unCheckedDark: {
    backgroundColor: rgba(color.White1, 0.05),
  },
  checked: {
    backgroundColor: color.Green,
  },
  smallWhiteCircle: {
    width: 5,
    height: 5,
    borderRadius: 5,
    backgroundColor: color.White,
  },
  bgDark: {
    backgroundColor: color.Black1,
  },
  text: {
    marginLeft: 16,
    fontSize: 16,
    lineHeight: 23,
  },
  disabledDash: {
    width: 10,
    height: 2,
    backgroundColor: rgba(color.White1, 0.1),
  },
  disabledText: {
    color: rgba(color.White, 0.2),
  },
})

export default class Radio extends React.PureComponent {
  state = {
    checked: this.props.checked,
  }

  componentDidMount() {
    if (this.props.checked) {
      this.props.onRadioChecked(this.props.value)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ checked: nextProps.checked })
  }

  checked = () => {
    if (!this.props.disabled) {
      this.props.onRadioChecked(this.props.value)
      this.setState({
        checked: true,
      })
    }
  }

  render() {
    const { text, children, style, bgDark, disabled, customStyle } = this.props
    const { checked } = this.state
    const radioStyles = [
      styles.radioStyle,
      checked ? styles.checked : bgDark ? styles.unCheckedDark : styles.unChecked,
    ]

    return (
      <TouchableWithoutFeedback onPress={this.checked}>
        <View style={[styles.container, style]}>
          <View style={[radioStyles, disabled && styles.unCheckedDark]}>
            {disabled ? (
              <View style={styles.disabledDash} />
            ) : (
              checked && <View style={[styles.smallWhiteCircle, bgDark && styles.bgDark]} />
            )}
          </View>
          {text && (
            <TextFont
              fontWeight="regular"
              style={[styles.text, bgDark && Style.textWhite, disabled && styles.disabledText, customStyle]}
            >
              {text}
            </TextFont>
          )}
          {children}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

Radio.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  children: PropTypes.any,
  checked: PropTypes.bool,
  text: PropTypes.string,
  onRadioChecked: PropTypes.func,
  value: PropTypes.any,
  bgDark: PropTypes.bool,
}

Radio.defaultProps = {
  bgDark: false,
  checked: false,
}
