import { fromJS } from 'immutable'
import { SET_USER_INFO } from '../actions/user'

export const initialState = fromJS({
  userInfo: {},
})

export default function user(state = initialState, { type, payload }) {
  switch (type) {
    case SET_USER_INFO:
      return state.set('userInfo', payload)

    default:
      return state
  }
}
