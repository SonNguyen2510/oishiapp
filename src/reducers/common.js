import { fromJS } from 'immutable'
import {
  TOGGLE_LOADING,
  SET_ALL_PROJECTS,
  SET_LOGIN_STATUS,
  SET_PROJECT_LOCATION_DETAIL,
  RESET_FILTER_DATE,
  SET_MASTER_DATA,
  SET_FILTER_DATE,
  SET_NOTIFICATION,
  SET_GET_INVENTORY,
  SET_POST_INVENTORY
} from '../actions/common'

export const initialState = fromJS({
  isLoading: false,
  allProjects: {},
  isLogin: false,
  locationDetail: {},
  masterData: {},
  filterDate: '',
  notifications: [],
})

export default function common(state = initialState, { type, payload }) {
  switch (type) {
    case TOGGLE_LOADING:
      return state.set('isLoading', payload)

    case SET_ALL_PROJECTS:
      return state.set('allProjects', payload)

    case SET_LOGIN_STATUS:
      return state.set('isLogin', payload)

    case SET_PROJECT_LOCATION_DETAIL:
      return state.set('locationDetail', payload)

    case RESET_FILTER_DATE:
      return state.set('filterDate', '')

    case SET_FILTER_DATE:
      return state.set('filterDate', payload)

    case SET_MASTER_DATA:
      return state.set('masterData', { ...state.toJS().masterData, ...payload })

    case SET_NOTIFICATION:
      return state.set('notifications', payload)

    case SET_GET_INVENTORY:
        return state.set('getInventory', payload)
  
    case SET_POST_INVENTORY:
        return state.set('postInventory', payload)

    default:
      return state
  }
}
